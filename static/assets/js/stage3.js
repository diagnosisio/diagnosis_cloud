var base_url = window.location.origin;

var ACTION_REQUIRED = 3;

var medicalTree = {};


var mainObj = {
	canvasWidth:null,
	canvasHeight:null,
	stage:null,
	stage_zoom:null,
	layer_main:null,
	layer_rect:null,
	layer_zoom:null,
	layer_zoom2:null,
	rect_id_array:[],	
	img:null,
	image_url:null,
	imgZoom:null,
	dst:null,
	dstI:null,
	dental_data:null,
	zoomLevel:1.5,
	scalingFactor:1.5,
	heightOffset:0,
	mouseDown:false,
	brightnessLevel:1,
	contrastLevel:0,
	isInvert:false,
	xStart:null,
	yStart:null,
	renderableWidth:null,
	renderableHeight:null,
	imageHeight:null,
	imageWidth:null,
	multipoint:false,
	multimove:false,
	point:false,
	scale:1
}

var instanceArray = [];

var line = new Konva.Line({
	x: 0,
	y: 0,
	points: [],
	stroke: 'red',
	tension: 0,
	strokeWidth:1,
	shadowColor:'white',
	shadowBlur:1,
	draggable:false
});

var circle_temp = new Konva.Circle({
	strokeWidth:2,
	fill:'red',
	radius:5
})

var startingPoint = new Konva.Circle({
	fill:'blue',
	radius:3,
	listening:false
})


var line2 = new Konva.Line({
	x: 0,
	y: 0,
	points: [],
	stroke: 'red',
	tension: 0,
	strokeWidth:1,
	shadowColor:'white',
	shadowBlur:1,
	draggable:false
});

var a = 0;

var obj1 = $.extend(true,{},mainObj);

var NUMBER_OF_ANNOTATIONS = 0;

$(document).ready(function(){
	Konva.pixelRatio = 1;
	//obj1.layer_main = new Konva.Layer();
	obj1.layer_rect = new Konva.Layer();
	obj1.layer_zoom = new Konva.Layer();
	obj1.layer_zoom2 = new Konva.Layer();
	
 	if(screen.height>850)
	{
		obj1.scalingFactor = 1.35;
		obj1.heightOffset = 85;
	}
	else
	{
		obj1.heightOffset = 40;
		obj1.scalingFactor = 1.35;
	}
	$('#main-view,.main-view-wrapper').height(parseInt(screen.height-75-180))
	//$('#main-view,.main-view-wrapper').width(parseInt(screen.availWidth/1.8))
    
	obj1.canvasHeight = $('#main-view').height();
	obj1.canvasWidth = $('#main-view').width();

	obj1.stage = new Konva.Stage({
		container: 'main-view',   // id of container <div>
		width: obj1.canvasWidth,
		height: obj1.canvasHeight
	});

	obj1.stage_zoom = new Konva.Stage({
		container: 'zoom-view',   // id of container <div>
		width: 300,
		height: 250
	})

	var directionCtr = 0,
	    oldy = 0,
	    oldx = 0,
	    mousemovemethod = function (e,obj) {
	    	if (e.pageY < oldy) {
	            obj.brightnessLevel -= 2
	        } else if (e.pageY > oldy) {
	            obj.brightnessLevel += 2
	        }
	        if (e.pageX < oldx) {
	            obj.contrastLevel += 0.03
	        } else if (e.pageX > oldx) {
	            obj.contrastLevel -= 0.03
	        }
	        oldx = e.pageX;  
	        oldy = e.pageY;
	        filter(obj.contrastLevel,obj.brightnessLevel,obj);
	}

	$('#main-view').mousedown(function(){
		obj1.mouseDown=true;
	})
	
	$('body').mouseup(function(){
		obj1.mouseDown=false;directionCtr=0;oldx=0,oldy=0
	})
	
	$('#main-view').mousemove(function(e){
		if(obj1.mouseDown==true && $('.wl-ww').hasClass('active') && $('.multipoint-tool').hasClass('active')==false && $('.zoom-btn').hasClass('active')==false){
			mousemovemethod(e,obj1)
		}
	})

	$('.wl-ww').click(function(){
		if($('.multipoint-tool').hasClass('active')){
			$('.multipoint-tool').trigger('click');
		}
		$(this).toggleClass('active');
	})

	$('.reset-color-btn').click(function(){
		obj1.brightnessLevel = 1;
		obj1.contrastLevel = 0;
		filter(obj1.contrastLevel,obj1.brightnessLevel,obj1);
	})
	

	var scrollevt =(/Firefox/i.test(navigator.userAgent))? "DOMMouseScroll" : "mousewheel"
	$('#main-view').bind(scrollevt, function(e){
		var direction;
		if(scrollevt == "DOMMouseScroll")
		{
			direction = e.originalEvent.detail;
		}
		else
		{
			direction = e.originalEvent.wheelDelta;
		}
		if($('.zoom').hasClass('active'))
		{
			if(direction < 0) 
			{
			//scroll down

				if(obj1.zoomLevel>1.0)
				{
					obj1.zoomLevel-=0.1;
				}
				if(obj1.zoomLevel<1.0)
				{
					obj1.zoomLevel=1.0;
				}
			}
			else 
			{
			//scroll up

				obj1.zoomLevel+=0.1;
			}

			//prevent page fom scrolling
			zoomView();
			return false;
		}
		else
		{
			return true;
		}
	});

	$('#pat1').change(function(){
		$('#ana1').empty();
		var an1 = medicalTree["BRAIN"]["instance"]["boolean"][$(this).val()];
		if(Object.size(an1) == 0){
			$('#ana1').hide();
		}
		else{
			$('#ana1').show();
			an1 = sortedList(an1);
			$('#ana1').append('<option value="select">Please Select</option>')
			for(i in an1){
				$('#ana1').append('<option value="'+an1[i]+'">'+an1[i]+'</option>')
			}
		}
	})

	$('#pat1-v').change(function(){
		$('#ana1-v').empty();
		var an1 = medicalTree["BRAIN"]["instance"]["boolean"][$(this).val()];
		if(Object.size(an1) == 0){
			$('#ana1-v').hide();
		}
		else{
			$('#ana1-v').show();
			an1 = sortedList(an1);
			$('#ana1-v').append('<option value="select">Please Select</option>')
			for(i in an1){
				$('#ana1-v').append('<option value="'+an1[i]+'">'+an1[i]+'</option>')
			}
		}
	})

	$('#tag-options-v').change(function(){
		$('#tag-options2-v,#tag-options3-v').empty();
		$('#tag-options2-v,#tag-options3-v').hide();
		var to2 = medicalTree["BRAIN"]["annotation"]["polygon"][$(this).val()];
		if(Object.size(to2) > 0){
			$('#tag-options2-v').show();
			to2 = sortedList(to2);
			$('#tag-options2-v').append('<option value="select">Please Select</option>')
			for(i in to2){
				$('#tag-options2-v').append('<option value="'+to2[i]+'">'+to2[i]+'</option>')
			}
		}
	})
	$('#tag-options2-v').change(function(){
		$('#tag-options3-v').empty();
		$('#tag-options3-v').hide();
		var to3 = medicalTree["BRAIN"]["annotation"]["polygon"][$('#tag-options-v').val()][$(this).val()];
		if(Object.size(to3) > 0){
			$('#tag-options3-v').show();
			to3 = sortedList(to3);
			$('#tag-options3-v').append('<option value="select">Please Select</option>')
			for(i in to3){
				$('#tag-options3-v').append('<option value="'+to3[i]+'">'+to3[i]+'</option>')
			}
		}		
	})

	$('#tag-options').change(function(){
		$('#tag-options2,#tag-options3').empty();
		$('#tag-options2,#tag-options3').hide();
		var to2 = medicalTree["BRAIN"]["annotation"]["polygon"][$(this).val()];
		if(Object.size(to2) > 0){
			$('#tag-options2').show();
			to2 = sortedList(to2);
			$('#tag-options2').append('<option value="select">Please Select</option>')
			for(i in to2){
				$('#tag-options2').append('<option value="'+to2[i]+'">'+to2[i]+'</option>')
			}
		}
	})
	$('#tag-options2').change(function(){
		$('#tag-options3').empty();
		$('#tag-options3').hide();
		var to3 = medicalTree["BRAIN"]["annotation"]["polygon"][$('#tag-options').val()][$(this).val()];
		if(Object.size(to3) > 0){
			$('#tag-options3').show();
			to3 = sortedList(to3);
			$('#tag-options3').append('<option value="select">Please Select</option>')
			for(i in to3){
				$('#tag-options3').append('<option value="'+to3[i]+'">'+to3[i]+'</option>')
			}
		}		
	})

	$('#verify-tag-close-btn').click(function(){
		var $_this = $(this);
		$('#new-tag-v').hide();
		$_this.attr('data-lineid');
		$('#annotation-verification-comment').val('');
		removeCircles($_this.attr('data-lineid'),$_this);
	})

	$('#new-tag-save-btn').click(function(){
  		var that = $(this);
  		var tag = $('#tag-options').val();
  		var tag2 = $('#tag-options2').val();
  		var tag3 = $('#tag-options3').val();
  		tag2 = (tag2==null?"NA":tag2);
  		tag3 = (tag3==null?"NA":tag3);
  		var tag_type = $('#new-tag').attr('data-tag-type');
  		var annotation_comment = $('#annotation-comment').val();
  		if(tag!="select" && tag2!="select" && tag3!="select")
  		{
  			if(tag == "Other" && annotation_comment==""){
  				$('#annotation-comment').css('border','red solid 1px');
  				$('#annotation-comment').focus();
		  		setTimeout(function(){
		  			$('#annotation-comment').css('border','#A6A6A6 solid 1px');
		  		},500)
  			}
  			else{
  				var sliceUid = parseInt($('.img-thumbnail-small.active img').attr('data-sliceuid'));
	  			var sliceNumber = parseInt($('.img-thumbnail-small.active').attr('data-slicenumber'))
	  			if(tag_type == "new-multipoint" || tag_type == "new-point"){
		  			var coords = [];
		  			var shape = '';
		  			if(tag_type == "new-multipoint"){
		  				coords = line2.points();
						shape = "polygon"
		  			}
		  			if(tag_type == "new-point"){
		  				coords.push(circle_temp.getX());
		  				coords.push(circle_temp.getY());
		  				shape = "point"
		  			}
					var comments = annotation_comment;
					var annotation_condition = ($('#annotation-condition').is(':checked')==true?"critical":"");
		  			addNewTag(that,sliceUid,tag,tag2,tag3,sliceNumber,coords,false,comments,shape,annotation_condition); // coords corresponds to local system
		  			propagationStartPoint = parseInt($('#new-tag input[name="slice_start_number"]').val())
		  			propagationEndPoint = parseInt($('#new-tag input[name="slice_end_number"]').val())
		  			propagateAnnotation(that,tag,tag2,tag3,coords,propagationStartPoint,propagationEndPoint,comments,shape,annotation_condition); // coords corresponds to local system
		  		}
		  		if(tag_type == "existing-multipoint" || tag_type == "existing-point"){
		  			var currentActiveSlice = $('.img-thumbnail-small.active');
		  			var currentObj = JSON.parse(unescape($('#new-tag').attr('data-tag-data'))); // coords original image size
		  			currentObj.pathology_lvl1 = tag;
		  			currentObj["anatomy_lvl1"] = tag2;
		  			currentObj["anatomy_lvl2"] = tag3;
		  			var auid = currentObj.annotation_id;
		  			currentObj["series_uuid"] = $('#folderid').val();
		  			currentObj["slice_id"] = sliceUid;
		  			currentObj["coords"] = localToGlobalPoints(obj1.stage.find('#'+auid)[0].points())
		  			currentObj["shape"] = (tag_type=="existing-multipoint"?"polygon":"point");
		  			currentObj["comments"] = annotation_comment;
		  			currentObj["severity"] = ($('#annotation-condition').is(':checked')==true?"critical":"");
		  			currentObj["stage"] = ACTION_REQUIRED;
		  			$.ajax({
		  				method:"POST",
		  				url:base_url+"/apis/annotation/allstages/update",
		  				data:JSON.stringify(currentObj),
		  				beforeSend:function(){
		  					that.text('Wait..')
		  				},
		  				success:function(data){
		  					that.text('Updated')
		  					setTimeout(function(){
		  						that.text('Save');
		  						$('#new-tag').hide();
		  					},10);
		  					var allObj = JSON.parse(unescape(currentActiveSlice.find('img').attr('data-annotation')))
		  					if(Object.size(allObj)==0){
		  						allObj = {stage_1:[],stage_2:[],stage_3:[]}
		  					}
		  					if(annotationObj["stage_"+ACTION_REQUIRED] == undefined){
								annotationObj["stage_"+ACTION_REQUIRED] = [];
							}
		  					$.grep(allObj["stage_"+ACTION_REQUIRED],function(ele,key){
		  						if(auid == ele.annotation_id){
		  							ele.pathology_lvl1 = tag;
		  							ele.anatomy_lvl1 = tag2;
		  							ele.anatomy_lvl2 = tag3;
		  							ele["severity"] = currentObj.severity;
		  							ele.comments = currentObj["comments"];
		  							ele.coords = currentObj["coords"];
		  						}
		  					})
		  					currentActiveSlice.find('img').attr('data-annotation',escape(JSON.stringify(allObj)));
		  					currentActiveSlice.trigger('click');
		  					var coords = currentObj.coords;
				  			propagationStartPoint = parseInt($('input[name="slice_start_number"]').val())
				  			propagationEndPoint = parseInt($('input[name="slice_end_number"]').val())
				  			var localCoords = globalToLocalPoints(coords);
				  			propagateAnnotation(that,tag,tag2,tag3,localCoords,propagationStartPoint,propagationEndPoint,currentObj.comments,currentObj.shape,currentObj.severity); // coords corresponds to local system
		  				},
		  				error:function(data){

		  				},
		  				complete:function(){
		  					$('#annotation-comment').val('');
		  				}
		  			})
		  		}
  			}
	  	}
	  	else
	  	{
	  		$('#tag-options').css('border-color','red');
	  		setTimeout(function(){
	  			$('#tag-options').css('border-color','#A6A6A6');
	  		},500)
	  	}
  	})

	$('#series-normal').change(function(){
		var $_this = $(this);
		var isNormal = $(this).is(':checked');
		if(isNormal){
			$.ajax({
				method:"POST",
				url:base_url+'/apis/series/allstages/add',
				data:JSON.stringify({
					series_uuid:$('#folderid').val(),
					label:"normal",
					value:"yes",
					comments:"",
					stage:ACTION_REQUIRED
				}),
				success:function(data){
					$_this.attr('data-series-info-id',data.series_info_id);
				},
				error:function(){
					$_this.prop('checked',false)
				}
			})
		}
		else
		{
			$.ajax({
				method:"POST",
				url:base_url+'/apis/series/allstages/delete',
				data:JSON.stringify({
					series_info_id:parseInt($_this.attr('data-series-info-id')),
					stage:ACTION_REQUIRED
				}),
				success:function(data){
				},
				error:function(){
					$_this.prop('checked',true)
				}
			})
		}
		// var $_this = $(this);
		// $_this.addClass('active');
		// $.ajax({
		// 	method:"POST",
		// 	url:base_url+'/apis/tagging/seriesinfo/add',
		// 	data:JSON.stringify({
		// 		folder_uid:$('#folderid').val(),
		// 		label:"normal",
		// 		value:"yes"
		// 	}),
		// 	success:function(data){
		// 		console.log(data)
		// 	},
		// 	error:function(){
		// 		$_this.removeClass('active');
		// 	}
		// })
	})

	$('#verify-new-annotation').click(function(){
		if($('.multipoint-tool').hasClass('active')==false){
			$('.multipoint-tool').trigger('click');
		}
	})

  	$('#new-tag-close-btn').click(function(){
  		var tag_type = $('#new-tag').attr('data-tag-type');
  		if(tag_type == "new-multipoint" || tag_type == "new-point")
  		{
  			cancelDrawing();
	  	}
	  	else{
	  		var lineObj = JSON.parse(unescape($('#new-tag').attr('data-tag-data')));
	  		removeCircles(lineObj.annotation_id,$('#new-tag-close-btn'));
	  	}
	  	$('#new-tag').hide();
		$('input[name="slice_start_number"]').val('')
  		$('input[name="slice_end_number"]').val('')
	  	$('#annotation-comment').val('');
		$('#new-tag-save-btn').text("Save");
		$('#annotation-condition').prop('checked',false)
  	})

  	$('#new-tag-remove-btn').click(function(){
  		var that = $(this);
  		// if($('#new-tag').attr('data-tag-type')=="new")
  		// {
			$('#new-tag').hide();
			line2.destroy();
			line2 = new Konva.Line({
				x: 0,
				y: 0,
				points: [],
				stroke: 'red',
				tension: 0,
				strokeWidth:1,
				shadowColor:'white',
				shadowBlur:1,
				draggable:false
			});
			obj1.layer_rect.draw();
		// }
		// else
		// {
		// 	// code for existing tag
		// }
  	})

  	$('.invert-btn').click(function(){
  		if($(this).hasClass('locked')==false)
  		{
  			invert(obj1,$(this));
  		}
  	})

  	$('.new-cavity').click(function(){
  		if($(this).hasClass('locked')==false)
  		{
  			$(this).toggleClass('active')
  		}
  	})
  	$('.zoom').click(function(){
	    if($(this).hasClass('locked')==false)
	    {
	      $('.show-tool').removeClass('active');
	      $(this).toggleClass('active');
	      $('.tool-box-expanded').hide();
	      if($(this).hasClass('active')){
	        $('#zoom-view').show();
	      }
	      else
	      {
	        $('#zoom-view').hide();
	      }
	    }
    })

	$(document).mousemove(function(e){
	    if($('.zoom').hasClass('active'))
	    {
	        $('#zoom-view').css("left",e.pageX+15-300);
	        $('#zoom-view').css("top",e.pageY+15);
	    }
	})

	$('#new-tag-secondary').click(function(){
		if($('.multipoint-tool').hasClass('active') == false){
			$('.multipoint-tool').trigger('click');
		}
	})

	$('.multipoint-tool').click(function(){
		if($('.point-tool').hasClass('active')){
			$('.point-tool').trigger('click');
		}
		if($('.wl-ww').hasClass('active')){
			$('.wl-ww').trigger('click')
		}
		$(this).toggleClass('active');
		if($(this).hasClass('active')){
			obj1.multipoint = true;
		}
		else
		{
			obj1.multipoint = false;
			obj1.multimove = false;
		}
	})

	$('.point-tool').click(function(){
		if($('.multipoint-tool').hasClass('active')){
			$('.multipoint-tool').trigger('click');
		}
		$(this).toggleClass('active');
		if($(this).hasClass('active')){
			obj1.point = true;
		}
		else
		{
			obj1.point = false;
		}
	})

	$(document).on('mouseleave','#layer-items .annotation-item,#layer-items-2 .annotation-item,#layer-items .verified-annotation-box',function(){
		var $_this = $(this);
		var auid = $_this.attr('data-annotationid');
		obj1.stage.find('#'+parseInt(auid))[0].strokeWidth(1);
		obj1.stage.find('#'+parseInt(auid))[0].stroke('red');
		obj1.layer_rect.draw();
	})
	$(document).on('mouseenter','#layer-items .annotation-item,#layer-items-2 .annotation-item,#layer-items .verified-annotation-box',function(){
		var $_this = $(this);
		var auid = $_this.attr('data-annotationid');
		obj1.stage.find('#'+parseInt(auid))[0].strokeWidth(3);
		obj1.stage.find('#'+parseInt(auid))[0].stroke('blue');
		obj1.layer_rect.draw();
	})

	$(document).on('mouseleave','.annotation-view',function(e){
		// var $_this = $(this);
		// var auid = $_this.parent().attr('data-annotationid');
		// obj1.stage.find('#'+parseInt(auid))[0].strokeWidth(1);
		// obj1.layer_rect.draw();
		e.stopPropagation();
	})
	$(document).on('mousemove','.annotation-view',function(e){
		// var $_this = $(this);
		// var auid = $_this.parent().attr('data-annotationid');
		// obj1.stage.find('#'+parseInt(auid))[0].strokeWidth(3);
		// obj1.layer_rect.draw();
		e.stopPropagation();
	})

	$(document).on('click','.delete-my-series',function(){
		var $_this = $(this);
		$.ajax({
			method:"POST",
			url:base_url+"/apis/series/allstages/delete",
			data:JSON.stringify({
				stage:ACTION_REQUIRED,
				series_info_id:parseInt($_this.attr('data-id'))
			}),
			beforeSend:function(){
				$_this.text('Wait..')
			},
			success:function(){
				$_this.parent().parent().remove();
			}
		})
	})

	$(document).on('click','#verify-instance-btn',function(){
		var $_this = $(this);
		var currentSlice = $('.img-thumbnail-small.active img')
		if($('#pat1-v').val()=="select" || $('#ana1-v').val()=="select"){

		}
		else{
			var dataObj = {
				stage_2_instance_info_id:"",
	            stage_1_instance_info_id:"",
	            series_uuid:$('#folderid').val(),
	            slice_id:currentSlice.attr('data-sliceuid'),
	            status:"valid",
	            comments:"",
	            edit:1,
	            pathology_lvl1:$('#pat1-v').val(),
	            anatomy_lvl1:$('#ana1-v').val(),
	            anatomy_lvl2:$('#ana2-v').val(),
	            severity:"",
	            verification_stage:""
			}
			switch($_this.attr('data-stagelevel')){
				case "1":
					dataObj.stage_1_instance_info_id = parseInt($('#instance-tag-box-v').attr('data-iuid1'));
					dataObj.verification_stage = 1;
				break;

				case "12":
					dataObj.stage_1_instance_info_id = parseInt($('#instance-tag-box-v').attr('data-iuid1'));
					dataObj.stage_2_instance_info_id = parseInt($('#instance-tag-box-v').attr('data-iuid2'));
					dataObj.verification_stage = 2;
				break;

				case "2":
					dataObj.stage_2_instance_info_id = parseInt($('#instance-tag-box-v').attr('data-iuid2'));
					dataObj.verification_stage = 2;
				break;
			}
			if($_this.attr('data-type')=="add"){
				$.ajax({
					method:"POST",
					url:base_url+"/apis/verification/instance/stage3/add",
					data:JSON.stringify(dataObj),
					beforeSend:function(){
						$_this.text('Wait')
					},
					success:function(data){
						currentSlice.attr('data-verified-count',(parseInt(currentSlice.attr('data-verified-count'))+1));
						if(currentSlice.attr('data-verified-count') == currentSlice.attr('data-total')){
							currentSlice.parent().find('.verification-done').removeClass('hide');
						}
						nextBtnStatus();
						$_this.text('Save');
						$('#instance-tag-box-v').hide();
						currentSlice.attr('data-verified-count',parseInt(currentSlice.attr('data-verified-count'))+1);
						(currentSlice.attr('data-verified-count') == currentSlice.attr('data-total')? currentSlice.parent().find('.verification-done').removeClass('hide'):"");
						//$('.feature-box.instance-item[data-instance-info-id="'+parseInt($('#instance-tag-box-v').attr('data-iuid'))+'"]').addClass('lbg3')
						//$('.feature-box.instance-item[data-instance-info-id="'+parseInt($('#instance-tag-box-v').attr('data-iuid'))+'"]').after('<div class="verified-instance-box custom-tooltip" data-toggle="tooltip" data-placement="left" data-instanceid="'+data.instance_info_id+'" title="'+$('#pat1-v').val()+' / '+$('#ana1-v').val()+' / '+$('#ana2-v').val()+'"><div class="elips">'+$('#pat1-v').val()+' / '+$('#ana1-v').val()+' / '+$('#ana2-v').val()+'</div></div>');
						
						var obj = JSON.parse(unescape(currentSlice.attr('data-instance')));
						switch($_this.attr('data-stagelevel')){
							case "1":
								$.grep(obj["stage_1"],function(ele,key){
									if(ele.instanceinfo_uid ==	$('#instance-tag-box-v').attr('data-iuid1')){
										if(ele.stage_2 == undefined){
											ele["stage_2"] = {};
										}
										if(ele.stage_2.stage_3 == undefined){
											ele["stage_2"]["stage_3"] = {}
										}

										ele["stage_2"]["stage_3"]["anatomy_lvl1"] = dataObj.anatomy_lvl1;
										ele["stage_2"]["stage_3"]["anatomy_lvl2"] = dataObj.anatomy_lvl2;
										ele["stage_2"]["stage_3"]["comments"] = "";
										ele["stage_2"]["stage_3"]["edit"] = 1;
										ele["stage_2"]["stage_3"]["pathology_lvl1"] = dataObj.pathology_lvl1;
										ele["stage_2"]["stage_3"]["severity"] = "";
										ele["stage_2"]["stage_3"]["stage_3_instance_info_id"] = data.instance_info_id;
										ele["stage_2"]["stage_3"]["status"] = "valid";
										ele["stage_2"]["stage_3"]["verification_stage"] = "1";
									}
								})
							break;

							case "12":
								$.grep(obj["stage_1"],function(ele,key){
									if(ele.stage_2.stage_2_instance_info_id ==	$('#instance-tag-box-v').attr('data-iuid2')){
										if(ele.stage_2.stage_3 == undefined){
											ele["stage_2"]["stage_3"] = {};
										}

										ele["stage_2"]["stage_3"]["anatomy_lvl1"] = dataObj.anatomy_lvl1;
										ele["stage_2"]["stage_3"]["anatomy_lvl2"] = dataObj.anatomy_lvl2;
										ele["stage_2"]["stage_3"]["comments"] = "";
										ele["stage_2"]["stage_3"]["edit"] = 1;
										ele["stage_2"]["stage_3"]["pathology_lvl1"] = dataObj.pathology_lvl1;
										ele["stage_2"]["stage_3"]["severity"] = "";
										ele["stage_2"]["stage_3"]["stage_3_instance_info_id"] = data.instance_info_id;
										ele["stage_2"]["stage_3"]["status"] = "valid";
										ele["stage_2"]["stage_3"]["verification_stage"] = "2";
									}
								})
							break;

							case "2":
								$.grep(obj["stage_2"],function(ele,key){
									if(ele.instanceinfo_uid ==	$('#instance-tag-box-v').attr('data-iuid2')){
										if(ele.stage_3 == undefined){
											ele["stage_3"] = {};
										}
										ele["stage_3"]["anatomy_lvl1"] = dataObj.anatomy_lvl1;
										ele["stage_3"]["anatomy_lvl2"] = dataObj.anatomy_lvl2;
										ele["stage_3"]["comments"] = "";
										ele["stage_3"]["edit"] = 1;
										ele["stage_3"]["pathology_lvl1"] = dataObj.pathology_lvl1;
										ele["stage_3"]["severity"] = "";
										ele["stage_3"]["stage_3_instance_info_id"] = data.instance_info_id;
										ele["stage_3"]["status"] = "valid";
										ele["stage_3"]["verification_stage"] = "2";
									}
								})
							break;
						}
						currentSlice.attr('data-instance',escape(JSON.stringify(obj)));
						currentSlice.trigger('click');
					}
				})
			}
			else{
				dataObj["stage_3_instance_info_id"] = parseInt($('#instance-tag-box-v').attr('data-iuid3'));
				delete dataObj["stage_1_instance_info_id"];
				delete dataObj["stage_2_instance_info_id"];
				$.ajax({
					method:"POST",
					url:base_url+"/apis/verification/instance/stage3/update",
					data:JSON.stringify(dataObj),
					beforeSend:function(){
						$_this.text('Wait')
					},
					success:function(data){
						$_this.text('Save');
						$('#instance-tag-box-v').hide();
						// $('.feature-box.instance-item[data-instance-info-id="'+parseInt($('#instance-tag-box-v').attr('data-iuid'))+'"] + .verified-instance-box').remove();
						// $('.feature-box.instance-item[data-instance-info-id="'+parseInt($('#instance-tag-box-v').attr('data-iuid'))+'"]').addClass('lbg3')
						// $('.feature-box.instance-item[data-instance-info-id="'+parseInt($('#instance-tag-box-v').attr('data-iuid'))+'"]').after('<div class="verified-instance-box custom-tooltip" data-toggle="tooltip" data-placement="left" data-instanceid="'+data.instance_info_id+'" title="'+$('#pat1-v').val()+' / '+$('#ana1-v').val()+' / '+$('#ana2-v').val()+'"><div class="elips">'+$('#pat1-v').val()+' / '+$('#ana1-v').val()+' / '+$('#ana2-v').val()+'</div></div>');
						
						var obj = JSON.parse(unescape(currentSlice.attr('data-instance')));
						switch($_this.attr('data-stagelevel')){
							case "1":
								$.grep(obj["stage_1"],function(ele,key){
									if(ele.instanceinfo_uid ==	$('#instance-tag-box-v').attr('data-iuid1')){
										if(ele.stage_2 == undefined){
											ele["stage_2"] = {};
										}
										if(ele.stage_2.stage_3 == undefined){
											ele["stage_2"]["stage_3"] = {}
										}

										ele["stage_2"]["stage_3"]["anatomy_lvl1"] = dataObj.anatomy_lvl1;
										ele["stage_2"]["stage_3"]["anatomy_lvl2"] = dataObj.anatomy_lvl2;
										ele["stage_2"]["stage_3"]["comments"] = "";
										ele["stage_2"]["stage_3"]["edit"] = 1;
										ele["stage_2"]["stage_3"]["pathology_lvl1"] = dataObj.pathology_lvl1;
										ele["stage_2"]["stage_3"]["severity"] = "";
										ele["stage_2"]["stage_3"]["stage_3_instance_info_id"] = data.instance_info_id;
										ele["stage_2"]["stage_3"]["status"] = "valid";
										ele["stage_2"]["stage_3"]["verification_stage"] = "1";
									}
								})
							break;

							case "12":
								$.grep(obj["stage_1"],function(ele,key){
									if(ele.stage_2.stage_2_instance_info_id ==	$('#instance-tag-box-v').attr('data-iuid2')){
										if(ele.stage_2.stage_3 == undefined){
											ele["stage_2"]["stage_3"] = {};
										}

										ele["stage_2"]["stage_3"]["anatomy_lvl1"] = dataObj.anatomy_lvl1;
										ele["stage_2"]["stage_3"]["anatomy_lvl2"] = dataObj.anatomy_lvl2;
										ele["stage_2"]["stage_3"]["comments"] = "";
										ele["stage_2"]["stage_3"]["edit"] = 1;
										ele["stage_2"]["stage_3"]["pathology_lvl1"] = dataObj.pathology_lvl1;
										ele["stage_2"]["stage_3"]["severity"] = "";
										ele["stage_2"]["stage_3"]["stage_3_instance_info_id"] = data.instance_info_id;
										ele["stage_2"]["stage_3"]["status"] = "valid";
										ele["stage_2"]["stage_3"]["verification_stage"] = "2";
									}
								})
							break;

							case "2":
								$.grep(obj["stage_2"],function(ele,key){
									if(ele.instanceinfo_uid ==	$('#instance-tag-box-v').attr('data-iuid2')){
										if(ele.stage_3 == undefined){
											ele["stage_3"] = {};
										}

										ele["stage_3"]["anatomy_lvl1"] = dataObj.anatomy_lvl1;
										ele["stage_3"]["anatomy_lvl2"] = dataObj.anatomy_lvl2;
										ele["stage_3"]["comments"] = "";
										ele["stage_3"]["edit"] = 1;
										ele["stage_3"]["pathology_lvl1"] = dataObj.pathology_lvl1;
										ele["stage_3"]["severity"] = "";
										ele["stage_3"]["stage_3_instance_info_id"] = data.instance_info_id;
										ele["stage_3"]["status"] = "valid";
										ele["stage_3"]["verification_stage"] = "2";
									}
								})
							break;
						}
						currentSlice.attr('data-instance',escape(JSON.stringify(obj)));
						currentSlice.trigger('click');
					}
				})
			}
		}
	})

	$(document).on('click','.feature-box .btn-valid',function(){
		var that = $(this);
		that.toggleClass('active');
		var auid = $(this).parent().parent().attr('data-annotationid');
		var currentSlice = $('.img-thumbnail-small.active img');
		var annotationObj = JSON.parse(unescape(currentSlice.attr('data-annotation')));
		if(Object.size(annotationObj) == 0){
			annotationObj = {stage_1:[],stage_2:[],stage_3:[]}
		}
		if(annotationObj["stage_1"] == undefined){
			annotationObj["stage_1"] = [];
		}
		if(annotationObj["stage_2"] == undefined){
			annotationObj["stage_2"] = [];
		}
		var currentObj = {};
		$.grep(annotationObj["stage_1"],function(ele,key){
			if(ele.annotation_id==auid){				
				currentObj = ele;
			}
		})
		$.grep(annotationObj["stage_2"],function(ele,key){
			if(ele.annotation_id==auid){
				currentObj = ele;
			}
		})

		// setAnnotationVerificationBox(currentObj,auid);
		if(that.hasClass('active')){
			that.closest('.feature-box').next().next('.verified-annotation-box:first').find('.btn-valid').addClass('hide');
			that.closest('.feature-box').next().next('.verified-annotation-box:first').find('.btn-edit').addClass('hide');
			var s3_annotation_obj = {
				stage_2_annotation_id:(that.attr('data-stage')=="2"?currentObj.annotation_id:""),//(currentObj.stage_2?currentObj.stage_2['stage_2_annotation_id']:currentObj.annotation_id),
                series_uuid:$('#folderid').val(),
                slice_id:parseInt(currentSlice.attr('data-sliceuid')),
                stage_1_annotation_id:(that.attr('data-stage')=="2"?"":currentObj.annotation_id),
                status:'valid',
                comments:currentObj.comments,
                edit:0,
                coords:currentObj.coords,
                shape:'polygon',
                pathology_lvl1:currentObj.pathology_lvl1,
                anatomy_lvl1:currentObj.anatomy_lvl1,
                anatomy_lvl2:currentObj.anatomy_lvl2,
                severity:currentObj.severity,
                verification_stage:that.attr('data-stage')
			}
			$.ajax({
				method:'POST',
				url:base_url+"/apis/verification/annotations/stage3/add",
				beforeSend:function(){
					that.text('wait..');
					that.attr('disabled',true);
					that.closest('.feature-box').addClass('lbg3');
				},
				data:JSON.stringify(s3_annotation_obj),
				success:function(data){
					that.text('Valid')
					that.removeAttr('disabled');
					that.attr('data-id3',data.stage_3_annotation_id);
					currentSlice.attr('data-verified-count',(parseInt(currentSlice.attr('data-verified-count'))+1))
					if(currentSlice.attr('data-verified-count') == currentSlice.attr('data-total')){
						currentSlice.parent().find('.verification-done').removeClass('hide');
					}
					nextBtnStatus();
					$.grep(annotationObj["stage_1"],function(ele,key){
						if(ele.annotation_id==auid){	
							if(ele["stage_2"]==undefined){
								ele["stage_2"] = {};
							}			
							ele["stage_2"]["stage_3"] = {};
							ele["stage_2"]["stage_3"]["pathology_lvl1"] = s3_annotation_obj.pathology_lvl1;
							ele["stage_2"]["stage_3"]["anatomy_lvl1"] = s3_annotation_obj.anatomy_lvl1;
							ele["stage_2"]["stage_3"]["anatomy_lvl2"] = s3_annotation_obj.anatomy_lvl2;
							ele["stage_2"]["stage_3"]["comments"] = s3_annotation_obj.comments;
							ele["stage_2"]["stage_3"]["coords"] = s3_annotation_obj.coords;
							ele["stage_2"]["stage_3"]["edit"] = s3_annotation_obj.edit;
							ele["stage_2"]["stage_3"]["severity"] = s3_annotation_obj.severity;
							ele["stage_2"]["stage_3"]["shape"] = s3_annotation_obj.shape;
							ele["stage_2"]["stage_3"]["stage_3_annotation_id"] = data.stage_3_annotation_id;
							ele["stage_2"]["stage_3"]["status"] = s3_annotation_obj.status;
							ele["stage_2"]["stage_3"]["verification_stage"] = s3_annotation_obj.verification_stage;
						}
					})
					$.grep(annotationObj["stage_2"],function(ele,key){
						if(ele.annotation_id==auid){
							// if(ele["stage_2"]==undefined){
							// 	ele["stage_2"] = {};
							// }
							ele["stage_3"] = {};
							ele["stage_3"]["pathology_lvl1"] = s3_annotation_obj.pathology_lvl1;
							ele["stage_3"]["anatomy_lvl1"] = s3_annotation_obj.anatomy_lvl1;
							ele["stage_3"]["anatomy_lvl2"] = s3_annotation_obj.anatomy_lvl2;
							ele["stage_3"]["comments"] = s3_annotation_obj.comments;
							ele["stage_3"]["coords"] = s3_annotation_obj.coords;
							ele["stage_3"]["edit"] = s3_annotation_obj.edit;
							ele["stage_3"]["severity"] = s3_annotation_obj.severity;
							ele["stage_3"]["shape"] = s3_annotation_obj.shape;
							ele["stage_3"]["stage_3_annotation_id"] = data.stage_3_annotation_id;
							ele["stage_3"]["status"] = s3_annotation_obj.status;
							ele["stage_3"]["verification_stage"] = s3_annotation_obj.verification_stage;
						}
					})
					currentSlice.attr('data-annotation',escape(JSON.stringify(annotationObj)));
					currentSlice.trigger('click');
				}
			})
		}
		else{
			that.closest('.feature-box').next().next('.verified-annotation-box:first').find('.btn-valid').removeClass('hide');
			that.closest('.feature-box').next().next('.verified-annotation-box:first').find('.btn-edit').removeClass('hide');
			$.ajax({
				method:'POST',
				url:base_url+'/apis/verification/annotations/stage3/delete',
				data:JSON.stringify({
					stage_3_annotation_id:parseInt(that.attr('data-id3'))
				}),
				beforeSend:function(){
					that.text('wait..');
					that.attr('disabled',true);
					that.closest('.feature-box').removeClass('lbg3');
				},
				success:function(data){
					currentSlice.attr('data-verified-count',(parseInt(currentSlice.attr('data-verified-count'))-1))
					currentSlice.parent().find('.verification-done').addClass('hide')
					that.text('Valid')
					that.siblings('.btn-edit').attr('data-stage3','na')
					that.removeAttr('disabled');
					$('#tv').val((parseInt($('#tv').val())-2));
					nextBtnStatus();
					$.grep(annotationObj["stage_1"],function(ele,key){
						if(ele.annotation_id==auid){				
							delete ele["stage_2"]["stage_3"];
						}
					})
					$.grep(annotationObj["stage_2"],function(ele,key){
						if(ele.annotation_id==auid){
							delete ele["stage_3"];
						}
					})
					currentSlice.attr('data-annotation',escape(JSON.stringify(annotationObj)));
					currentSlice.trigger('click');
				}
			})
		}
	})
	$(document).on('click','.verified-annotation-box .btn-valid',function(){
		var that = $(this);
		that.toggleClass('active');
		var auid = $(this).parent().parent().attr('data-annotationid');
		var currentSlice = $('.img-thumbnail-small.active img');
		var annotationObj = JSON.parse(unescape(currentSlice.attr('data-annotation')));
		if(Object.size(annotationObj) == 0){
			annotationObj = {stage_1:[],stage_2:[],stage_3:[]}
		}
		if(annotationObj["stage_1"] == undefined){
			annotationObj["stage_1"] = [];
		}
		if(annotationObj["stage_2"] == undefined){
			annotationObj["stage_2"] = [];
		}
		var currentObj = {};
		var s1_id = '';
		$.grep(annotationObj["stage_1"],function(ele,key){
			if(ele.stage_2.stage_2_annotation_id==auid){				
				currentObj = ele.stage_2;
				s1_id = ele.annotation_id;
			}
		})
		// $.grep(annotationObj["stage_2"],function(ele,key){
		// 	if(ele.annotation_id==auid){
		// 		currentObj = ele;
		// 	}
		// })
		if(that.hasClass('active')){
			that.closest('.verified-annotation-box').prev('.feature-box:first').find('.btn-valid').addClass('hide');
			that.closest('.verified-annotation-box').prev('.feature-box:first').find('.btn-edit').addClass('hide');
			var s3_annotation_obj = {
				stage_2_annotation_id:currentObj.stage_2_annotation_id,
                series_uuid:$('#folderid').val(),
                slice_id:parseInt(currentSlice.attr('data-sliceuid')),
                stage_1_annotation_id:s1_id,
                status:'valid',
                comments:currentObj.comments,
                edit:0,
                coords:currentObj.coords,
                shape:'polygon',
                pathology_lvl1:currentObj.pathology_lvl1,
                anatomy_lvl1:currentObj.anatomy_lvl1,
                anatomy_lvl2:currentObj.anatomy_lvl2,
                severity:currentObj.severity,
                verification_stage:"2"
			}
			$.ajax({
				method:'POST',
				url:base_url+"/apis/verification/annotations/stage3/add",
				beforeSend:function(){
					that.text('wait..');
					that.attr('disabled',true);
					that.closest('.verified-annotation-box').addClass('lbg3');
				},
				data:JSON.stringify(s3_annotation_obj),
				success:function(data){
					currentSlice.attr('data-verified-count',(parseInt(currentSlice.attr('data-verified-count'))+1));
					if(currentSlice.attr('data-verified-count') == currentSlice.attr('data-total')){
						currentSlice.parent().find('.verification-done').removeClass('hide');
					}
					nextBtnStatus();
					that.text('Valid')
					that.removeAttr('disabled');
					that.attr('data-id3',data.stage_3_annotation_id);
					$.grep(annotationObj["stage_1"],function(ele,key){
						if(ele.stage_2.stage_2_annotation_id==auid){				
							ele["stage_2"]["stage_3"] = {};
							ele["stage_2"]["stage_3"]["pathology_lvl1"] = s3_annotation_obj.pathology_lvl1;
							ele["stage_2"]["stage_3"]["anatomy_lvl1"] = s3_annotation_obj.anatomy_lvl1;
							ele["stage_2"]["stage_3"]["anatomy_lvl2"] = s3_annotation_obj.anatomy_lvl2;
							ele["stage_2"]["stage_3"]["comments"] = s3_annotation_obj.comments;
							ele["stage_2"]["stage_3"]["coords"] = s3_annotation_obj.coords;
							ele["stage_2"]["stage_3"]["edit"] = s3_annotation_obj.edit;
							ele["stage_2"]["stage_3"]["severity"] = s3_annotation_obj.severity;
							ele["stage_2"]["stage_3"]["shape"] = s3_annotation_obj.shape;
							ele["stage_2"]["stage_3"]["stage_3_annotation_id"] = data.stage_3_annotation_id;
							ele["stage_2"]["stage_3"]["status"] = s3_annotation_obj.status;
							ele["stage_2"]["stage_3"]["verification_stage"] = s3_annotation_obj.verification_stage;
						}
					})
					// $.grep(annotationObj["stage_2"],function(ele,key){
					// 	if(ele.annotation_id==auid){
					// 		ele["stage_2"]["stage_3"] = {};
					// 		ele["stage_2"]["stage_3"]["anatomy_lvl1"] = s3_annotation_obj.anatomy_lvl1;
					// 		ele["stage_2"]["stage_3"]["anatomy_lvl2"] = s3_annotation_obj.anatomy_lvl2;
					// 		ele["stage_2"]["stage_3"]["comments"] = s3_annotation_obj.comments;
					// 		ele["stage_2"]["stage_3"]["coords"] = s3_annotation_obj.coords;
					// 		ele["stage_2"]["stage_3"]["edit"] = s3_annotation_obj.edit;
					// 		ele["stage_2"]["stage_3"]["severity"] = s3_annotation_obj.severity;
					// 		ele["stage_2"]["stage_3"]["shape"] = s3_annotation_obj.shape;
					// 		ele["stage_2"]["stage_3"]["stage_3_annotation_id"] = data.stage_3_annotation_id;
					// 		ele["stage_2"]["stage_3"]["status"] = s3_annotation_obj.status;
					// 		ele["stage_2"]["stage_3"]["verification_stage"] = s3_annotation_obj.verification_stage;
					// 	}
					// })
					currentSlice.attr('data-annotation',escape(JSON.stringify(annotationObj)));
				}
			})
		}
		else{
			that.closest('.verified-annotation-box').prev('.feature-box:first').find('.btn-valid').removeClass('hide');
			that.closest('.verified-annotation-box').prev('.feature-box:first').find('.btn-edit').removeClass('hide');
			$.ajax({
				method:'POST',
				url:base_url+'/apis/verification/annotations/stage3/delete',
				data:JSON.stringify({
					stage_3_annotation_id:parseInt(that.attr('data-id3'))
				}),
				beforeSend:function(){
					that.text('wait..');
					that.attr('disabled',true);
					that.closest('.verified-annotation-box').removeClass('lbg3');
				},
				success:function(data){
					currentSlice.attr('data-verified-count',(parseInt(currentSlice.attr('data-verified-count'))-1))
					currentSlice.parent().find('.verification-done').addClass('hide');
					$('#tv').val((parseInt($('#tv').val())-2));
					nextBtnStatus();
					that.text('Valid')
					that.siblings('.btn-edit').attr('data-stage3','na')
					that.removeAttr('disabled');
					$.grep(annotationObj["stage_1"],function(ele,key){
						if(ele.stage_2.stage_2_annotation_id==auid){				
							delete ele["stage_2"]["stage_3"];
						}
					})
					// $.grep(annotationObj["stage_2"],function(ele,key){
					// 	if(ele.annotation_id==auid){
					// 		delete ele["stage_3"];
					// 	}
					// })
					currentSlice.attr('data-annotation',escape(JSON.stringify(annotationObj)));
					currentSlice.trigger('click');
				}
			})
		}
	})

	$(document).on('click','.verify-instance',function(){
		var $_this = $(this);
		var currentSlice = $('.img-thumbnail-small.active img');
		var instance_stage = $_this.attr('data-stage');
		var s3_id = $_this.attr('data-id3');
		$('#instance-tag-box-v').show();
		var iuid = $_this.attr('data-id');
		var instanceObj = JSON.parse(unescape(currentSlice.attr('data-instance')));
		if(Object.size(instanceObj) == 0){
			instanceObj = {stage_1:[],stage_2:[],stage_3:[]}
		}
		if(instanceObj["stage_1"] == undefined){
			instanceObj["stage_1"] = [];
		}
		if(instanceObj["stage_2"] == undefined){
			instanceObj["stage_2"] = [];
		}
		var currentObj = {};
		var s2_id,s1_id;
		if(instance_stage == "1")
		{
			$.grep(instanceObj["stage_1"],function(ele,key){
				if(ele.instanceinfo_uid==iuid){
					if(s3_id=="")
					{
						currentObj = ele;
					}	
					else{
						currentObj = ele.stage_2.stage_3;
					}
				}
			})
			s1_id = iuid;
			s2_id = "";
		}
		if(instance_stage == "12"){
			$.grep(instanceObj["stage_1"],function(ele,key){
				if(ele.stage_2.stage_2_instance_info_id==iuid){
					if(s3_id == ""){
						currentObj = ele.stage_2;
					}
					else{
						currentObj = ele.stage_2.stage_3;
					}
					s1_id = ele.instanceinfo_uid;
					s2_id = iuid;
				}
			})
		}
		if(instance_stage == "2")
		{
			$.grep(instanceObj["stage_2"],function(ele,key){
				if(ele.instanceinfo_uid==iuid){
					if(s3_id=="")
					{
						currentObj = ele;
					}	
					else{
						currentObj = ele.stage_3;
					}
				}
			})
			s1_id = "";
			s2_id = iuid;
		}
		setInstanceVerificationBox(currentObj,iuid,s3_id,instance_stage,s1_id,s2_id);
	})

	$(document).on('click','#layer-items .btn-edit',function(){
		NUMBER_OF_ANNOTATIONS = 0;
		var temp_lines = obj1.stage.find('Line');
		for(i=0;i<temp_lines.length;i++){
			if(temp_lines[i].getId() != undefined){
				NUMBER_OF_ANNOTATIONS++;
			}
		}
		if($('.multipoint-tool').hasClass('active')){
			$('.multipoint-tool').trigger('click');
		}
		var $_this = $(this);
		$('#new-tag-v').show();
		var auid = $(this).parent().parent().attr('data-annotationid');
		var annotationObj = JSON.parse(unescape($('.img-thumbnail-small.active img').attr('data-annotation')));
		if(Object.size(annotationObj) == 0){
			annotationObj = {stage_1:[],stage_2:[],stage_3:[]}
		}
		if(annotationObj["stage_1"] == undefined){
			annotationObj["stage_1"] = [];
		}
		if(annotationObj["stage_2"] == undefined){
			annotationObj["stage_2"] = [];
		}
		var currentObj = {};
		var tag_level = "1nv";
		var allStageIds = {
			stage1:"",
			stage2:"",
			stage3:""
		}
		// 1nv -> level 1 annotation, not verified yet
		// 1v1 -> level 1 annotation, verified stage is 1
		// 12nv -> level 1--2 annotation, not verified yet
		// 12v2 -> level 1--2 annotation, verified stage is 2 
		// 2nv -> level 2 annotation, not verified yet
		// 2v2 -> level 2 annotation, verified in stage 2
		$.grep(annotationObj["stage_1"],function(ele,key){
			if(ele.annotation_id==auid){
				if($_this.siblings('.btn-valid').hasClass('active'))
				{
					currentObj = ele.stage_2.stage_3;
					// console.log('level1, verification-stage:',ele.stage_2.stage_3.verification_stage);
					tag_level = "1v"+ele.stage_2.stage_3.verification_stage;
					allStageIds.stage1 = ele.annotation_id;
					allStageIds.stage2 = ele.stage_2.stage_2_annotation_id;
					allStageIds.stage3 = ele.stage_2.stage_3.stage_3_annotation_id;
				}
				else{
					currentObj = ele;
					allStageIds.stage1 = ele.annotation_id;
					allStageIds.stage2 = (ele.stage_2!=undefined?ele.stage_2.stage_2_annotation_id:"")
					// console.log('level1, not verified');
					tag_level = '1nv';
				}
			}
		})
		$.grep(annotationObj["stage_1"],function(ele,key){
			if(ele.stage_2 != undefined && ele.stage_2.stage_2_annotation_id != undefined)
			{
				if(ele.stage_2.stage_2_annotation_id==auid){
					if($_this.siblings('.btn-valid').hasClass('active'))
					{
						currentObj = ele.stage_2.stage_3;
						// console.log('level12, verification-stage:',ele.stage_2.stage_3.verification_stage);
						tag_level = '12v'+ele.stage_2.stage_3.verification_stage;
						allStageIds.stage1 = ele.annotation_id;
						allStageIds.stage2 = ele.stage_2.stage_2_annotation_id;
						allStageIds.stage3 = ele.stage_2.stage_3.stage_3_annotation_id;
					}
					else{
						currentObj = ele.stage_2;
						allStageIds.stage1 = ele.annotation_id;
						allStageIds.stage2 = ele.stage_2.stage_2_annotation_id;
						// console.log('level12, not verified');
						tag_level = "12nv"
					}
				}
			}
		})
		$.grep(annotationObj["stage_2"],function(ele,key){
			if(ele.annotation_id==auid){
				if($_this.siblings('.btn-valid').hasClass('active'))
				{
					currentObj = ele.stage_3
					// console.log('level2, verification-stage:',ele.stage_3.verification_stage);
					tag_level = "2v"+ele.stage_3.verification_stage
					allStageIds.stage2 = ele.annotation_id;
					allStageIds.stage3 = ele.stage_3.stage_3_annotation_id;
				}
				else{
					currentObj = ele;
					// console.log('level2, not verified');
					tag_level = "2nv"
					allStageIds.stage2 = ele.annotation_id;
				}
			}
		})
		console.log(allStageIds);
		setAnnotationVerificationBox((currentObj.annotation_id||currentObj.stage_2_annotation_id||currentObj.stage_3_annotation_id),currentObj.pathology_lvl1,currentObj.anatomy_lvl1,currentObj.anatomy_lvl2,currentObj.comments,currentObj.severity,auid,$_this.siblings('.btn-valid').hasClass('active'),tag_level,allStageIds);
	})

	$(document).on('click','#layer-items-2 .annotation-item .btn-edit',function(e){
		if($('.multipoint-tool').hasClass('active')){
			$('.multipoint-tool').trigger('click');
		}
		var $_this = $(this);
		var shape = $_this.attr('data-annotation-type');
		var auid = $_this.parent().parent().attr('data-annotationid');
		if(shape=="polygon"){
			$('#new-tag').attr('data-tag-type','existing-multipoint');
			$('#new-tag').css({left:(obj1.stage.find('#'+auid)[0].points()[0] + $('#main-view').offset().left),top:(obj1.stage.find('#'+auid)[0].points()[1] + $('#main-view').offset().top),'margin-left':'-'+(($('#new-tag').width()+20)/2)+'px'})
			if($('#new-tag-close-btn').attr('data-lineid') != undefined){
				var lineObj = JSON.parse(unescape($('#new-tag').attr('data-tag-data')));
				removeCircles(lineObj.annotation_id,$('#new-tag-close-btn'));
			}
			drawMovingPoints(auid);
			$('#new-tag-close-btn').attr('data-lineid',auid);
			$('#new-tag-close-btn').attr('data-linepts',obj1.stage.find('#'+auid)[0].points());
		}
		if(shape == "point"){
			$('#new-tag').attr('data-tag-type','existing-point');	
			$('#new-tag').css({left:(obj1.stage.find('#'+auid)[0].getX() + $('#main-view').offset().left),top:(obj1.stage.find('#'+auid)[0].getY() + $('#main-view').offset().top),'margin-left':'-'+(($('#new-tag').width()+20)/2)+'px'})
		}
		$('#new-tag-save-btn').text("Save");
		$('#new-tag').show();
		var currentSlice = $('.img-thumbnail-small.active img');
		annotationObj = JSON.parse(unescape(currentSlice.attr('data-annotation')));
		var currentObj = {};
		if(annotationObj["stage_"+ACTION_REQUIRED] == undefined){
			annotationObj["stage_"+ACTION_REQUIRED] = [];
		}
		$.grep(annotationObj["stage_"+ACTION_REQUIRED],function(ele,key){
			if(ele.annotation_id==auid){
				currentObj = ele;
			}
		})
		$('#new-tag').attr('data-tag-data',escape(JSON.stringify(currentObj))); // coords original image size
		$('#tag-options').val(currentObj.pathology_lvl1);

		$('#tag-options2,#tag-options3').empty();
		var to2 = medicalTree["BRAIN"]["annotation"]["polygon"][currentObj.pathology_lvl1];
		if(Object.size(to2) > 0){
			$('#tag-options2').show();
			to2 = sortedList(to2);
			for(i in to2){
				$('#tag-options2').append('<option value="'+to2[i]+'">'+to2[i]+'</option>')
			}
		}
		var to3 = medicalTree["BRAIN"]["annotation"]["polygon"][currentObj.pathology_lvl1][currentObj.anatomy_lvl1];
		if(Object.size(to3) > 0){
			$('#tag-options3').show();
			to3 = sortedList(to3);
			for(i in to3){
				$('#tag-options3').append('<option value="'+to3[i]+'">'+to3[i]+'</option>')
			}
		}

		$('#tag-options2').val(currentObj.anatomy_lvl1)
		$('#tag-options3').val(currentObj.anatomy_lvl2)
		if(currentObj.severity == "critical"){
			$('#annotation-condition').prop('checked',true)
		}
		else{
			$('#annotation-condition').prop('checked',false)
		}
		$('#annotation-comment').val(currentObj.comments);
	})

	$(document).on('click','.annotation-view',function(){
		var $_this = $(this);
		var auid = $_this.parent().parent().attr('data-annotationid');
		if($_this.find('.zmdi').hasClass('zmdi-eye')){
			$_this.html('<i class="zmdi zmdi-eye-off"></i>')
			obj1.stage.find('#'+parseInt(auid))[0].hide();	
			$('#layer-items .annotation-item,#layer-items-2 .annotation-item').trigger('mouseleave')
		}
		else
		{
			$_this.html('<i class="zmdi zmdi-eye"></i>');
			obj1.stage.find('#'+parseInt(auid))[0].show();
		}
		
		obj1.layer_rect.draw();
		obj1.layer_rect.draw();
		obj1.layer_rect.draw();
	})

	$(document).on('click','#layer-items-2 .annotation-item .btn-delete',function(e){
		var $_this = $(this);
		var auid = $_this.parent().parent().attr('data-annotationid');
		var currentSlice = $('.img-thumbnail-small.active img');
		$('#new-tag').hide();
		var c = confirm('Are you sure?')
		if(c){
			$.ajax({
				method:"POST",
				url:base_url+"/apis/annotation/allstages/delete",
				data:JSON.stringify({
					annotation_id:parseInt(auid),
					stage:ACTION_REQUIRED
				}),
				beforeSend:function(){
					$_this.text('Wait..')
				},
				success:function(data){
					try{
						$_this.parent().parent().remove();
						obj1.stage.find('#'+parseInt(auid))[0].destroy();
						setTimeout(function(){
							obj1.layer_rect.draw();
						},10)
					}
					catch(e){
					}
					annotationObj = JSON.parse(unescape(currentSlice.attr('data-annotation')));
					if(Object.size(annotationObj)==0){
						annotationObj = {stage_1:[],stage_2:[],stage_3:[]}
					}
					var index = null;
					if(annotationObj["stage_1"] == undefined){
						annotationObj["stage_1"] = [];
					}
					if(annotationObj["stage_2"] == undefined){
						annotationObj["stage_2"] = [];
					}
					if(annotationObj["stage_3"] == undefined){
						annotationObj["stage_3"] = [];
					}
					$.grep(annotationObj["stage_"+ACTION_REQUIRED],function(ele,key){
						if(ele.annotation_id==auid){
							index = key;
						}
					})
					annotationObj["stage_"+ACTION_REQUIRED].splice(index,1);
					if(annotationObj["stage_1"].length==0 && annotationObj["stage_2"].length==0 && annotationObj["stage_3"].length==0)
					{
						currentSlice.parent().find('.top-mark.tagged').addClass('hide');
					}
					currentSlice.attr('data-annotation',escape(JSON.stringify(annotationObj)));
				}
			})
		}
		
	})

	$(document).on('click','.btn-annotation-verification',function(){
		var $_this = $(this);
		$_this.parent().find('.btn-annotation-verification').removeClass('active');
		$_this.addClass('active')
		if($_this.text()=="Valid"){
			$('#tag-options-v').val($('#new-tag-v').attr('data-pat1'))
			var an1 = medicalTree["BRAIN"]["annotation"]["polygon"][$('#new-tag-v').attr('data-pat1')];
			if(Object.size(an1) == 0){
				$('#tag-options2-v').hide();
				$('#tag-options3-v').hide();
			}
			else{
				$('#tag-options2-v').show();
				an1 = sortedList(an1);
				$('#tag-options2-v').append('<option value="select">Please Select</option>')
				for(i in an1){
					$('#tag-options2-v').append('<option value="'+an1[i]+'">'+an1[i]+'</option>')
				}
				$('#tag-options3-v').empty();
				var an2 = medicalTree["BRAIN"]["annotation"]["polygon"][$('#new-tag-v').attr('data-pat1')][$('#new-tag-v').attr('data-ana1')];
				if(Object.size(an2) == 0){
					$('#tag-options3-v').hide();
				}
				else{
					$('#tag-options3-v').show();
					an2 = sortedList(an2);
					$('#tag-options3-v').append('<option value="select">Please Select</option>')
					for(i in an2){
						$('#tag-options3-v').append('<option value="'+an2[i]+'">'+an2[i]+'</option>')
					}
				}
			}
			$('#tag-options2-v').val($('#new-tag-v').attr('data-ana1'))
			$('#tag-options3-v').val($('#new-tag-v').attr('data-ana2'))
			$('#tag-options-v').prop('disabled',true)
			$('#tag-options2-v').prop('disabled',true)
			$('#tag-options3-v').prop('disabled',true)
		}
		else{
			$('#tag-options-v').prop('disabled',false)
			$('#tag-options2-v').prop('disabled',false)
			$('#tag-options3-v').prop('disabled',false)
		}
	})

	$(document).on('click','.btn-series-verification',function(){
		var $_this = $(this);
		$_this.parent().find('.btn-series-verification').removeClass('active');
		$_this.addClass('active')
	})

	$(document).on('click','.btn-instance-verification',function(){
		var $_this = $(this);
		$_this.parent().find('.btn-instance-verification').removeClass('active');
		$_this.addClass('active')
		if($_this.text()=="Valid"){
			$('#pat1-v').val($('#instance-tag-box-v').attr('data-pat1'))
			$('#ana1-v').val($('#instance-tag-box-v').attr('data-ana1'))
			$('#ana2-v').val($('#instance-tag-box-v').attr('data-ana2'))
			$('#pat1-v').prop('disabled',true)
			$('#ana1-v').prop('disabled',true)
			$('#ana2-v').prop('disabled',true)
		}
		else{
			$('#pat1-v').prop('disabled',false)
			$('#ana1-v').prop('disabled',false)
			$('#ana2-v').prop('disabled',false)
		}
	})

	$(document).on('click','.feature-box',function(){
		var selected = $(this).hasClass('active');
		// $('.feature-box').removeClass('active');
		// if(selected){
		// 	$(this).removeClass('active');
		// }
		// else{
		// 	$(this).addClass('active');
		// }
	})

	$(document).on('click','#verify-series-submit',function(){
		var $_this = $(this);
		if($('.series-validation-grp').is(':visible')){
			// series info validation
			var st = $('.btn-series-verification.active').text();
			if($_this.attr('data-stage2-id') == undefined)
			{
				$.ajax({
					method:"POST",
					url:base_url+"/apis/verification/series/stage2/add",
					data:JSON.stringify({
						label:"normal",
						series_uuid:$('#folderid').val(),
						status:st,
						edit:(st=='Valid'?0:1),
						stage_1_series_info_id:parseInt($('.series-validation-grp').attr('data-stage1-id')),
						value:($('#series-info-validate').is(':checked')==true?"yes":""),
						comments:""
					}),
					beforeSend:function(){
						$_this.text('Wait')
					},
					success:function(data){
						$_this.text('Saved')
						$_this.attr('data-stage2-id',data.seriesinfo_uid)
						setTimeout(function(){
							$_this.text('Submit')
						},3000)
					}
				})
			}
			else{
				$.ajax({
					method:"POST",
					url:base_url+"/apis/verification/series/stage2/update",
					data:JSON.stringify({
						label:"normal",
						series_uuid:$('#folderid').val(),
						status:st,
						edit:(st=='Valid'?0:1),
						stage_1_series_info_id:parseInt($('.series-validation-grp').attr('data-stage1-id')),
						stage_2_series_info_id:parseInt($_this.attr('data-stage2-id')),
						value:($('#series-info-validate').is(':checked')==true?"yes":""),
						comments:""
					}),
					beforeSend:function(){
						$_this.text('Wait')
					},
					success:function(data){
						$_this.text('Saved')
						setTimeout(function(){
							$_this.text('Submit')
						},3000)
					}
				})
			}
		}
		else{
			// series info add
			if($_this.attr('data-stage2-id') == undefined){
				$.ajax({
					method:"POST",
					url:base_url+"/apis/series/allstages/add",
					data:JSON.stringify({
						label:"normal",
						series_uuid:$('#folderid').val(),
						value:($('#series-info-validate').is(':checked')==true?"yes":""),
						comments:"",
						stage:ACTION_REQUIRED
					}),
					beforeSend:function(){
						$_this.text('Wait')
					},
					success:function(data){
						$_this.text('Saved')
						$_this.attr('data-stage2-id',data.series_info_id)
						setTimeout(function(){
							$_this.text('Submit')
						},3000)
					}
				})
			}
			else{
				$.ajax({
					method:"POST",
					url:base_url+"/apis/series/allstages/update",
					data:JSON.stringify({
						label:"normal",
						series_uuid:$('#folderid').val(),
						series_info_id:$_this.attr('data-stage2-id'),
						value:($('#series-info-validate').is(':checked')==true?"yes":""),
						comments:"",
						stage:ACTION_REQUIRED
					}),
					beforeSend:function(){
						$_this.text('Wait')
					},
					success:function(data){
						$_this.text('Saved')
						setTimeout(function(){
							$_this.text('Submit')
						},3000)
					}
				})
			}
		}
	})

	$(document).on('click','.series-validation-btn',function(){
		var $_this = $(this);
		$_this.parent().find('.series-validation-btn').removeClass('active');
		$_this.addClass('active');
		var suid = $_this.attr('data-suid');
		var id = $_this.attr('data-id');
		var st = $_this.text().trim().toLowerCase();
		var dataObj = {
			stage_1_series_info_id:"",
			stage_2_series_info_id:"",
            series_uuid:$('#folderid').val(),
            status:st,
            edit:(st=="valid"?0:1),
            label:"normal",
            value:"",
            comments:"",
            verification_stage:""
		}
		switch($_this.attr('data-stage')){
			case "1":
				dataObj.stage_1_series_info_id = parseInt(id);
				dataObj.verification_stage = 1;
			break;

			case "2":
				dataObj.verification_stage = 2;
				dataObj.stage_2_series_info_id = parseInt(id);
			break;
		}
		if(suid==undefined){
			// verify series add
			$.ajax({
				method:"POST",
				url:base_url+"/apis/verification/series/stage3/add",
				data:JSON.stringify(dataObj),
				success:function(data){
					$_this.parent().parent().addClass('lbg3')
					$_this.parent().find('.series-validation-btn').attr('data-suid',data.seriesinfo_uid);
					nextBtnStatus();
				},
				error:function(){
					$_this.removeClass('active');
				}

			})
		}
		else{
			// verify series update
			dataObj["stage_3_series_info_id"] = parseInt(suid);
			$.ajax({
				method:"POST",
				url:base_url+"/apis/verification/series/stage3/update",
				data:JSON.stringify(dataObj),
				success:function(data){
				},
				error:function(){
					$_this.removeClass('active');
				}

			})
		}
	})

	$(document).on('click','a[data-target="#reportModal"]',function(){
		var report = $(this).attr('data-text');
		var date = $(this).attr('data-date')
		report = report.replace(/%0A/g, "<br/>");
		$('.report.modal-body').html('<p>'+unescape(report)+'</p>')
		$('.report.modal-title').html(date)
	})

	$(document).on('click','.submit-annotation-verification',function(){
		var $_this = $(this);
		var vuid = $_this.parent().parent().attr('data-verificationuid');
		var currentSlice = $('.img-thumbnail-small.active');
		var annUid = parseInt($_this.parent().parent().attr('data-annotationid'));
		var comment = $_this.parent().find('input[type="text"]').val();
		var status = $_this.parent().find('.btn-annotation-verification.active').text().trim().toLowerCase();
		if(vuid=="")
		{
			$.ajax({
				method:"POST",
				url:base_url+"/apis/verification/add",
				data:JSON.stringify({
					annotation_uid:annUid,
					status:status,
					comments:comment,
					folder_uid:$('#folderid').val()
				}),
				beforeSend:function(){
					$_this.text('Wait..')
				},
				success:function(data){
					$_this.text('Saved');
					setTimeout(function(){
						$_this.text('Submit')
					},2000)
					$_this.parent().parent().attr('data-verificationuid',data.verification_uid);
					var currentAnnotationObj = JSON.parse(unescape(currentSlice.find('img').attr('data-annotation')));
					$.grep(currentAnnotationObj,function(ele,key){
						if(ele.annotation_uid==annUid){
							ele["verification_data"] = {
								comments:comment,
								status:status,
								verification_uid:data.verification_uid
							}
						}
					})
					currentSlice.find('img').attr('data-annotation',escape(JSON.stringify(currentAnnotationObj)));
				}
			})
		}
		else{
			$.ajax({
				method:"POST",
				url:base_url+"/apis/verification/update",
				data:JSON.stringify({
					annotation_uid:annUid,
					status:status,
					comments:comment,
					verification_uid:parseInt(vuid),
					folder_uid:$('#folderid').val()
				}),
				beforeSend:function(){
					$_this.text('Wait..')
				},
				success:function(data){
					$_this.text('Saved');
					setTimeout(function(){
						$_this.text('Submit')
					},2000)
					var currentAnnotationObj = JSON.parse(unescape(currentSlice.find('img').attr('data-annotation')));
					$.grep(currentAnnotationObj,function(ele,key){
						if(ele.annotation_uid==annUid){
							ele["verification_data"] = {
								comments:comment,
								status:status,
								verification_uid:parseInt(vuid)
							}
						}
					})
					currentSlice.find('img').attr('data-annotation',escape(JSON.stringify(currentAnnotationObj)));
				}
			})
		}
	})

	getCT("current");
	
	$('#skip').click(function(){
		var r = confirm('DO NOT proceed without marking the SERIES DATA.\n Are you sure to continue?');
		if(r){
			$(this).text('Wait..');
			initialiseStage();
			picUpload('/static/img/default.jpg',[]);
			getCT("skip");
		}
	})

	$('#next').click(function(){
		var r = confirm('DO NOT proceed without marking the SERIES DATA.\n Are you sure to continue?')
		if(r){
			$(this).text('Wait..');
			initialiseStage();
			picUpload('/static/img/default.jpg',[]);
			getCT("next");
		}
	})
	$(document).keyup(function(e){
		if(e.keyCode==27){
			cancelDrawing();
		}
	})
	$('#btn-new-series-tag').click(function(){
		$('#series-tag-box').css('top',$('#btn-new-series-tag').offset().top+"px");
		$('#series-tag-box').css('left',$('#btn-new-series-tag').offset().left+"px");
		$('#series-tag-box').css('margin-left',"60px");
		$('#series-tag-box').show();
	})
	$('#btn-new-instance-tag').click(function(){
		$('#instance-tag-box').css('top',$('#btn-new-instance-tag').offset().top+"px");
		$('#instance-tag-box').css('left',$('#btn-new-instance-tag').offset().left+"px");
		$('#instance-tag-box').css('margin-left',"60px");
		$('#ana1').hide();
		$('input[name="slice_start_number"]').val('')
  		$('input[name="slice_end_number"]').val('')
		$('#instance-tag-box').show();
	})

	$('#cancel-series-btn').click(function(){
		$('#series-tag-box').hide();
		$('#series-op').val('select');
	})
	$('#cancel-instance-btn').click(function(){
		$('#instance-tag-box').hide();
		$('#pat1').val('select');
	})
	$('#cancel-verify-instance-btn').click(function(){
		$('#instance-tag-box-v').hide();
		$('#pat1-v').val('select');
	})

	$('#save-series-btn').click(function(){
		var $_this = $(this);
		var op = $('#series-op').val();
		if(op=="select"){

		}
		else{
			$.ajax({
				method:"POST",
				url:base_url+"/apis/series/allstages/add",
				data:JSON.stringify({
					series_uuid:$(folderid).val(),
                    label:op,
                    value:"yes",
                    comments:$('#series-comment').val(),
                    stage:ACTION_REQUIRED
				}),
				beforeSend:function(){
					$_this.text('Wait')
				},
				success:function(data){
					$_this.text('Save')
					$('#series-tag-box').hide();
					populateSeriesMyTags([{comments:$('#series-comment').val(),label:op,value:"yes",series_info_id:data.series_info_id}]);
					$('#series-comment').val('');
				}

			})
		}
	})

	$('#save-instance-btn').click(function(){
		if($('#pat1').val()=="select" || $('#ana1').val()=="select"){

		}
		else
		{
			var propagationStartPoint = parseInt($('#instance-tag-box input[name="slice_start_number"]').val()) - 1;
		  	var propagationEndPoint = parseInt($('#instance-tag-box input[name="slice_end_number"]').val()) - 1;
		  	for(i=propagationStartPoint;i<=propagationEndPoint;i++){
				var sliceNumber = i;
				var that_slice = $('.img-thumbnail-small:eq('+sliceNumber+')');
				addInstanceInfo($('#folderid').val(),that_slice,$('#pat1').val(),$('#ana1').val(),$('#instance-comment').val(),true)
			}

			addInstanceInfo($('#folderid').val(),$('.img-thumbnail-small.active'),$('#pat1').val(),$('#ana1').val(),$('#instance-comment').val(),false)
		}
	})

	$(document).on('click','.instance-item-delete',function(){
		var r = confirm('Are you sure?')
		if(r)
		{
			removeInstanceInfo($(this).parent().parent().attr('data-instance-info-id'),$(this),$('.img-thumbnail-small.active'));
		}
	})
	$('#verify-tag-save-btn').click(function(){
		// $.ajax({
		// 	url:base_url+"/apis/verification/annotations/stage3/update",
		// 	method:"POST",
		// 	data:JSON.stringify({
		// 		stage_3_annotation_id:119,
	 //            stage_2_annotation_id:"",
	 //            series_uuid:"77377b43-0aad-438c-8865-8b442bcda68b",
	 //            slice_id:213597,
	 //            stage_1_annotation_id:43790,
	 //            status:"valid",
	 //            comments:"test",
	 //            edit:1,
	 //            coords:[135.66197183098592, 151.43661971830986, 224, 171.26760563380282, 164.50704225352112, 333.5211267605634, 111.32394366197182, 325.40845070422534, 96.90140845070422, 280.3380281690141, 114.92957746478874, 209.1267605633803, 135.66197183098592, 153.2394366197183],
	 //            shape:"polygon",
	 //            pathology_lvl1:"Calvarial Fracture",
	 //            anatomy_lvl1:"NA",
	 //            anatomy_lvl2:"NA",
	 //            severity:""
		// 	}),
		// 	success:function(data){
		// 		console.log(data);
		// 	}
		// })

		var that = $(this);
		var currentSlice = $('.img-thumbnail-small.active img');
		var allStageIds = JSON.parse(unescape(that.attr('data-allstageids')));
		var dataObj = {
			stage_2_annotation_id:"",
            series_uuid:$('#folderid').val(),
            slice_id:parseInt(currentSlice.attr('data-sliceuid')),
            stage_1_annotation_id:"",
            status:"valid",
            comments:$('#annotation-verification-comment').val(),
            edit:1,
            coords:localToGlobalPoints(obj1.stage.find('#'+that.attr('data-id2'))[0].points()),
            shape:'polygon',
            pathology_lvl1:$('#tag-options-v').val(),
            anatomy_lvl1:$('#tag-options2-v').val() || "NA",
            anatomy_lvl2:$('#tag-options3-v').val() || "NA",
            severity:($('#annotation-verification-condition').is(':checked')==true?"critical":""),
            verification_stage:''
		}
		var current_number_of_lines = obj1.stage.find('Line').length;
		if(NUMBER_OF_ANNOTATIONS != current_number_of_lines){
			dataObj.coords = localToGlobalPoints(obj1.stage.find('Line')[obj1.stage.find('Line').length-1].points());
		}
		if(that.attr('data-needupdate')=="false"){
			switch(that.attr('data-taglevel')){
				// 1nv -> level 1 annotation, not verified yet
				// 1v1 -> level 1 annotation, verified stage is 1
				// 12nv -> level 1--2 annotation, not verified yet
				// 12v2 -> level 1--2 annotation, verified stage is 2 
				// 2nv -> level 2 annotation, not verified yet
				// 2v2 -> level 2 annotation, verified in stage 2
				case '1nv':
				dataObj.stage_1_annotation_id = allStageIds.stage1;
				dataObj.verification_stage = "1";
				break;

				case '12nv':
				dataObj.stage_1_annotation_id = allStageIds.stage1;
				dataObj.stage_2_annotation_id = allStageIds.stage2;
				dataObj.verification_stage = "2";
				break;

				case '2nv':
				dataObj.stage_2_annotation_id = allStageIds.stage2;
				dataObj.verification_stage = "2";
				break;
			}
			$.ajax({
				method:'POST',
				url:base_url+'/apis/verification/annotations/stage3/add',
				data:JSON.stringify(dataObj),
				beforeSend:function(){
					that.text('Wait')
				},
				success:function(data){
					console.log(data);
					currentSlice.attr('data-verified-count',(parseInt(currentSlice.attr('data-verified-count'))+1));
					if(currentSlice.attr('data-verified-count') == currentSlice.attr('data-total')){
						currentSlice.parent().find('.verification-done').removeClass('hide');
					}
					nextBtnStatus();
					that.text('Save')
					var currentObj = JSON.parse(unescape(currentSlice.attr('data-annotation')));
					switch(that.attr('data-taglevel')){
						case '1nv':
							$.grep(currentObj.stage_1,function(ele,key){
								if(ele.annotation_id==allStageIds.stage1){
									if(ele.stage_2 == undefined){
										ele["stage_2"] = {};
									}
									ele["stage_2"]["stage_3"] = {};
									ele["stage_2"]["stage_3"]["pathology_lvl1"] = dataObj.pathology_lvl1;
									ele["stage_2"]["stage_3"]["anatomy_lvl1"] = dataObj.anatomy_lvl1;
									ele["stage_2"]["stage_3"]["anatomy_lvl2"] = dataObj.anatomy_lvl2;
									ele["stage_2"]["stage_3"]["comments"] = dataObj.comments;
									ele["stage_2"]["stage_3"]["coords"] = dataObj.coords;
									ele["stage_2"]["stage_3"]["edit"] = 1;
									ele["stage_2"]["stage_3"]["severity"] = dataObj.severity;
									ele["stage_2"]["stage_3"]["shape"] = "polygon";
									ele["stage_2"]["stage_3"]["stage_3_annotation_id"] = data.stage_3_annotation_id;
									ele["stage_2"]["stage_3"]["status"] = "valid";
									ele["stage_2"]["stage_3"]["verification_stage"] = "1"
								}
							})
						break;

						case '12nv':
							$.grep(currentObj.stage_1,function(ele,key){
								if(ele.stage_2.stage_2_annotation_id==allStageIds.stage2){
									ele["stage_2"]["stage_3"] = {};
									ele["stage_2"]["stage_3"]["pathology_lvl1"] = dataObj.pathology_lvl1;
									ele["stage_2"]["stage_3"]["anatomy_lvl1"] = dataObj.anatomy_lvl1;
									ele["stage_2"]["stage_3"]["anatomy_lvl2"] = dataObj.anatomy_lvl2;
									ele["stage_2"]["stage_3"]["comments"] = dataObj.comments;
									ele["stage_2"]["stage_3"]["coords"] = dataObj.coords;
									ele["stage_2"]["stage_3"]["edit"] = 1;
									ele["stage_2"]["stage_3"]["severity"] = dataObj.severity;
									ele["stage_2"]["stage_3"]["shape"] = "polygon";
									ele["stage_2"]["stage_3"]["stage_3_annotation_id"] = data.stage_3_annotation_id;
									ele["stage_2"]["stage_3"]["status"] = "valid";
									ele["stage_2"]["stage_3"]["verification_stage"] = "2"
								}
							})
						break;

						case '2nv':
							$.grep(currentObj.stage_2,function(ele,key){
								if(ele.annotation_id==allStageIds.stage2){
									ele["stage_3"] = {};
									ele["stage_3"]["pathology_lvl1"] = dataObj.pathology_lvl1;
									ele["stage_3"]["anatomy_lvl1"] = dataObj.anatomy_lvl1;
									ele["stage_3"]["anatomy_lvl2"] = dataObj.anatomy_lvl2;
									ele["stage_3"]["comments"] = dataObj.comments;
									ele["stage_3"]["coords"] = dataObj.coords;
									ele["stage_3"]["edit"] = 1;
									ele["stage_3"]["severity"] = dataObj.severity;
									ele["stage_3"]["shape"] = "polygon";
									ele["stage_3"]["stage_3_annotation_id"] = data.stage_3_annotation_id;
									ele["stage_3"]["status"] = "valid";
									ele["stage_3"]["verification_stage"] = "2"
								}
							})
						break;
					}
					currentSlice.attr('data-annotation',escape(JSON.stringify(currentObj)))
					currentSlice.trigger('click');
				}
			})
		}
		else{
			switch(that.attr('data-taglevel')){
				// 1nv -> level 1 annotation, not verified yet
				// 1v1 -> level 1 annotation, verified stage is 1
				// 12nv -> level 1--2 annotation, not verified yet
				// 12v2 -> level 1--2 annotation, verified stage is 2 
				// 2nv -> level 2 annotation, not verified yet
				// 2v2 -> level 2 annotation, verified in stage 2
				case '1v1':
				//dataObj.stage_1_annotation_id = allStageIds.stage1;
				dataObj.verification_stage = "1";
				break;

				case '12v2':
				//dataObj.stage_1_annotation_id = allStageIds.stage1;
				//dataObj.stage_2_annotation_id = allStageIds.stage2;
				dataObj.verification_stage = "2";
				break;

				case '2v2':
				//dataObj.stage_2_annotation_id = allStageIds.stage2;
				dataObj.verification_stage = "2";
				break;
			}
			dataObj["stage_3_annotation_id"] = allStageIds.stage3;
			console.log(dataObj)
			$.ajax({
				method:'POST',
				url:base_url+'/apis/verification/annotations/stage3/update',
				data:JSON.stringify(dataObj),
				beforeSend:function(){
					that.text('Wait');
				},
				success:function(data){
					console.log(data);
					that.text('Save');
					var currentObj = JSON.parse(unescape(currentSlice.attr('data-annotation')));
					switch(that.attr('data-taglevel')){
						case '1v1':
							$.grep(currentObj.stage_1,function(ele,key){
								if(ele.annotation_id==allStageIds.stage1){
									ele["stage_2"]["stage_3"] = {};
									ele["stage_2"]["stage_3"]["pathology_lvl1"] = dataObj.pathology_lvl1;
									ele["stage_2"]["stage_3"]["anatomy_lvl1"] = dataObj.anatomy_lvl1;
									ele["stage_2"]["stage_3"]["anatomy_lvl2"] = dataObj.anatomy_lvl2;
									ele["stage_2"]["stage_3"]["comments"] = dataObj.comments;
									ele["stage_2"]["stage_3"]["coords"] = dataObj.coords;
									ele["stage_2"]["stage_3"]["edit"] = 1;
									ele["stage_2"]["stage_3"]["severity"] = dataObj.severity;
									ele["stage_2"]["stage_3"]["shape"] = "polygon";
									ele["stage_2"]["stage_3"]["stage_3_annotation_id"] = data.stage_3_annotation_id;
									ele["stage_2"]["stage_3"]["status"] = "valid";
									ele["stage_2"]["stage_3"]["verification_stage"] = "1"
								}
							})
						break;

						case '12v2':
							$.grep(currentObj.stage_1,function(ele,key){
								if(ele.stage_2.stage_2_annotation_id==allStageIds.stage2){
									ele["stage_2"]["stage_3"] = {};
									ele["stage_2"]["stage_3"]["pathology_lvl1"] = dataObj.pathology_lvl1;
									ele["stage_2"]["stage_3"]["anatomy_lvl1"] = dataObj.anatomy_lvl1;
									ele["stage_2"]["stage_3"]["anatomy_lvl2"] = dataObj.anatomy_lvl2;
									ele["stage_2"]["stage_3"]["comments"] = dataObj.comments;
									ele["stage_2"]["stage_3"]["coords"] = dataObj.coords;
									ele["stage_2"]["stage_3"]["edit"] = 1;
									ele["stage_2"]["stage_3"]["severity"] = dataObj.severity;
									ele["stage_2"]["stage_3"]["shape"] = "polygon";
									ele["stage_2"]["stage_3"]["stage_3_annotation_id"] = data.stage_3_annotation_id;
									ele["stage_2"]["stage_3"]["status"] = "valid";
									ele["stage_2"]["stage_3"]["verification_stage"] = "2"
								}
							})
						break;

						case '2v2':
							$.grep(currentObj.stage_2,function(ele,key){
								if(ele.annotation_id==allStageIds.stage2){
									ele["stage_3"] = {};
									ele["stage_3"]["pathology_lvl1"] = dataObj.pathology_lvl1;
									ele["stage_3"]["anatomy_lvl1"] = dataObj.anatomy_lvl1;
									ele["stage_3"]["anatomy_lvl2"] = dataObj.anatomy_lvl2;
									ele["stage_3"]["comments"] = dataObj.comments;
									ele["stage_3"]["coords"] = dataObj.coords;
									ele["stage_3"]["edit"] = 1;
									ele["stage_3"]["severity"] = dataObj.severity;
									ele["stage_3"]["shape"] = "polygon";
									ele["stage_3"]["stage_3_annotation_id"] = data.stage_3_annotation_id;
									ele["stage_3"]["status"] = "valid";
									ele["stage_3"]["verification_stage"] = "2"
								}
							})
						break;
					}
					currentSlice.attr('data-annotation',escape(JSON.stringify(currentObj)))
					currentSlice.trigger('click');
				}
			})
		}
	})
	// $('#verify-tag-save-btn').click(function(){
	// 	var currentSlice = $('.img-thumbnail-small.active img');
	// 	var $_this = $(this);
	// 	if($('#tag-options-v').val()=="select" || $('#tag-options2-v').val()=="select" || $('#tag-options3-v').val()=="select"){

	// 	}
	// 	else{
	// 		if($_this.attr('data-type')=="add"){
	// 			var comm = $('#annotation-verification-comment').val()
	// 			$.ajax({
	// 				method:"POST",
	// 				url:base_url+"/apis/verification/annotations/stage2/add",
	// 				data:JSON.stringify({
	// 					series_uuid:$('#folderid').val(),
	// 					slice_id:parseInt($('.img-thumbnail-small.active img').attr('data-sliceuid')),
	// 					stage_1_annotation_id:parseInt($('#new-tag-v').attr('data-auid')),
	// 					status:st,
	// 					comments:comm,
	// 					edit:(st=="Valid"?0:1),
	// 					coords:localToGlobalPoints(line2.points()),
	// 					shape:"polygon",
	// 					pathology_lvl1:$('#tag-options-v').val(),
	// 					anatomy_lvl1:$('#tag-options2-v').val(),
	// 					anatomy_lvl2:$('#tag-options3-v').val(),
	// 					severity:($('#annotation-verification-condition').is(':checked')==true?"critical":"")
	// 				}),
	// 				beforeSend:function(){
	// 					$_this.text('Wait..')
	// 				},
	// 				success:function(data){
	// 					nextBtnStatus();
	// 					$_this.text('Save')
	// 					$('#new-tag-v').hide();
	// 					currentSlice.attr('data-verified-count',parseInt(currentSlice.attr('data-verified-count'))+1);
	// 					(currentSlice.attr('data-verified-count') == currentSlice.attr('data-total')? currentSlice.parent().find('.verification-done').removeClass('hide'):"");
	// 					$('#annotation-verification-comment').val('');
	// 					var line_temp = new Konva.Line({
	// 						points: line2.points(),
	// 						stroke: 'red',
	// 						tension: 0,
	// 						strokeWidth:1,
	// 						shadowColor:'white',
	// 						shadowBlur:1,
	// 						draggable:false,
	// 						id:data.stage_2_annotation_id
	// 					});
	// 					obj1.layer_rect.add(line_temp);
	// 					obj1.layer_rect.draw();
	// 					$('.feature-box.annotation-item[data-annotationid="'+parseInt($('#new-tag-v').attr('data-auid'))+'"]').addClass('lbg3')
	// 					$('.feature-box.annotation-item[data-annotationid="'+parseInt($('#new-tag-v').attr('data-auid'))+'"] .btn-edit').attr('data-verified','1')
	// 					$('.feature-box.annotation-item[data-annotationid="'+parseInt($('#new-tag-v').attr('data-auid'))+'"]').after('<div class="verified-annotation-box custom-tooltip" data-toggle="tooltip" data-placement="left" data-annotationid="'+data.stage_2_annotation_id+'" title="'+$('#tag-options-v').val()+' / '+$('#tag-options2-v').val()+' / '+$('#tag-options3-v').val()+'"><div class="elips d-inline-block" style="width:65%;float:left">'+$('#tag-options-v').val()+' / '+$('#tag-options2-v').val()+' / '+$('#tag-options3-v').val()+'</div><div style="padding:4px 8px"><button class="annotation-view"><i class="zmdi zmdi-eye"></i> Hide</button></div></div>');
						
	// 					var obj = JSON.parse(unescape(currentSlice.attr('data-annotation')));
	// 					$.grep(obj["stage_1"],function(ele,key){
	// 						if(ele.annotation_id ==	$('#new-tag-v').attr('data-auid')){
	// 							ele["stage_2"] = {
	// 								coords:localToGlobalPoints(line2.points()),
	// 								stage_2_annotation_id:data.stage_2_annotation_id,
	// 								pathology_lvl1:$('#tag-options-v').val(),
	// 								anatomy_lvl1:$('#tag-options2-v').val(),
	// 								anatomy_lvl2:$('#tag-options3-v').val(),
	// 								status:st,
	// 								edit:(st=="Valid"?0:1),
	// 								comments:comm,
	// 								severity:($('#annotation-verification-condition').is(':checked')==true?"critical":"")
	// 							}
	// 						}
	// 					})
	// 					currentSlice.attr('data-annotation',escape(JSON.stringify(obj)));
	// 					line2.destroy();
	// 					line2 = new Konva.Line({
	// 						x: 0,
	// 						y: 0,
	// 						points: [],
	// 						stroke: 'red',
	// 						tension: 0,
	// 						strokeWidth:1,
	// 						shadowColor:'white',
	// 						shadowBlur:1,
	// 						draggable:false
	// 					});
	// 					$('[data-toggle="tooltip"]').tooltip({
	// 						trigger : 'hover'
	// 					});
	// 				}
	// 			})
	// 		}
	// 		if($_this.attr('data-type')=="update"){
	// 			$.ajax({
	// 				method:"POST",
	// 				url:base_url+"/apis/verification/annotations/stage2/update",
	// 				data:JSON.stringify({
	// 					series_uuid:$('#folderid').val(),
	// 					slice_id:parseInt($('.img-thumbnail-small.active img').attr('data-sliceuid')),
	// 					stage_1_annotation_id:parseInt($('#new-tag-v').attr('data-auid')),
	// 					stage_2_annotation_id:parseInt($('#new-tag-v').attr('data-auid2')),
	// 					status:st,
	// 					comments:$('#annotation-verification-comment').val(),
	// 					edit:(st=="Valid"?0:1),
	// 					coords:localToGlobalPoints(line2.points()),
	// 					shape:"polygon",
	// 					pathology_lvl1:$('#tag-options-v').val(),
	// 					anatomy_lvl1:$('#tag-options2-v').val(),
	// 					anatomy_lvl2:$('#tag-options3-v').val(),
	// 					severity:($('#annotation-verification-condition').is(':checked')==true?"critical":"")
	// 				}),
	// 				beforeSend:function(){
	// 					$_this.text('Wait..')
	// 				},
	// 				success:function(data){
	// 					$_this.text('Save')
	// 					$('#new-tag-v').hide();
	// 					obj1.stage.find('#'+parseInt($('#new-tag-v').attr('data-auid2')))[0].destroy();
	// 					var line_temp = new Konva.Line({
	// 						points: line2.points(),
	// 						stroke: 'red',
	// 						tension: 0,
	// 						strokeWidth:1,
	// 						shadowColor:'white',
	// 						shadowBlur:1,
	// 						draggable:false,
	// 						id:data.stage_2_annotation_id
	// 					});
	// 					obj1.layer_rect.add(line_temp);
	// 					obj1.layer_rect.draw();
	// 					$('.feature-box.annotation-item[data-annotationid="'+parseInt($('#new-tag-v').attr('data-auid'))+'"] .btn-edit').attr('data-verified','1')
	// 					$('.feature-box.annotation-item[data-annotationid="'+parseInt($('#new-tag-v').attr('data-auid'))+'"] + .verified-annotation-box').remove();
	// 					$('.feature-box.annotation-item[data-annotationid="'+parseInt($('#new-tag-v').attr('data-auid'))+'"]').addClass('lbg3');
	// 					$('.feature-box.annotation-item[data-annotationid="'+parseInt($('#new-tag-v').attr('data-auid'))+'"]').after('<div class="verified-annotation-box custom-tooltip" data-toggle="tooltip" data-placement="left" data-annotationid="'+data.stage_2_annotation_id+'" title="'+$('#tag-options-v').val()+' / '+$('#tag-options2-v').val()+' / '+$('#tag-options3-v').val()+'"><div class="elips d-inline-block" style="width:65%;float:left">'+$('#tag-options-v').val()+' / '+$('#tag-options2-v').val()+' / '+$('#tag-options3-v').val()+'</div><div style="padding:4px 8px"><button class="annotation-view"><i class="zmdi zmdi-eye"></i> Hide</button></div></div>');
						
	// 					var obj = JSON.parse(unescape(currentSlice.attr('data-annotation')));
	// 					$.grep(obj["stage_1"],function(ele,key){
	// 						if(ele.annotation_id ==	$('#new-tag-v').attr('data-auid')){
	// 							ele["stage_2"] = {
	// 								coords:localToGlobalPoints(line2.points()),
	// 								stage_2_annotation_id:data.stage_2_annotation_id,
	// 								pathology_lvl1:$('#tag-options-v').val(),
	// 								anatomy_lvl1:$('#tag-options2-v').val(),
	// 								anatomy_lvl2:$('#tag-options3-v').val(),
	// 								status:st,
	// 								edit:(st=="Valid"?0:1)
	// 							}
	// 						}
	// 					})
	// 					currentSlice.attr('data-annotation',escape(JSON.stringify(obj)));
	// 					line2.destroy();
	// 					line2 = new Konva.Line({
	// 						x: 0,
	// 						y: 0,
	// 						points: [],
	// 						stroke: 'red',
	// 						tension: 0,
	// 						strokeWidth:1,
	// 						shadowColor:'white',
	// 						shadowBlur:1,
	// 						draggable:false
	// 					});
	// 					$('[data-toggle="tooltip"]').tooltip({
	// 						trigger : 'hover'
	// 					});
	// 				}
	// 			})
	// 		}
	// 	}	
	// })
})




function getCT(type){
	var url = '';
	switch(type){
		case "current":
		url = base_url+"/apis/tagging/serve/current?email=farhan@paralleldots.com";
		break;
		case "next":
		url = base_url+"/apis/tagging/serve/next?email=farhan@paralleldots.com";
		break;

		case "folder":
		url = base_url+"/apis/tagging/serve/deacc97b-a63a-4f08-9bcb-95f1c63792cc";
		break;

		case "skip":
		url = base_url+"/apis/tagging/serve/skip";
	}
	$.ajax({
		method:"GET",
		url:url,
		global:false,
		beforeSend:function(){
			$('.load-error').addClass('hide');
			$('#thumbnail-panel').empty();
			instanceArray = [];
			$('#loader-div').css('display','flex');
			$('#loader-div #msg-loader').html('Loading 0.00%');
			$('#loader-div .progress-bar').attr('aria-valuenow','0');
            $('#loader-div .progress-bar').css('width','0%')
		},
		success:function(data){
			//$('#loader-div').hide();	
			// ACTION_REQUIRED = data.stage;
			ACTION_REQUIRED = 3;
			$('.tag-counter').show();
			$('.v-tags').show();
			setVerifiedCount(data.data.series);
			$('#tagging-status').html('<i class="zmdi zmdi-dot-circle" style="color:#f00"></i> FINAL VERIFICATION');

			// if(data.data.series.series_info.stage_1)
			// {
			// 	$('.series-validation-grp').show();
			// 	$('.series-validation-grp').attr('data-stage1-id',data.data.series.series_info.stage_1[0].series_info_id)
			// 	if(data.data.series.series_info.stage_1[0].value=="yes")
			// 	{
			// 		$('#series-normal').prop('checked',true)
			// 	}
			// 	else{
			// 		$('#series-normal').prop('checked',false)
			// 	}
			// 	if(data.data.series.series_info.stage_2){
			// 		$('#verify-series-submit').attr('data-stage2-id',data.data.series.series_info.stage_2[0].series_info_id)
			// 		if(data.data.series.series_info.stage_2[0].value == "yes"){
			// 			$('#series-info-validate').prop('checked',true)
			// 		}
			// 		else{

			// 			$('#series-info-validate').prop('checked',false);
			// 		}
			// 	}
			// }
			// else{
			// 	$('.series-validation-grp').hide();
			// 	$('#series-normal').prop('checked',false)
			// }
			medicalTree = JSON.parse(JSON.stringify(data.meta.medical_tree));
			var instance_options = medicalTree.BRAIN.instance.boolean;
			var l = medicalTree.BRAIN.annotation.polygon;
			populateSeriesBox(data.data.series.series_info);
			$('#ana1,#tag-options2,#tag-options3,#tag-options2-v,#tag-options3-v,#ana1-v').hide();
			$('#tag-options,#tag-options2,#tag-options3,#pat1,#ana1,#tag-options-v,#pat1-v,#ana1-v').empty();
			$('#tag-options,#pat1,#tag-options-v,#pat1-v').html('<option value="select" selected="">Please select</option>');
			l = sortedList(l);
			for(i in l){
				$('#tag-options,#tag-options-v').append('<option value="'+l[i]+'">'+l[i]+'</option>');
			}
			instance_options = sortedList(instance_options);
			for(i in instance_options){
				$('#pat1,#pat1-v').append('<option value="'+instance_options[i]+'">'+instance_options[i]+'</option>')
			}
			$('#next').text('Save & Next');
			$('#skip').text('Skip')
			var ctr = 0;
			$('#folderid').val(data.data.series.series_uuid);
			$('#seriesType').html(data.data.series.study_description+" | "+data.data.series.series_description);
			$('#patientId').html("Patient ID: "+data.data.series.patient_id);
			$('#seriesid').html("Series ID: "+data.data.series.series_uuid)
			$('#patientage').html("Age: "+data.data.series.patient_age)
			$('#patientgender').html("Gender: "+data.data.series.patient_sex)
			getReports(data.data.series.patient_id);
			var inst = data.data.series.instances;
			//populateSliceBox(data.data.tagging_meta.instance_tags)
			for(i in inst){
				instanceArray.push(inst[i]);
			}
			var slice_number = 0;
		 	for(i in instanceArray){
		 		var l = instanceArray[i].annotations || {};
		 		var m = instanceArray[i].instance_info || {};
		 		var n = 0;

		  		$('#thumbnail-panel').append('<div data-slicenumber="'+slice_number+'" title="Click to analyse" class="custom-tooltip top img-thumbnail-small">'+
		    		'<div class="top-mark tagged '+(Object.size(l)>0?"":"hide")+'"></div><div class="top-mark series '+(n>0?"":"hide")+'"></div><div class="top-mark instance '+(Object.size(m)>0?"":"hide")+'"></div><div class="verification-done'+((instanceArray[i].Total_verified_annotation!=instanceArray[i].Total_annotations) || instanceArray[i].Total_annotations==0 || instanceArray[i].Total_annotations == undefined?" hide":"")+'"><i class="zmdi zmdi-check-circle" style="color: #00ff3b;background: black;"></i></div><img class="img128" data-instance="'+escape(JSON.stringify(m))+'" data-annotation="'+escape(JSON.stringify(l))+'" data-sliceuid="'+instanceArray[i].slice_id+'" data-verified-count="'+instanceArray[i].Total_verified_annotation+'" data-total="'+instanceArray[i].Total_annotations+'" data-dicomurl="'+instanceArray[i].dcmlink+'" src="'+instanceArray[i].jpglink_brain_window+'">'+
		    	'<span class="slice-number-icon">'+(slice_number+1)+'</span></div>');
		    	slice_number++;
			}
			var imgList = $('.img-thumbnail-small');
			loadAll();
			$('.img-thumbnail-small').click(function(){
				
				cancelDrawing();
				
				var $_that = $(this);
				$('#sliceid').html('Slice ID: '+$_that.find('img').attr('data-sliceuid'))
				$('#layer-items,#layer-items-2').empty();
				$('#new-tag').hide();
				$('#instance-tag-box-v,#new-tag-v').hide();

				$('.img-thumbnail-small').removeClass('active');
				$_that.addClass('active')
				$('.slice-number-icon').removeClass('active');
				$_that.find('.slice-number-icon').addClass('active');
				
				populateSliceBox(JSON.parse(unescape($_that.find('img').attr('data-instance'))))
				
				initialiseStage();

				picUpload($_that.find('img').attr('src'),JSON.parse(unescape($_that.find('img').attr('data-annotation'))));
				
				var dcmurl = $_that.find('img').attr("data-dicomurl");
				cornerstone.loadAndCacheImage("wadouri:"+dcmurl).then(function(image){
		        	downloadAndViewURL(dcmurl);
		        })
		        ctr = $_that.attr('data-slicenumber');
		        var start_min = parseInt(ctr)+2;
		        $('input[type="number"]').attr('min',(start_min>imgList.length?imgList.length:start_min));
		        $('input[name="number"]').attr('max',imgList.length);
		        if($('.point-tool').hasClass('active'))	
				{
					$('.point-tool').trigger('click');
					$('.point-tool').trigger('click');
				}
			})
			$('.img-thumbnail-small:eq(0)').trigger('click');
			var flag = true;
			var scrollevt =(/Firefox/i.test(navigator.userAgent))? "DOMMouseScroll" : "mousewheel"
			$('.main-view-wrapper').bind(scrollevt, function(e){
				var direction;
				if(scrollevt == "DOMMouseScroll")
				{
					direction = e.originalEvent.detail;
				}
				else
				{
					direction = e.originalEvent.wheelDelta;
				}
				if($('.zoom-btn').hasClass('active') == false && flag==true)
				{
					if(direction < 0) 
					{
						//scroll down
						ctr--;
						if(ctr<0)
						{
							ctr = 0 ;
						}
					}
					else 
					{
						//scroll up
						ctr++
						if(ctr>=imgList.length){
							ctr = imgList.length-1;
						}
					}
					$('#thumbnail-panel').scrollLeft(ctr*40);
					$(imgList[ctr]).find('img').trigger('click');
					flag = false;
					setTimeout(function(){
						flag = true;
					},150)
				}
				if($('.zoom-btn').hasClass('active')){
					if(direction < 0) 
					{
						//scroll down
						obj1.scale -= 0.02;
						if(obj1.scale<0.1){
							obj1.scale = 0.1
						}
						var scaleCSS = "scale("+obj1.scale+")";
						$('.main-view-parent').css('transform',scaleCSS);
					}
					else 
					{
						if(obj1.scale>2){
							obj1.scale = 2
						}
						obj1.scale += 0.02;
						var scaleCSS = "scale("+obj1.scale+")";
						$('.main-view-parent').css('transform',scaleCSS);
					}
				}
				return false;
					//prevent page fom scrolling
			});
			$(window).keyup(function(e){
				var keyC = e.keyCode;
				if(keyC==37){
					ctr--;
					if(ctr<0)
					{
						ctr = 0 ;
					}
					$('#thumbnail-panel').scrollLeft(ctr*40);
					$(imgList[ctr]).find('img').trigger('click');
				}
				if(keyC==39){
					ctr++
					if(ctr>=imgList.length){
						ctr = imgList.length-1;
					}
					$('#thumbnail-panel').scrollLeft(ctr*40);
					$(imgList[ctr]).find('img').trigger('click');
				}
			})
		},
		error:function(data){
			$('.load-error').removeClass('hide');
		}
	})
}



function filter(cvalue,bvalue,obj){
	try
	{
		var xS=obj.img.getX();
		var yS=obj.img.getY();
		var rW=obj.img.getWidth();
		var rH=obj.img.getHeight();
		ctx=obj.img.getContext();
		imageDataInvert = ctx.getImageData(xS, yS, rW,rH);
	    var data = imageDataInvert.data;

	    d=obj.dst.data;
	    //console.log(d)
	    dI=obj.dstI.data;
	    //factor = (259 * (value + 255)) / (255 * (259 - value));
	    cvalue = (parseFloat(cvalue) || 0) + 1;
	    if(obj.isInvert){
	    	for(var i = 0; i < data.length; i += 4) 
		    {  
				r = parseInt(dI[i])+parseInt(bvalue);
		    	g = parseInt(dI[i+1])+parseInt(bvalue);
		    	b = parseInt(dI[i+2])+parseInt(bvalue);
		    	data[i] = (((((r)/255) - 0.5)*cvalue) + 0.5) * 255;
		    	data[i+1] = (((((g)/255) - 0.5)*cvalue) + 0.5) * 255;
		    	data[i+2] = (((((b)/255) - 0.5)*cvalue) + 0.5) * 255;
		    }
	    }
	    else
	    {
	    	for(var i = 0; i < data.length; i += 4) 
		    {  
		    	r = parseInt(d[i])+parseInt(bvalue);
		    	g = parseInt(d[i+1])+parseInt(bvalue);
		    	b = parseInt(d[i+2])+parseInt(bvalue);
		  		data[i] = (((((r)/255) - 0.5)*cvalue) + 0.5) * 255;
		    	data[i+1] = (((((g)/255) - 0.5)*cvalue) + 0.5) * 255;
		    	data[i+2] = (((((b)/255) - 0.5)*cvalue) + 0.5) * 255;
		    }
		}
	    ctx.putImageData(imageDataInvert, xS, yS);
	}
	catch(e)
	{}
}


function addNewTag(that,sliceUid,tag,tag2,tag3,sliceNumber,coords,isPropagated,comments,shape,annotation_condition,verificationUid){ // receives coords corresponding to local system
	coords = localToGlobalPoints(coords); // coords scaled to original image size
	$.ajax({
		method: "POST",
		url: base_url+"/apis/annotation/allstages/add",
		data:JSON.stringify({
			email:"farhan@paralleldots.com",
			coords:coords,
			pathology_lvl1:tag,
			anatomy_lvl1:tag2,
			anatomy_lvl2:tag3,
			comments:comments,
			shape:shape,
			severity:annotation_condition,
			color:"NA",
			series_uuid:$('#folderid').val(),
			slice_id:sliceUid,
			stage:parseInt(ACTION_REQUIRED)
		}),
		beforeSend:function(){
			that.text("Wait..")
		},
		success:function(data){
			$('.img-thumbnail-small:eq('+sliceNumber+')').find('.top-mark.tagged').removeClass('hide');
			that.text("Saved")
			setTimeout(function(){
				$('#new-tag').hide();
				$('#annotation-condition').prop('checked',false)
				that.text("Save");
				$('input[name="slice_start_number"]').val('')
  				$('input[name="slice_end_number"]').val('')
  				$('#annotation-comment').val('');
			},0)
			if(shape == "polygon")
			{
				line2.destroy();
				line2 = new Konva.Line({
					x: 0,
					y: 0,
					points: [],
					stroke: 'red',
					tension: 0,
					strokeWidth:1,
					shadowColor:'white',
					shadowBlur:1,
					draggable:false
				});
			}
			if(shape == "point"){
				circle_temp.destroy();
  				circle_temp = new Konva.Circle({
  					radius:5,
  					stroke:'red',
  					strokeWidth:2
  				})
			}
			drawAnnotation(coords,tag,tag2,tag3,data.annotation_id,true,sliceNumber,isPropagated,comments,shape,annotation_condition,verificationUid,"mytags",undefined,ACTION_REQUIRED); // coords corresponds to original image size
		},
		error:function(){
			that.text("Error! Try again");
		}
	})
}


function initialiseStage(){
	obj1.canvasHeight = $('#main-view').height();
	obj1.canvasWidth = $('#main-view').width();
	obj1.zoomLevel = 1.5;
	obj1.point = false;
	//obj1.stage.destroy();
	//obj1.layer_main.destroy();
	obj1.layer_rect.destroy();
	obj1.layer_zoom.destroy();
	obj1.layer_zoom2.destroy();
	// if(obj1.stage == null)
	// {
	// 	obj1.stage = new Konva.Stage({
	// 		container: 'main-view',   // id of container <div>
	// 		width: obj1.canvasWidth,
	// 		height: obj1.canvasHeight
	// 	});
	// }
	

	obj1.stage_zoom = new Konva.Stage({
		container: 'zoom-view',   // id of container <div>
		width: 300,
		height: 250
	})
	obj1.layer_rect = new Konva.Layer();
	obj1.layer_zoom = new Konva.Layer();
	obj1.layer_zoom2 = new Konva.Layer();
	circle_temp.destroy();
	circle_temp = new Konva.Circle({
		strokeWidth:2,
		stroke:'red',
		radius:5
	})
	obj1.layer_rect.add(circle_temp);
	circle_temp.hide();
	var isDrawing = false;
	obj1.stage.on('mousemove',function(){
		var coordX = Math.round((obj1.stage.getPointerPosition().x - obj1.xStart)*(obj1.imageWidth/obj1.renderableWidth));
		var coordY = Math.round((obj1.stage.getPointerPosition().y-obj1.yStart)*(obj1.imageHeight/obj1.renderableHeight));
		str = ''
		str += "X: "+coordX+" px Y: "+coordY+" px Value:"+((hounsfieldArray[coordY*(obj1.imageHeight) + coordX]*slope)+intercept);
		$('#hounsfield').html(str);
		if($('.zoom').hasClass('active'))
		{
			var x = obj1.stage.getPointerPosition().x;
			var y = obj1.stage.getPointerPosition().y;
			zoomView();
		}
	});
	var x_current,y_current;
	if(obj1.layer_main==null){
		obj1.layer_main = new Konva.Layer({
			strokeWidth:3,
			stroke:'red'
		});
		obj1.layer_main.on('click',function()
		{
			if($('.multipoint-tool').hasClass('active'))
			{
				if(obj1.multipoint==true)
				{
					x_current = obj1.stage.getPointerPosition().x/obj1.scale;
					y_current = obj1.stage.getPointerPosition().y/obj1.scale;
					if(a==0)
					{
						startingPoint.setX(x_current)
						startingPoint.setY(y_current)
						obj1.layer_rect.add(startingPoint);
					}
					a+=1;
					line2.points(line2.points().concat([x_current,y_current]));
					obj1.layer_rect.add(line2);                             //=========================================
					obj1.layer_rect.draw();                             //=========================================
					obj1.multimove=true;
					if(Math.abs(line2.points()[0]-x_current)<5 && Math.abs(line2.points()[1]-y_current)<5 && a>1){                       // polygon finish on layer1
						if(obj1.multimove)
						{
							obj1.multipoint=false;
							obj1.multimove=false;
							line.destroy();
							a = 0;		
							startingPoint.remove();
							obj1.layer_rect.draw();
							$('.multipoint-tool').trigger('click');
							$('.multipoint-tool').trigger('click');		
							$('#new-tag').attr('data-tag-type','new-multipoint');
							showTagBox((obj1.stage.getPointerPosition().x/obj1.scale + $('#main-view').offset().left),(obj1.stage.getPointerPosition().y/obj1.scale + $('#main-view').offset().top+20));	
						}
					}
					if(a==1)
					{
						// $('#status').text('Press ESC to exit tagging.');
						// $('#status').animate({opacity:1},400,function(){
						// 	setTimeout(function(){
						// 		$('#status').animate({opacity:0},200);
						// 	},4000)
						// });
					}
				}
			}
			if($('.point-tool').hasClass('active')){
				if(obj1.point == true){

					var x_current_point = obj1.stage.getPointerPosition().x/obj1.scale;
					var y_current_point = obj1.stage.getPointerPosition().y/obj1.scale;
					
					circle_temp.setX(x_current_point);
					circle_temp.setY(y_current_point);
					obj1.layer_rect.add(circle_temp)
					circle_temp.show();
					obj1.layer_rect.draw();
					$('#new-tag').attr('data-tag-type','new-point');
					showTagBox(x_current_point + $('#main-view').offset().left,y_current_point + $('#main-view').offset().top+20);
				}
			}
		});

		obj1.layer_main.on('mousemove',function(){
			if(obj1.multipoint==true && obj1.multimove==true)
			{
				line.points([]);
				var x = obj1.stage.getPointerPosition().x/obj1.scale;
				var y = obj1.stage.getPointerPosition().y/obj1.scale;
				line.points(line.points().concat([x,y]));
				obj1.layer_rect.add(line);                             //=========================================
				obj1.layer_rect.draw();                             //=========================================




				x1=x_current;
				y1=y_current;
				x2=obj1.stage.getPointerPosition().x/obj1.scale;
				y2=obj1.stage.getPointerPosition().y/obj1.scale;

				lineCoord=lineCoordinates(x1,y1,x2,y2);


				line.points([]);
				line.points(line.points().concat(lineCoord));
				obj1.layer_rect.add(line);                             //=========================================
				obj1.layer_rect.draw();                             //=========================================
			}
		})
	}
	obj1.layer_rect.on('mousemove',function(){
		if($('.zoom').hasClass('active'))
		{
			var x = obj1.stage.getPointerPosition().x;
			var y = obj1.stage.getPointerPosition().y;
			zoomView();
		}
	});
	

	obj1.layer_rect.on('click',function(){
		if($('.multipoint-tool').hasClass('active'))
		{
			if(obj1.multipoint==true)
			{
				a+=1;
				x_current = obj1.stage.getPointerPosition().x/obj1.scale;
				y_current = obj1.stage.getPointerPosition().y/obj1.scale;
				line2.points(line2.points().concat([x_current,y_current]));
				obj1.layer_rect.add(line2);                             //=========================================
				obj1.layer_rect.draw();                             //=========================================
				obj1.multimove=true;
				
				if(Math.abs(line2.points()[0]-x_current)<5 && Math.abs(line2.points()[1]-y_current)<5 && a>1){                       // polygon finish on layer1
					if(obj1.multimove)
					{
						obj1.multipoint=false;
						obj1.multimove=false;
						line.destroy();
						a = 0;		
						$('.multipoint-tool').trigger('click');
						$('.multipoint-tool').trigger('click');
						$('#new-tag').attr('data-tag-type','new-multipoint');			
						showTagBox((obj1.stage.getPointerPosition().x/obj1.scale + $('#main-view').offset().left),(obj1.stage.getPointerPosition().y/obj1.scale + $('#main-view').offset().top+20));
					}
				}
				if(a==1)
				{
					// $('#status').text('Press ESC to exit tagging.');
					// $('#status').animate({opacity:1},400,function(){
					// 	setTimeout(function(){
					// 		$('#status').animate({opacity:0},200);
					// 	},4000)
					// });
				}
			}
		}
	});

	// obj1.layer_rect.on('mousemove',function(){
	// 	if(obj1.multipoint==true && obj1.multimove==true)
	// 	{
	// 		var x = obj1.stage.getPointerPosition().x;
	// 		var y = obj1.stage.getPointerPosition().y;
	// 		line.points(line.points().concat([x,y]));
	// 		obj1.layer_rect.add(line);                             //=========================================
	// 		obj1.layer_rect.draw();                             //=========================================




	// 		x1=x_current;
	// 		y1=y_current;
	// 		x2=obj1.stage.getPointerPosition().x;
	// 		y2=obj1.stage.getPointerPosition().y;

	// 		lineCoord=lineCoordinates(x1,y1,x2,y2);


	// 		line.points([]);
	// 		line.points(line.points().concat(lineCoord));
	// 		obj1.layer_rect.add(line);                             //=========================================
	// 		obj1.layer_rect.draw();                             //=========================================
	// 	}
	// })
}


function picUpload(e,data){  // receives coords corresponding to original image size
	var imageObj = new Image;
	imageObj.crossOrigin="anonymous";
	imageObj.src = e;
	var xStart,yStart,renderableHeight,renderableWidth;
	imageObj.onload = function() 
	{
		var imageDimensionRatio = imageObj.width / imageObj.height;
		var canvasDimensionRatio = obj1.canvasWidth / obj1.canvasHeight;
		if(imageDimensionRatio < canvasDimensionRatio) 
		{
			renderableHeight = obj1.canvasHeight;
			renderableWidth = imageObj.width * (renderableHeight / imageObj.height);
			xStart = (obj1.canvasWidth - renderableWidth) / 2;
			yStart = 0;
		} 
		else if(imageDimensionRatio > canvasDimensionRatio) 
		{
			renderableWidth = obj1.canvasWidth;
			renderableHeight = imageObj.height * (renderableWidth / imageObj.width);
			xStart = 0;
			yStart = (obj1.canvasHeight - renderableHeight) / 2;
		} 
		else 
		{
			renderableHeight = obj1.canvasHeight;
			renderableWidth = obj1.canvasWidth;
			xStart = 0;
			yStart = 0;
		}
		obj1.xStart = xStart;
		obj1.yStart = yStart;
		obj1.renderableHeight = renderableHeight;
		obj1.renderableWidth = renderableWidth;
		obj1.imageHeight = imageObj.height;
		obj1.imageWidth = imageObj.width;
		if(obj1.img==null)
		{
			obj1.img = new Konva.Image({
				x: xStart,
				y: yStart,
				image: imageObj,
				height:renderableHeight,
				width:renderableWidth
			});
			obj1.imgZoom = new Konva.Image({
				x: xStart,
				y: yStart,
				image: imageObj,
				height:renderableHeight,
				width:renderableWidth
			});
		}
		else
		{
			obj1.img.destroy();
			obj1.imgZoom.destroy();	
			obj1.img = new Konva.Image({
				x: xStart,
				y: yStart,
				image: imageObj,
				height:renderableHeight,
				width:renderableWidth
			});
			obj1.imgZoom = new Konva.Image({
				x: xStart,
				y: yStart,
				image: imageObj,
				height:renderableHeight,
				width:renderableWidth
			});
			obj1.img.image(imageObj);
			obj1.imgZoom.image(imageObj)
		}
		obj1.layer_main.add(obj1.img);
		obj1.stage.add(obj1.layer_main);
		obj1.layer_zoom.add(obj1.imgZoom);
		obj1.stage_zoom.add(obj1.layer_zoom);
		obj1.stage_zoom.add(obj1.layer_zoom2);
		obj1.stage.add(obj1.layer_rect);

		ctx = obj1.img.getContext();
		src = ctx.getImageData(xStart,yStart,renderableWidth,renderableHeight);
		obj1.dst = null;
		obj1.dstI = null;
		obj1.dst = copyImageData(ctx,src);
		obj1.dstI = copyImageData(ctx,src);
		for(i=0;i<obj1.dstI.data.length;i+=4)
		{
			obj1.dstI.data[i]=255-obj1.dstI.data[i];
			obj1.dstI.data[i+1]=255-obj1.dstI.data[i+1];
			obj1.dstI.data[i+2]=255-obj1.dstI.data[i+2];
		}
		var data1 = (Object.size(data)>0?data.stage_1:{});
		for(i in data1){
			drawAnnotation(data1[i].coords,data1[i].pathology_lvl1,data1[i].anatomy_lvl1,data1[i].anatomy_lvl2,data1[i].annotation_id,false,$('.img-thumbnail-small.active').attr('data-slicenumber'),false,data1[i].comments,data1[i].shape,data1[i].severity,data1[i].verification_data,"tagstoverify",data1[i].stage_2,1) // coords corresponds to original image size
		}
		var data2 = (Object.size(data)>0?data.stage_2:{});
		for(i in data2){
			drawAnnotation(data2[i].coords,data2[i].pathology_lvl1,data2[i].anatomy_lvl1,data2[i].anatomy_lvl2,data2[i].annotation_id,false,$('.img-thumbnail-small.active').attr('data-slicenumber'),false,data2[i].comments,data2[i].shape,data2[i].severity,data2[i].verification_data,"tagstoverify",data2[i].stage_3,2) // coords corresponds to original image size
		}
		var data3 = (Object.size(data)>0?data.stage_3:{});
		for(i in data3){
			drawAnnotation(data3[i].coords,data3[i].pathology_lvl1,data3[i].anatomy_lvl1,data3[i].anatomy_lvl2,data3[i].annotation_id,false,$('.img-thumbnail-small.active').attr('data-slicenumber'),false,data3[i].comments,data3[i].shape,data3[i].severity,data3[i].verification_data,"mytags",undefined,3) // coords corresponds to original image size
		}
		obj1.layer_rect.draw();
		filter(obj1.contrastLevel,obj1.brightnessLevel,obj1);
	} // image onload ends
} // picUpload() ends	

function copyImageData(ctx, src){
	var dt = ctx.createImageData(src.width, src.height);
	dt.data.set(src.data);
	return dt;
}

function invert(obj,that){
	var xS=obj.img.getX();
	var yS=obj.img.getY();
	obj.isInvert = !obj.isInvert;
	if(obj.isInvert){
		that.addClass('active')
		ctx = obj.img.getContext();
		ctx.putImageData(obj.dstI,xS,yS);
	}
	else
	{
		that.removeClass('active')
		ctx = obj.img.getContext();
		ctx.putImageData(obj.dst,xS,yS);
	}
	filter(obj.contrastLevel,obj.brightnessLevel,obj);
}



function zoomView(){
	try
	{
		var zoomWidth=300,zoomHeight=250;
		var srcCtx = obj1.layer_main.getContext();
		var imageData = srcCtx.getImageData(obj1.stage.getPointerPosition().x-(zoomWidth/2),obj1.stage.getPointerPosition().y-(zoomHeight/2), zoomWidth, zoomHeight);
		var destCtx = obj1.layer_zoom.getContext();
		var newCanvas = $("<canvas>")
		.attr("width", zoomWidth)
		.attr("height", zoomHeight)[0];
		obj1.layer_zoom.clear({
			x : 0,
			y : 0,
			width : zoomWidth,
			height : zoomHeight
		});
		newCanvas.getContext("2d").putImageData(imageData, 0, 0);
		widthNew = zoomWidth * obj1.zoomLevel;
		heightNew = zoomHeight * obj1.zoomLevel;
		destCtx.drawImage(newCanvas, 0, 0,zoomWidth,zoomHeight,0-((widthNew - zoomWidth)/2),-((heightNew - zoomHeight)/2),widthNew,heightNew);


		var srcCtx2 = obj1.layer_rect.getContext();
		var imageData2 = srcCtx2.getImageData(obj1.stage.getPointerPosition().x-(zoomWidth/2),obj1.stage.getPointerPosition().y-(zoomHeight/2), zoomWidth, zoomHeight);
		var destCtx2 = obj1.layer_zoom2.getContext();
		var newCanvas2 = $("<canvas>")
		.attr("width", zoomWidth)
		.attr("height", zoomHeight)[0];
		obj1.layer_zoom2.clear({
			x : 0,
			y : 0,
			width : zoomWidth,
			height : zoomHeight
		});
		newCanvas2.getContext("2d").putImageData(imageData2, 0, 0);
		widthNew = zoomWidth * obj1.zoomLevel;
		heightNew = zoomHeight * obj1.zoomLevel;
		destCtx2.drawImage(newCanvas2, 0, 0,zoomWidth,zoomHeight,0-((widthNew-zoomWidth)/2),-((heightNew-zoomHeight)/2),widthNew,heightNew);
	}
	catch(e){
		console.log(e)
	}
}



function customDateFormat(datestring){
	datestring = datestring.split(" ")
	datestring = datestring[2]+"-"+datestring[1]+"-"+datestring[3];
	return datestring;
}

function lineCoordinates(x1,y1,x2,y2){
	lineCoords=[];
	var a;
	if(Math.abs(x1-x2)>Math.abs(y1-y2))
	{
		if(x2>x1)
		{
			a=0;
			for(var i=x1;i<=x2;i++)
			{
				lineCoords.push(i);
				if(y2>y1)
				{
					tmp_y=Math.round(y1+(a*((y2-y1)/(x2-x1))));
				}
				else
				{
					tmp_y=Math.round(y1-(a*((y1-y2)/(x2-x1))));
				}
				lineCoords.push(tmp_y);
				a++;
			}
		}
		else
		{	
			a=0;
			for(var i=x1;i>=x2;i--)
			{
				lineCoords.push(i);
				if(y2>y1)
				{
					tmp_y=Math.round(y1-(a*((y2-y1)/(x2-x1))));
				}
				else
				{
					tmp_y=Math.round(y1+(a*((y1-y2)/(x2-x1))));
				}
				lineCoords.push(tmp_y);
				a++;
			}
		}
	}
	else
	{
		if(y2>y1)
		{
			a=0;
			for(var i=y1;i<=y2;i++)
			{
				if(x2>x1)
				{
					tmp_x=roundingX(x1+(a*((x2-x1)/(y2-y1))));
				}
				else
				{
					tmp_x=roundingX(x1-(a*((x1-x2)/(y2-y1))));
				}
				lineCoords.push(tmp_x);
				lineCoords.push(i);
				a++;
			}
		}
		else
		{	
			a=0;
			for(var i=y1;i>=y2;i--)
			{
				if(y2>y1)
				{
					tmp_x=roundingX(x1-(a*((x2-x1)/(y2-y1))));
				}
				else
				{
					tmp_x=roundingX(x1+(a*((x1-x2)/(y2-y1))));
				}
				lineCoords.push(tmp_x);
				lineCoords.push(i);
				a++;
			}
		}
	}
	return lineCoords;
}

function roundingX(a){
	x = (Math.floor(a) + 0.5);
	return (parseFloat(x.toFixed(1)));
}

function propagateAnnotation(that,tag,tag2,tag3,coords,propagationStartPoint,propagationEndPoint,comments,shape,condition){ // receives coords corresponding to local system resolution
		var start = parseInt(propagationStartPoint)-1;
		var end = parseInt(propagationEndPoint)-1;
		for(i=start;i<=end;i++){
			var sliceNumber = i;
			var that_slice = $('.img-thumbnail-small:eq('+sliceNumber+')');
			var sliceUid = parseInt(that_slice.find('img').attr('data-sliceuid'));
			//that_slice.addClass('marked')
			addNewTag(that,sliceUid,tag,tag2,tag3,sliceNumber,coords,true,comments,shape,condition); // coords local
		}
}

function drawAnnotation(coords,tag,tag2,tag3,annotationId,isNew,sliceNumber,isPropagated,comments,shape,annotation_condition,verificationData,tagType,stage2,stage){ // coords corresponds to original image size
	var localCoords = globalToLocalPoints(coords); // coords scaled to local system resolution
	// var localCoords1 = localToGlobalPoints(coords);
	// var localCoords = globalToLocalPoints(localCoords1);


	var verified_in_stage = (stage2 != undefined?(stage2.stage_3!=undefined?(stage2.stage_3.verification_stage=="1"?1:2):(stage2.verification_stage!=undefined?(stage2.verification_stage=="1"?1:2):undefined)):undefined);
	var verified_stage_3_id = (stage2 != undefined?(stage2.stage_3!=undefined?stage2.stage_3.stage_3_annotation_id:stage2.stage_3_annotation_id):undefined);

		if(tagType == "tagstoverify"){
		switch(stage){
			case 1:
				if(stage2){
					if(stage2.stage_3){
						console.log('stage1 - verified in stage2 and stage3 both, verified stage is: '+stage2.stage_3.verification_stage);
						if(stage2.stage_3.verification_stage == 1){
							localCoords = globalToLocalPoints(stage2.stage_3.coords);
							tag = stage2.stage_3.pathology_lvl1;
							tag2 = stage2.stage_3.anatomy_lvl1;
							tag3 = stage2.stage_3.anatomy_lvl2;
							comments = stage2.stage_3.comments;
							annotation_condition = stage2.stage_3.severity;
						}
						else{
							stage2.coords = stage2.stage_3.coords;
							stage2.pathology_lvl1 = stage2.stage_3.pathology_lvl1;
							stage2.anatomy_lvl1 = stage2.stage_3.anatomy_lvl1;
							stage2.anatomy_lvl2 = stage2.stage_3.anatomy_lvl2;
						}
					}
					else{
						console.log('stage1 - verified in stage2 but not in stage3')
					}
				}
			break;

			case 2:
				if(stage2){
					console.log('stage2 - verified in stage3');
					localCoords = globalToLocalPoints(stage2.coords);
					tag = stage2.pathology_lvl1;
					tag2 = stage2.anatomy_lvl1;
					tag3 = stage2.anatomy_lvl2;
					comments = stage2.comments;
					annotation_condition = stage2.severity;
				}
				else{
					console.log('stage2 - not verified in stage3')
				}
			break;
		}
	}
	
	//console.log(verified_in_stage,verified_stage_3_id);
	if(isPropagated==false)
	{
		var currentSliceNumber = parseInt($('.slice-number-icon.active').html())
		if((parseInt(sliceNumber)+1)==currentSliceNumber)
		{
			if(shape == "polygon")
			{
				var line_temp = new Konva.Line({
					points: localCoords,
					stroke: 'red',
					tension: 0,
					strokeWidth:1,
					shadowColor:'white',
					shadowBlur:1,
					draggable:false,
					id:annotationId
				});
				obj1.layer_rect.add(line_temp);
				obj1.layer_rect.draw();
				if(ACTION_REQUIRED==1){
					$('#layer-items-2').append('<div class="feature-box annotation-item custom-tooltip" data-toggle="tooltip" data-placement="left" title="'+tag+' / '+tag2+' / '+tag3+'" data-annotationid="'+annotationId+'">'+
						'<div class="elips">'+tag+' / '+tag2+' / '+tag3+
						'</div><div style="padding:4px 8px">'+
							'<button class="annotation-view"><i class="zmdi zmdi-eye"></i></button><button data-annotation-type="polygon" class="btn-edit"><i class="zmdi zmdi-edit"></i> Edit</button><button class="btn-delete"><i class="zmdi zmdi-delete"></i> Delete</button>'+
						'</div>'+
						'</div>');
				}
				else{
					var valid = '',invalid = '',notsure = '';
					var vuid = '';
					var vtext = '';
					if(verificationData)
					{
						vuid = verificationData.verification_uid;
						vtext = verificationData.comments;
						switch(verificationData.status){
							case 'valid':
							valid = ' active';
							break;

							case 'invalid':
							invalid = ' active';
							break;

							case 'not sure':
							notsure = ' active';
							break;
						}
					}
					if(tagType == "tagstoverify")
					{
						if(stage == 1)
						{
							//console.log(stage2.stage_3)
							$('#layer-items').append('<div class="feature-box annotation-item custom-tooltip'+(verified_in_stage==1?" lbg3":"")+'" data-toggle="tooltip" data-placement="left" title="'+tag+' / '+tag2+' / '+tag3+'" data-verificationuid="'+vuid+'" data-annotationid="'+annotationId+'">'+
							'<div class="elips verify-it">'+tag+' / '+tag2+' / '+tag3+
							'</div><div style="padding:4px 8px">'+
									'<button class="annotation-view"><i class="zmdi zmdi-eye"></i></button><button data-annotation-type="polygon" class="btn-valid '+(verified_in_stage==1?"active":"")+(verified_in_stage==2?" hide":"")+'" data-stage="'+stage+'" data-verified="'+(stage2 != undefined?1:0)+'" data-id3="'+(verified_in_stage==1?verified_stage_3_id:"")+'">Valid</button> <button data-annotation-type="polygon" data-stage3="'+(stage2 != undefined?(stage2.stage_3==undefined?undefined:escape(JSON.stringify(stage2.stage_3))):undefined)+'" class="btn-edit'+(verified_in_stage==2?" hide":"")+'">Edit</button>'+
								'</div>'+
							'</div>')
						}
						if(stage == 2){
							//console.log(stage2)
							$('#layer-items').append('<div class="feature-box annotation-item custom-tooltip'+(verified_in_stage==2?" lbg3":"")+'" data-toggle="tooltip" data-placement="left" title="'+tag+' / '+tag2+' / '+tag3+'" data-verificationuid="'+vuid+'" data-annotationid="'+annotationId+'">'+
							'<div class="elips verify-it">'+tag+' / '+tag2+' / '+tag3+
							'</div><div style="padding:4px 8px">'+
									'<button class="annotation-view"><i class="zmdi zmdi-eye"></i></button><button data-annotation-type="polygon" class="btn-valid '+(verified_in_stage==2?"active":"")+'" data-stage="'+stage+'" data-verified="'+(stage2 != undefined?1:0)+'" data-id3="'+(stage2!=undefined?stage2.stage_3_annotation_id:"")+'">Valid</button> <button data-annotation-type="polygon" data-stage3="'+(stage2==undefined?undefined:escape(JSON.stringify(stage2)))+'" class="btn-edit">Edit</button>'+
								'</div>'+
							'</div>')
						}
					}
					else{
						$('#layer-items-2').append('<div class="feature-box annotation-item custom-tooltip" data-toggle="tooltip" data-placement="left" title="'+tag+' / '+tag2+' / '+tag3+'" data-verificationuid="'+vuid+'" data-annotationid="'+annotationId+'">'+
						'<div class="elips">'+tag+' / '+tag2+' / '+tag3+
						'</div><div style="padding:4px 8px">'+
								'<button class="annotation-view"><i class="zmdi zmdi-eye"></i></button><button data-annotation-type="polygon" class="btn-edit"><i class="zmdi zmdi-edit"></i> Edit</button><button class="btn-delete"><i class="zmdi zmdi-delete"></i> Delete</button>'+
							'</div>'+
						'</div>')
					}
				}
			}
			if(shape == "point"){
				var circle_tmp = new Konva.Circle({
					radius:5,
					stroke:'red',
					strokeWidth:2,
					x:localCoords[0],
					y:localCoords[1],
					id:annotationId,
					draggable:false
				})
				obj1.layer_rect.add(circle_tmp);
				obj1.layer_rect.draw();
				$('#layer-items').append('<div class="feature-box annotation-item" data-annotationid="'+annotationId+'">'+tag+'<br/><div class="annotation-view pull-right"><i class="zmdi zmdi-eye"></i></div><button data-annotation-type="point" class="btn btn-sm btn-primary btn-edit">Edit</button> <button class="btn btn-sm btn-default btn-delete">Delete</button></div>')
			}
		}
	}
	if(isNew){
		var currentSlice = $('.img-thumbnail-small:eq('+sliceNumber+')');
		annotationObj = JSON.parse(unescape(currentSlice.find('img').attr('data-annotation')) || "{stage_1:[],stage_2:[],stage_3:[]}");
		if(Object.size(annotationObj)==0){
			annotationObj = {stage_1:[],stage_2:[],stage_3:[]};
		}
		if(annotationObj["stage_"+ACTION_REQUIRED] == undefined){
			annotationObj["stage_"+ACTION_REQUIRED] = [];
		}
		currentSlice.find('.top-mark.tagged').removeClass('hide');
		annotationObj["stage_"+ACTION_REQUIRED].push({
		    "pathology_lvl1": tag,
			"anatomy_lvl1":tag2,
			"anatomy_lvl2":tag3,
		    "comments": comments,
		    "color": "NA",
		    "coords": coords,
			"shape": shape,
			"severity":annotation_condition,
			"annotation_id":annotationId
		})
		currentSlice.find('img').attr('data-annotation',escape(JSON.stringify(annotationObj)));
	}
	if(stage2 != undefined && stage == 1 && stage2.coords !=undefined){
			var line_temp = new Konva.Line({
				points: globalToLocalPoints(stage2.coords),
				stroke: 'red',
				tension: 0,
				strokeWidth:1,
				shadowColor:'white',
				shadowBlur:1,
				draggable:false,
				id:stage2.stage_2_annotation_id
			});
			obj1.layer_rect.add(line_temp);
			obj1.layer_rect.draw();
			$('#layer-items').append('<div class="verified-annotation-box custom-tooltip'+(verified_in_stage==2?" lbg3":"")+'" data-toggle="tooltip" data-placement="left" title="" data-annotationid="'+stage2.stage_2_annotation_id+'" data-original-title="'+stage2.pathology_lvl1+' / '+stage2.anatomy_lvl1+' / '+stage2.anatomy_lvl2+'"><div class="elips d-inline-block" style="width:40%;float:left">'+stage2.pathology_lvl1+' / '+stage2.anatomy_lvl1+' / '+stage2.anatomy_lvl2+'</div><div style="padding:4px 8px"><button class="annotation-view"><i class="zmdi zmdi-eye"></i></button> <button data-id3="'+(verified_in_stage==2?verified_stage_3_id:"")+'" class="btn-valid'+(verified_in_stage==1?" hide":"")+(verified_in_stage==2?" active":"")+'">Valid</button> <button data-stage3="'+(stage==1?escape(JSON.stringify(stage2.stage_3)):escape(JSON.stringify(stage2)))+'" class="btn-edit '+(verified_in_stage==1?"hide":"")+'">Edit</button></div></div>');
		// if(tagType == "tagstoverify")
		// {
		// 	$('#layer-items').append('<div data-stage3="'+(stage==1?escape(JSON.stringify(stage2.stage_3)):escape(JSON.stringify(stage2)))+'" style="margin:auto;margin-top:-10px;font-size:0.9em;width:60%;background:#fff;padding:5px;text-align:center">Add new annotation</div>');
		// }
	}
	$('[data-toggle="tooltip"]').tooltip({
		trigger : 'hover'
	});
}

function cancelDrawing(){
	line2.destroy();
	line2 = new Konva.Line({
		x: 0,
		y: 0,
		points: [],
		stroke: 'red',
		tension: 0,
		strokeWidth:1,
		shadowColor:'white',
		shadowBlur:1,
		draggable:false
	});
	startingPoint.remove();
	line.destroy();
	circle_temp.hide();
	obj1.layer_rect.draw();
	a = 0;	
	if($('.multipoint-tool').hasClass('active'))	
	{
		$('.multipoint-tool').trigger('click');
		$('.multipoint-tool').trigger('click');
	}
	if($('.point-tool').hasClass('active'))	
	{
		$('.point-tool').trigger('click');
		$('.point-tool').trigger('click');
	}
	$('#new-tag').hide();
	if($('#new-tag-close-btn').attr('data-lineid') != undefined){
		var lineObj = JSON.parse(unescape($('#new-tag').attr('data-tag-data')));
		removeCircles(lineObj.annotation_id,$('#new-tag-close-btn'));
	}
}

function globalToLocalPoints(coords){
	var imgH = obj1.imageHeight;
	var imgW = obj1.imageWidth;
	var renH = obj1.renderableHeight;
	var renW = obj1.renderableWidth;
	var xS = obj1.xStart;
	var yS = obj1.yStart;
	var l = coords.length-1;
	var updatedCoords = [];
	var i = 0;
	while(i<l){
		updatedCoords.push(((coords[i]/imgW)*renW)+xS)
		updatedCoords.push(((coords[i+1]/imgH)*renH)+yS)
		i += 2;
	}
	// return coords
	return updatedCoords;
}

function localToGlobalPoints(coords){
	var imgH = obj1.imageHeight;
	var imgW = obj1.imageWidth;
	var renH = obj1.renderableHeight;
	var renW = obj1.renderableWidth;
	var xS = obj1.xStart;
	var yS = obj1.yStart;
	var l = coords.length-1;
	var updatedCoords = [];
	var i = 0;
	while(i<l){
		updatedCoords.push(((coords[i]-xS)/renW)*imgW)
		updatedCoords.push(((coords[i+1]-yS)/renH)*imgH)
		i += 2;
	}
	return updatedCoords;
	// return coords;
}


function populateSliceBox(data){
	
	// slice data - MY TAGS
	var str = '';
	$('#added-instance-box').empty();
	data1 = data["stage_3"];
	for(i in data1){
		str +=  '<div class="feature-box instance-item custom-tooltip" data-toggle="tooltip" data-placement="left" title="'+data1[i].pathology_lvl1+' - '+data1[i].anatomy_lvl1
					+' - '+data1[i].comments+'" data-instance-info-id="'+data1[i].instanceinfo_uid+'"><div class="elips">'+data1[i].pathology_lvl1+' - '+data1[i].anatomy_lvl1
					+'</div><div style="padding:4px 8px">'
						+'<button class="instance-item-delete"><i class="zmdi zmdi-delete"></i> Delete</button>'
					+'</div>'
				+'</div>';
	}
	$('#added-instance-box').html(str);
	

	// slice data - TAGS TO VERIFY

	var str = '';
	$('#added-instance-box-verification').empty();
	data2 = data["stage_1"];
	for(i in data2){
		var verified_in_stage = undefined;
		if(data2[i].stage_2 && data2[i].stage_2.stage_3){
			verified_in_stage = data2[i].stage_2.stage_3.verification_stage;
		}
		// console.log(verified_in_stage)
		str += '<div class="feature-box instance-item custom-tooltip'+(verified_in_stage==1?" lbg3":"")+'" data-toggle="tooltip" data-placement="left" title="'+(verified_in_stage==1?data2[i].stage_2.stage_3.pathology_lvl1:data2[i].pathology_lvl1)+' - '+(verified_in_stage==1?data2[i].stage_2.stage_3.anatomy_lvl1:data2[i].anatomy_lvl1)
					+' - '+(verified_in_stage==1?data2[i].stage_2.stage_3.comments:data2[i].comments)+'" data-instance-info-id="'+data2[i].instanceinfo_uid+'"><div class="elips">'+(verified_in_stage==1?data2[i].stage_2.stage_3.pathology_lvl1:data2[i].pathology_lvl1)+' - '+(verified_in_stage==1?data2[i].stage_2.stage_3.anatomy_lvl1:data2[i].anatomy_lvl1)
					+'</div>'
					+'<div style="padding:4px 8px">'
						+'<button class="valid-instance'+(verified_in_stage==1?" active":"")+(verified_in_stage==2?" hide":"")+'" data-verified="0" data-id3="'+(verified_in_stage==1?data2[i].stage_2.stage_3.stage_3_instance_info_id:"")+'" data-id="'+data2[i].instanceinfo_uid+'" data-stage="1">Valid</button>'
						+'<button class="verify-instance'+(verified_in_stage==2?" hide":"")+'" data-verified="0" data-id3="'+(verified_in_stage==1?data2[i].stage_2.stage_3.stage_3_instance_info_id:"")+'" data-id="'+data2[i].instanceinfo_uid+'" data-stage="1">Edit</button>'
					+'</div>'
				+'</div>';
		if(data2[i].stage_2!=undefined)
		{
			str += '<div class="verified-instance-box custom-tooltip'+(verified_in_stage==2?" lbg3":"")+'" data-toggle="tooltip" data-placement="left" data-instanceid="'+data2[i].stage_2.stage_2_instance_info_id+'" title="'+(verified_in_stage==2?data2[i].stage_2.stage_3.pathology_lvl1:data2[i].stage_2.pathology_lvl1)+' / '+(verified_in_stage==2?data2[i].stage_2.stage_3.anatomy_lvl1:data2[i].stage_2.anatomy_lvl1)+' / '+(verified_in_stage==2?data2[i].stage_2.stage_3.anatomy_lvl2:data2[i].stage_2.anatomy_lvl2)+'"><div class="elips" style="width:40%;float:left">'+(verified_in_stage==2?data2[i].stage_2.stage_3.pathology_lvl1:data2[i].stage_2.pathology_lvl1)+' / '+(verified_in_stage==2?data2[i].stage_2.stage_3.anatomy_lvl1:data2[i].stage_2.anatomy_lvl1)+' / '+(verified_in_stage==2?data2[i].stage_2.stage_3.anatomy_lvl2:data2[i].stage_2.anatomy_lvl2)+'</div>'
					+'<div style="padding:4px 8px">'
						+'<button class="valid-instance'+(verified_in_stage==1?" hide":"")+(verified_in_stage==2?" active":"")+'" data-verified="0" data-id3="'+(verified_in_stage==2?data2[i].stage_2.stage_3.stage_3_instance_info_id:"")+'" data-id="'+data2[i].stage_2.stage_2_instance_info_id+'" data-stage="12">Valid</button>'
						+'<button class="verify-instance'+(verified_in_stage==1?" hide":"")+(verified_in_stage==2?" active":"")+'" data-verified="0" data-id3="'+(verified_in_stage==2?data2[i].stage_2.stage_3.stage_3_instance_info_id:"")+'" data-id="'+data2[i].stage_2.stage_2_instance_info_id+'" data-stage="12">Edit</button>'
					+'</div></div>'
		}
	}
	data2 = data["stage_2"];
	for(i in data2){
		var verified_in_stage = undefined;
		if(data2[i].stage_3){
			verified_in_stage = data2[i].stage_3.verification_stage;
		}
		console.log(verified_in_stage)
		str += '<div class="feature-box instance-item custom-tooltip'+(verified_in_stage==2?" lbg3":"")+'" data-toggle="tooltip" data-placement="left" title="'+(verified_in_stage==2?data2[i].stage_3.pathology_lvl1:data2[i].pathology_lvl1)+' - '+(verified_in_stage==2?data2[i].stage_3.anatomy_lvl1:data2[i].anatomy_lvl1)
					+' - '+(verified_in_stage==2?data2[i].stage_3.comments:data2[i].comments)+'" data-instance-info-id="'+data2[i].instanceinfo_uid+'"><div class="elips">'+(verified_in_stage==2?data2[i].stage_3.pathology_lvl1:data2[i].pathology_lvl1)+' - '+(verified_in_stage==2?data2[i].stage_3.anatomy_lvl1:data2[i].anatomy_lvl1)
					+'</div>'
					+'<div style="padding:4px 8px">'
						+'<button class="valid-instance'+(verified_in_stage==2?" active":"")+'" data-verified="0" data-id3="'+(verified_in_stage==2?data2[i].stage_3.stage_3_instance_info_id:"")+'" data-id="'+data2[i].instanceinfo_uid+'" data-stage="2">Valid</button>'
						+'<button class="verify-instance" data-verified="0" data-id3="'+(verified_in_stage==2?data2[i].stage_3.stage_3_instance_info_id:"")+'" data-id="'+data2[i].instanceinfo_uid+'" data-stage="2">Edit</button>'
					+'</div>'
				+'</div>';
		// if(data2[i].stage_2!=undefined)
		// {
		// 	str += '<div class="verified-instance-box custom-tooltip" data-toggle="tooltip" data-placement="left" data-instanceid="'+data2[i].stage_2.stage_2_instance_info_id+'" title="'+data2[i].stage_2.pathology_lvl1+' / '+data2[i].stage_2.anatomy_lvl1+' / '+data2[i].stage_2.anatomy_lvl2+'"><div class="elips" style="width:40%;float:left">'+data2[i].stage_2.pathology_lvl1+' / '+data2[i].stage_2.anatomy_lvl1+' / '+data2[i].stage_2.anatomy_lvl2+'</div>'
		// 			+'<div style="padding:4px 8px">'
		// 				+'<button class="valid-instance" data-verified="0" data-id="'+data2[i].stage_2.stage_2_instance_info_id+'" data-stage="2">Valid</button>'
		// 				+'<button class="verify-instance" data-verified="0" data-id="'+data2[i].stage_2.stage_2_instance_info_id+'" data-stage="2">Edit</button>'
		// 			+'</div></div>'
		// }
	}
	$('#added-instance-box-verification').html(str);
	$('[data-toggle="tooltip"]').tooltip({
		trigger : 'hover'
	});
}
$(document).on('click','.valid-instance',function(){
	var that = $(this);
	that.toggleClass('active')
	var currentSlice = $('.img-thumbnail-small.active img');
	currentObj = JSON.parse(unescape(currentSlice.attr('data-instance')));
	console.log(currentObj)
	var dataObj = {
		stage_2_instance_info_id:"", // int
        stage_1_instance_info_id:"", // int
        series_uuid:$('#folderid').val(),
        slice_id:parseInt(currentSlice.attr('data-sliceuid')),
        status:"valid",
        comments:"",
        edit:0,
        pathology_lvl1:"",
        anatomy_lvl1:"",
        anatomy_lvl2:"",
        severity:"",
        verification_stage:1
	}
	switch(that.attr('data-stage')){
		case "1":
			if(that.hasClass('active'))
			{
				that.closest('.feature-box').next().next('.verified-instance-box:first').find('.valid-instance').addClass('hide');
				that.closest('.feature-box').next().next('.verified-instance-box:first').find('.verify-instance').addClass('hide');
			}
			else{
				that.closest('.feature-box').next().next('.verified-instance-box:first').find('.valid-instance').removeClass('hide');
				that.closest('.feature-box').next().next('.verified-instance-box:first').find('.verify-instance').removeClass('hide');
			}
			$.grep(currentObj.stage_1,function(ele,key){
				if(ele.instanceinfo_uid==that.attr('data-id')){
					dataObj.stage_1_instance_info_id = ele.instanceinfo_uid;
					dataObj.pathology_lvl1 = ele.pathology_lvl1;
					dataObj.anatomy_lvl1 = ele.anatomy_lvl1;
					dataObj.anatomy_lvl2 = ele.anatomy_lvl2;
					dataObj.severity = ele.severity;
				}
			})
			dataObj.verification_stage = "1";
		break;

		case "2":
			$.grep(currentObj.stage_2,function(ele,key){
				if(ele.instanceinfo_uid==that.attr('data-id')){
					dataObj.stage_2_instance_info_id = ele.instanceinfo_uid;
					dataObj.pathology_lvl1 = ele.pathology_lvl1;
					dataObj.anatomy_lvl1 = ele.anatomy_lvl1;
					dataObj.anatomy_lvl2 = ele.anatomy_lvl2;
					dataObj.severity = ele.severity;
				}
			})
			dataObj.verification_stage = "2";
		break;

		case "12":
			if(that.hasClass('active'))
			{
				that.closest('.verified-instance-box').prev('.feature-box').find('.valid-instance').addClass('hide');
				that.closest('.verified-instance-box').prev('.feature-box').find('.verify-instance').addClass('hide');
			}
			else{
				that.closest('.verified-instance-box').prev('.feature-box').find('.valid-instance').removeClass('hide');
				that.closest('.verified-instance-box').prev('.feature-box').find('.verify-instance').removeClass('hide');
			}
			$.grep(currentObj.stage_1,function(ele,key){
				if(ele.stage_2.stage_2_instance_info_id==that.attr('data-id')){
					console.log(ele.stage_2)
					dataObj.stage_1_instance_info_id = ele.instanceinfo_uid;
					dataObj.stage_2_instance_info_id = ele.stage_2.stage_2_instance_info_id;
					dataObj.pathology_lvl1 = ele.stage_2.pathology_lvl1;
					dataObj.anatomy_lvl1 = ele.stage_2.anatomy_lvl1;
					dataObj.anatomy_lvl2 = ele.stage_2.anatomy_lvl2;
					dataObj.severity = ele.stage_2.severity;
				}
			})
			dataObj.verification_stage = "2";
		break;
	}
	if(that.hasClass('active')){
		$.ajax({
			method:'POST',
			url:base_url+'/apis/verification/instance/stage3/add',
			data:JSON.stringify(dataObj),
			beforeSend:function(){
				that.text('Wait..');
			},
			success:function(data){
				currentSlice.attr('data-verified-count',(parseInt(currentSlice.attr('data-verified-count'))+1));
				if(currentSlice.attr('data-verified-count') == currentSlice.attr('data-total')){
					currentSlice.parent().find('.verification-done').removeClass('hide');
				}
				that.text('Valid');
				nextBtnStatus();
				console.log(data);
				switch(that.attr('data-stage')){
					case "1":
						$.grep(currentObj.stage_1,function(ele,key){
							if(ele.instanceinfo_uid==that.attr('data-id')){
								if(ele.stage_2==undefined){
									ele.stage_2 = {}
								}
								if(ele.stage_2.stage_3 == undefined){
									ele.stage_2.stage_3 = {}
								}
								ele.stage_2.stage_3.anatomy_lvl1 = dataObj.anatomy_lvl1;
								ele.stage_2.stage_3.anatomy_lvl2 = dataObj.anatomy_lvl2;
								ele.stage_2.stage_3.comments = dataObj.comments;
								ele.stage_2.stage_3.edit = 0;
								ele.stage_2.stage_3.pathology_lvl1 = dataObj.pathology_lvl1;
								ele.stage_2.stage_3.severity = dataObj.severity;
								ele.stage_2.stage_3.stage_3_instance_info_id = data.instance_info_id;
								ele.stage_2.stage_3.status = "valid";
								ele.stage_2.stage_3.verification_stage = 1;
								//ele.stage_2.stage_3.
							}
						})
					break;

					case "12":
						$.grep(currentObj.stage_1,function(ele,key){
							if(ele.stage_2.stage_2_instance_info_id==that.attr('data-id')){
								if(ele.stage_2.stage_3 == undefined){
									ele.stage_2.stage_3 = {}
								}
								ele.stage_2.stage_3.anatomy_lvl1 = dataObj.anatomy_lvl1;
								ele.stage_2.stage_3.anatomy_lvl2 = dataObj.anatomy_lvl2;
								ele.stage_2.stage_3.comments = dataObj.comments;
								ele.stage_2.stage_3.edit = 0;
								ele.stage_2.stage_3.pathology_lvl1 = dataObj.pathology_lvl1;
								ele.stage_2.stage_3.severity = dataObj.severity;
								ele.stage_2.stage_3.stage_3_instance_info_id = data.instance_info_id;
								ele.stage_2.stage_3.status = "valid";
								ele.stage_2.stage_3.verification_stage = 2;
								//ele.stage_2.stage_3.
							}
						})
					break;

					case "2":
						$.grep(currentObj.stage_2,function(ele,key){
							if(ele.instanceinfo_uid==that.attr('data-id')){
								if(ele.stage_3==undefined){
									ele.stage_3 = {}
								}
								ele.stage_3.anatomy_lvl1 = dataObj.anatomy_lvl1;
								ele.stage_3.anatomy_lvl2 = dataObj.anatomy_lvl2;
								ele.stage_3.comments = dataObj.comments;
								ele.stage_3.edit = 0;
								ele.stage_3.pathology_lvl1 = dataObj.pathology_lvl1;
								ele.stage_3.severity = dataObj.severity;
								ele.stage_3.stage_3_instance_info_id = data.instance_info_id;
								ele.stage_3.status = "valid";
								ele.stage_3.verification_stage = 2;
								//ele.stage_2.stage_3.
							}
						})
					break;
				}
				currentSlice.attr('data-instance',escape(JSON.stringify(currentObj)));
				currentSlice.trigger('click');
			}

		})
	}
	else{
		$.ajax({
			method:"POST",
			url:base_url+"/apis/verification/instance/stage3/delete",
			data:JSON.stringify({
				stage_3_instance_info_id:that.attr('data-id3')
			}),
			beforeSend:function(){
				that.text('Wait..');
			},
			success:function(data){
				that.text('Valid');
				currentSlice.attr('data-verified-count',(parseInt(currentSlice.attr('data-verified-count'))-1));
				currentSlice.parent().find('.verification-done').addClass('hide');
				$('#tv').val((parseInt($('#tv').val())-2));
				nextBtnStatus();
				switch(that.attr('data-stage')){
					case "1":
						$.grep(currentObj.stage_1,function(ele,key){
							if(ele.instanceinfo_uid==that.attr('data-id')){
								delete ele["stage_2"]["stage_3"];
							}
						})
					break;

					case "12":
						$.grep(currentObj.stage_1,function(ele,key){
							if(ele.stage_2.stage_2_instance_info_id==that.attr('data-id')){
								delete ele["stage_2"]["stage_3"];
							}
						})
					break;

					case "2":
						$.grep(currentObj.stage_2,function(ele,key){
							if(ele.instanceinfo_uid==that.attr('data-id')){
								delete ele["stage_3"];
							}
						})
					break;
				}
				currentSlice.attr('data-instance',escape(JSON.stringify(currentObj)));
				currentSlice.trigger('click');
			}
		})
	}
})
function removeInstanceInfo(iuid,that,slice){
	$.ajax({
		method:"POST",
		url:base_url+"/apis/instance/allstages/delete",
		data:JSON.stringify({instance_info_id:parseInt(iuid),stage:ACTION_REQUIRED}),
		beforeSend:function(){
			that.html('Wait..')
		},
		success:function(data){
			that.parent().parent().remove();
			var keyToRemove = null;
			var instanceObj = JSON.parse(unescape(slice.find('img').attr('data-instance')));
			$.grep(instanceObj["stage_"+ACTION_REQUIRED],function(ele,key){
				if(ele.instanceinfo_uid == iuid){
					keyToRemove = key;
				}
			})
			if(instanceObj["stage_1"] == undefined){
				instanceObj["stage_1"] = []
			}
			if(instanceObj["stage_2"] == undefined){
				instanceObj["stage_2"] = []
			}
			if(instanceObj["stage_3"] == undefined){
				instanceObj["stage_3"] = []
			}
			instanceObj["stage_"+ACTION_REQUIRED].splice(keyToRemove,1);
			if(instanceObj["stage_1"].length == 0 && instanceObj["stage_2"].length == 0 && instanceObj["stage_3"].length == 0){
				slice.find('.top-mark.instance').addClass('hide');
			}
			slice.find('img').attr('data-instance',unescape(JSON.stringify(instanceObj)));
		}
	})
}


function addInstanceInfo(folderid,slice,pat1,ana1,comment,isPropagated){
	$.ajax({
		method:"POST",
		url:base_url+"/apis/instance/allstages/add",
		data:JSON.stringify({
			series_uuid:folderid,
			slice_id:parseInt(slice.find('img').attr('data-sliceuid')),
			pathology_lvl1:pat1,
			anatomy_lvl1:ana1,
			anatomy_lvl2:"",
			comments:comment,
			stage:ACTION_REQUIRED
		}),
		beforeSend:function(){
			if(isPropagated==false)
			{
				$('#save-instance-btn').html('Wait..')
			}
		},
		success:function(data){
			if(isPropagated==false){
				$('#added-instance-box').append('<div class="feature-box instance-item custom-tooltip" data-toggle="tooltip" data-placement="left" title="'+pat1+' - '+ana1+' - '+comment+
						'" data-instance-info-id="'
				+JSON.stringify(data.instance_info_id)+'">'+
						'<div class="elips">'+pat1+' - '+ana1+
						'</div><div style="padding:4px 8px">'+
							'<button class="instance-item-delete"><i class="zmdi zmdi-delete"></i> Delete</button>'+
						'</div>'+
					'</div>')
				$('#save-instance-btn').html('Save');
				$('#instance-tag-box').hide();
				$('#pat1').val('select');
			}
			var newInstanceObj = {
				anatomy_lvl1:ana1,
				anatomy_lvl2:"",
				instance_info_id:data.instance_info_id,
				pathology_lvl1:pat1,
				comments:comment
			}
			var instanceObj = JSON.parse(unescape(slice.find('img').attr('data-instance')));
			if(instanceObj["stage_"+ACTION_REQUIRED]==undefined){
				instanceObj["stage_"+ACTION_REQUIRED] = [];
			}
			instanceObj["stage_"+ACTION_REQUIRED].push(newInstanceObj);
			slice.find('.top-mark.instance').removeClass('hide');
			slice.find('img').attr('data-instance',escape(JSON.stringify(instanceObj)));
			$('[data-toggle="tooltip"]').tooltip({
				trigger : 'hover'
			});
		},
		error:function(){
			if(isPropagated==false)
			{
				$('#save-instance-btn').html('Error');
			}
		}
	})
}

function showTagBox(x,y){
	if($('#new-tag-v').is(':visible')==false){
		$('#tag-options').val('select');
		$('#tag-options2,#tag-options3').empty();
		$('#annotation-comment').val('');
		$('#ana1,#tag-options2,#tag-options3').hide();
		$('#new-tag').css({left:x,top:y,'margin-left':'-'+(($('#new-tag').width()+20)/2)+'px'});
		$('#new-tag-save-btn').text("Save");
		$('#new-tag').show();
	}
}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};


function getReports(reportId){
	$.ajax({
		method:"POST",
		url:base_url+"/apis/reports/"+reportId,
		global:false,
		beforeSend:function(){
			$('#reportList').html('<div style="padding: 2px 10px;">Please wait..</div>');
		},
		success:function(data){
			$('#reportList').empty();
			//$('#reportList').html('<li><a href="#" data-toggle="modal" data-target="#reportModal">Report-1</a></li><li><a href="#" data-toggle="modal" data-target="#reportModal">Report-2</a></li>');
			for(i in data){
				$('#reportList').append('<li><a href="#" data-toggle="modal" data-date="'+dateTimeFormat(data[i].report_date)+'" data-text="'+escape(data[i].report_text)+'" data-target="#reportModal">'+dateTimeFormat(data[i].report_date)+'</a></li>');
			}
		},
		error:function(data){
			$('#reportList').html('<div style="padding: 2px 10px;font-size:0.85em" id="reportError">Error! Click to Reload</div>');
			$('#reportError').click(function(){
				getReports(reportId);
			})
		}
	})
}

function dateTimeFormat(d){
	var date = (new Date(d)).toString();
	date = date.split(" ");
	return (date[2]+" "+date[1]+", "+date[3]+" - "+date[4])
}


function drawMovingPoints(lineId){
	var lineToMove = obj1.stage.find('#'+lineId)[0];
	var pts = lineToMove.points();
	for(var i=0;i<pts.length;i++){
		var cTemp = new Konva.Circle({
			fill:'#f00',
			radius:4,
			strokeWidth:1,
			x:pts[i],
			y:pts[i+1],
			opacity:0.5,
			name:escape(JSON.stringify({
				i:i,
				j:i+1,
				id:lineId
			})),
			draggable:true
		})
		obj1.layer_rect.add(cTemp);
		cTemp.on('dragmove',function(){
			this.setOpacity(1)
			var thisData = JSON.parse(unescape(this.name()));
			var  i = thisData.i;
			var j = thisData.j;
			pts[i] = this.getX();
			pts[j] = this.getY();
			lineToMove.points(pts);
			obj1.layer_rect.batchDraw();
		})
		cTemp.on('dragend',function(){
			this.setOpacity(0.5)
		})
		i++;
	}
	obj1.stage.draw();
}

function removeCircles(lineId,restoreBtn){
	try{
		removeMovingPoints(lineId);
		var lid = restoreBtn.attr('data-lineid');
		var pts = restoreBtn.attr('data-linepts').split(",");
		for(i in pts){
			pts[i] = parseFloat(pts[i])
		}
		restorePath(lid,pts);
		restoreBtn.removeAttr('data-lineid');
		restoreBtn.removeAttr('data-linepts');
	}
	catch(e){

	}
}

function removeMovingPoints(lineId){
	var allPoints = obj1.stage.find('Circle');
	allPoints.each(function(node){
		if(node.name() != undefined){
			var thisData = JSON.parse(unescape(node.name()));
			if(thisData.id == lineId){
				node.destroy();
			}
		}
	})
	obj1.layer_rect.draw();
}

function restorePath(id,pts){
	var lineToRestore = obj1.stage.find('#'+id)[0];
	lineToRestore.points(pts);
	obj1.layer_rect.draw();
}

function sortedList(obj){
	var tempList = [];
	for(i in obj){
		tempList.push(i)
	}
	tempList.sort();
	return tempList;
}

function setVerifiedCount(data){
	var tt = 0, tv = 0;

		if(data.series_info){
			if(data.series_info.stage_1){
				for(i in data.series_info.stage_1){
					tt++;
					if(data.series_info.stage_1[i].stage_2 !=undefined){
						tv++;
					}
				}
			}
			if(data.series_info.stage_2){
				for(i in data.series_info.stage_2){
					tt++;
					if(data.series_info.stage_2[i].stage_3 !=undefined){
						tv++;
					}
				}
			}
		}
		for(i in data.instances){
			if(data.instances[i].instance_info){
				for(j in data.instances[i].instance_info.stage_1){
					tt++;
					if(data.instances[i].instance_info.stage_1[j].stage_2 && data.instances[i].instance_info.stage_1[j].stage_2.stage_3){
						tv++;
					}
				}
			}
			if(data.instances[i].instance_info){
				for(j in data.instances[i].instance_info.stage_2){
					tt++;
					if(data.instances[i].instance_info.stage_2[j].stage_3){
						tv++;
					}
				}
			}
			if(data.instances[i].annotations){
				for(j in data.instances[i].annotations.stage_1){
					tt++;
					if(data.instances[i].annotations.stage_1[j].stage_2 && data.instances[i].annotations.stage_1[j].stage_2.stage_3){
						tv++;
					}
				}
				for(j in data.instances[i].annotations.stage_2){
					tt++;
					if(data.instances[i].annotations.stage_2[j].stage_3){
						tv++;
					}
				}
			}
		}
	$('#tt').val(tt)
	$('#tv').val(tv)
	if(tt==tv){
		$('#next').prop('disabled',false)
	}
	else{
		$('#next').prop('disabled',true)
	}
}


function setAnnotationVerificationBox(id,pathology_lvl1,anatomy_lvl1,anatomy_lvl2,comments,severity,auid,needUpdate,tag_level,allStageIds){
	if(id != undefined){
		if($('#verify-tag-close-btn').attr('data-lineid') !=undefined){
			removeCircles(auid,$('#verify-tag-close-btn'))
		}
		drawMovingPoints(auid);
		$('#verify-tag-close-btn').attr('data-lineid',auid);
		$('#verify-tag-close-btn').attr('data-linepts',obj1.stage.find('#'+auid)[0].points());
	}
	$('.btn-annotation-verification').removeClass('active');
	$('#tag-options-v').val(pathology_lvl1)
	$('#tag-options2-v').empty();
	var an1 = medicalTree["BRAIN"]["annotation"]["polygon"][pathology_lvl1];
	if(Object.size(an1) == 0){
		$('#tag-options2-v').hide();
		$('#tag-options3-v').hide();
	}
	else{
		$('#tag-options2-v').show();
		an1 = sortedList(an1);
		$('#tag-options2-v').append('<option value="select">Please Select</option>')
		for(i in an1){
			$('#tag-options2-v').append('<option value="'+an1[i]+'">'+an1[i]+'</option>')
		}
		$('#tag-options3-v').empty();
		var an2 = medicalTree["BRAIN"]["annotation"]["polygon"][pathology_lvl1][anatomy_lvl1];
		if(Object.size(an2) == 0){
			$('#tag-options3-v').hide();
		}
		else{
			$('#tag-options3-v').show();
			an2 = sortedList(an2);
			$('#tag-options3-v').append('<option value="select">Please Select</option>')
			for(i in an2){
				$('#tag-options3-v').append('<option value="'+an2[i]+'">'+an2[i]+'</option>')
			}
		}
	}
	$('#tag-options2-v').val(anatomy_lvl1)
	$('#tag-options3-v').val(anatomy_lvl2)
	$('#new-tag-v').attr('data-auid',auid);
	$('#new-tag-v').attr('data-auid2',id || "");
	$('#new-tag-v').attr('data-pat1',pathology_lvl1);
	$('#new-tag-v').attr('data-ana1',anatomy_lvl1);
	$('#new-tag-v').attr('data-ana2',anatomy_lvl2);
	$('#annotation-verification-comment').val(comments)
	// $('#tag-options-v').prop('disabled',true)
	// $('#tag-options2-v').prop('disabled',true)
	// $('#tag-options3-v').prop('disabled',true)
	if(severity==""){
		$('#annotation-verification-condition').prop('checked',false)
	}
	else{
		$('#annotation-verification-condition').prop('checked',true)	
	}
	// if(needUpdate){

	// }
	$('#verify-tag-save-btn').attr('data-id1',id)
							 .attr('data-id2',auid)
							 .attr('data-needupdate',needUpdate)
							 .attr('data-taglevel',tag_level)
							 .attr('data-allstageids',escape(JSON.stringify(allStageIds)))
	// switch(data.status){
	// 	case "Valid":
	// 	$('.btn-annotation-verification:eq(0)').trigger('click')
	// 	$('#verify-tag-save-btn').attr('data-type','update')
	// 	break;
	// 	case "Invalid":
	// 	$('.btn-annotation-verification:eq(1)').trigger('click')
	// 	$('#verify-tag-save-btn').attr('data-type','update')
	// 	break;
	// 	case "Not Sure":
	// 	$('.btn-annotation-verification:eq(2)').trigger('click')
	// 	$('#verify-tag-save-btn').attr('data-type','update')
	// 	break;
	// 	default:
	// 	$('#verify-tag-save-btn').attr('data-type','add')
	// }
}
function setInstanceVerificationBox(data,iuid,s3_id,instance_stage,s1_id,s2_id){
	$('#pat1-v').val(data.pathology_lvl1)
	$('#ana1-v').empty();
	var an1 = medicalTree["BRAIN"]["instance"]["boolean"][data.pathology_lvl1];
	if(Object.size(an1) == 0){
		$('#ana1-v').hide();
	}
	else{
		$('#ana1-v').show();
		an1 = sortedList(an1);
		$('#ana1-v').append('<option value="select">Please Select</option>')
		for(i in an1){
			$('#ana1-v').append('<option value="'+an1[i]+'">'+an1[i]+'</option>')
		}
	}
	$('#ana1-v').val(data.anatomy_lvl1)
	$('#ana2-v').val(data.anatomy_lvl2)
	// switch(instance_stage){
	// 	case "1":
	// 	break;

	// 	case "12":
	// 	break;

	// 	case "2":
	// 	break;
	// }
	$('#verify-instance-btn').attr('data-stagelevel',instance_stage);
	if(s3_id != ""){
		$('#verify-instance-btn').attr('data-type','update');
	}
	else{
		$('#verify-instance-btn').attr('data-type','add');	
	}
	$('#instance-tag-box-v').attr('data-iuid3',s3_id);
	$('#instance-tag-box-v').attr('data-iuid2',s2_id);
	$('#instance-tag-box-v').attr('data-iuid1',s1_id);
	$('#instance-tag-box-v').attr('data-iuid',iuid);
	$('#instance-tag-box-v').attr('data-pat1',data.pathology_lvl1);
	$('#instance-tag-box-v').attr('data-ana1',data.anatomy_lvl1);
	$('#instance-tag-box-v').attr('data-ana2',data.anatomy_lvl2);
}

function populateSeriesBox(data){
	switch(ACTION_REQUIRED){
		case 1:
		break;

		// case 2:
		// $('#series-info-verification-box').empty();
		// populateSeriesVerifyTags(data.stage_1);
		// populateSeriesMyTags(data.stage_2);
		// break;
		case 3:
			$('#series-info-verification-box').empty();
			populateSeriesVerifyTags(data.stage_1,data.stage_2);
			populateSeriesMyTags(data.stage_3);
		break;
	}
}

function nextBtnStatus(){
	var ct = parseInt($('#tv').val());
	ct++;
	$('#tv').val(ct);
	if($('#tv').val() == $('#tt').val()){
		$('#next').prop('disabled',false);
	}
	else{
		$('#next').prop('disabled',true);
	}
}

function populateSeriesVerifyTags(data,data2){
	for(i in data){
		if(data[i].stage_2 && data[i].stage_2.stage_3){
			var cl1='',cl2='';
			(data[i].stage_2.stage_3.status=="valid"?cl1=" active":cl2=" active")
			$('#series-info-verification-box').append('<div class="feature-box lbg3"><div class="elips">'+data[i].label+'</div><div style="padding:4px 8px"><button class="series-validation-btn'+cl1+'" data-suid="'+data[i].stage_2.stage_3.stage_3_series_info_id+'" data-stage="1" data-id="'+data[i].series_info_id+'"><i class="zmdi zmdi-check"></i> Valid</button><button class="series-validation-btn'+cl2+'" data-stage="1" data-suid="'+data[i].stage_2.stage_3.stage_3_series_info_id+'" data-id="'+data[i].series_info_id+'"><i class="zmdi zmdi-close"></i> Invalid</button></div></div>')
		}
		else{
			$('#series-info-verification-box').append('<div class="feature-box"><div class="elips">'+data[i].label+'</div><div style="padding:4px 8px"><button class="series-validation-btn" data-stage="1" data-id="'+data[i].series_info_id+'"><i class="zmdi zmdi-check"></i> Valid</button><button class="series-validation-btn" data-stage="1" data-id="'+data[i].series_info_id+'"><i class="zmdi zmdi-close"></i> Invalid</button></div></div>')
		}
	}
	for(i in data2){
		if(data2[i].stage_3){
			var cl1='',cl2='';
			(data2[i].stage_3.status=="valid"?cl1=" active":cl2=" active")
			$('#series-info-verification-box').append('<div class="feature-box lbg3"><div class="elips">'+data2[i].label+'</div><div style="padding:4px 8px"><button class="series-validation-btn'+cl1+'" data-stage="2" data-suid="'+data2[i].stage_3.stage_3_series_info_id+'" data-id="'+data2[i].series_info_id+'"><i class="zmdi zmdi-check"></i> Valid</button><button class="series-validation-btn'+cl2+'" data-stage="2" data-suid="'+data2[i].stage_3.stage_3_series_info_id+'" data-id="'+data2[i].series_info_id+'"><i class="zmdi zmdi-close"></i> Invalid</button></div></div>')
		}
		else{
			$('#series-info-verification-box').append('<div class="feature-box"><div class="elips">'+data2[i].label+'</div><div style="padding:4px 8px"><button class="series-validation-btn" data-stage="2" data-id="'+data2[i].series_info_id+'"><i class="zmdi zmdi-check"></i> Valid</button><button data-stage="2" class="series-validation-btn" data-id="'+data2[i].series_info_id+'"><i class="zmdi zmdi-close"></i> Invalid</button></div></div>')
		}
	}
}

function populateSeriesMyTags(data){
	$('#series-info-box').empty();
	for(i in data){
		$('#series-info-box').append('<div class="feature-box"><div class="elips">'+data[i].label+'</div><div style="padding:4px 8px"><button class="delete-my-series" data-id="'+data[i].series_info_id+'"><i class="zmdi zmdi-delete"></i> Delete</button></div></div>')
	}
}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

