$(document).ready(function(){
	$('.formToggle').click(function(e){
	   $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
	});


	$( "#lgbtn" ).click(function(e) {
	    e.preventDefault();
	    var that = $(this);
	    that.attr('disabled', true)
	    	.addClass('disabled')
	    	.text('logging in...');
	    
	    var username = $('#username').val();
		var password = $('#password').val();

		var login_obj={
			'email':username,
			'password':password
		}
		$.ajax({
		    url: "/api/doctor/login",
		    type: 'POST',
		    data:JSON.stringify(login_obj),
		    success: function(response){
		    	console.log('success')
				if(response.status == 1){
					window.location.href='/index';
					that.text('logged in');
				}
				else if(response.status == 0){
					$("#login-error-msg")
						.text(response.msg)
						.removeClass('hide');
					that.text('login')
						.attr('disabled', false)
						.removeClass('disabled');
				}

			},
		    error: function(error){
		    	console.log('fail')
		    	$("#login-error-msg")
		    		.text("Oops! There is some error please try again.")
		    		.removeClass('hide');
		    	that.text('login')
	        		.attr('disabled', false)
					.removeClass('disabled');
		    }
		  });
	});

	$( "#signupbtn" ).click(function(e) {
	 
		e.preventDefault();
		$('#signup-error-msg,#signup-success-msg').addClass('hide');
		var that = $(this);
	    var firstname = $('#name-prefix').val()+" "+$('#firstname').val();
		var lastname  = $('#name-prefix').val()+" "+$('#lastname').val();
		var email     = $('#email').val();
		var password  = $('#signuppassword').val();

		var signup_obj={
			
			'firstname':firstname,
			'lastname':lastname,
			'email':email,
			'password':password,
		}
		if($('#firstname').val().trim()=="" || $('#lastname').val().trim()=="" || $('#email').val().trim()=="" || $('#signuppassword').val().trim()==""){
			$('#signup-error-msg').html('Please fill all the fields.');
			$('#signup-error-msg').removeClass('hide');
			return;
		}
	    if(validateEmail($('#email').val().trim())==false)
	    {
	    	$('#signup-error-msg').html('Please enter a valid email address.');
			$('#signup-error-msg').removeClass('hide');
	    	return
	    }
	    that.attr('disabled', true)
	    	.addClass('disabled')
	    	.text('Please Wait...');
		if($("#gtccheck").is(':checked')){

			$.ajax({
		    url: "/api/doctor/signup",
		    type: 'POST',
		    data: JSON.stringify(signup_obj),
		    cache: false,
		    async: true,
		    datatype:'json',
		    contentType:'application/json',
		    beforeSend:function(){
		    	$('#signup-error-msg,#signup-success-msg').addClass('hide');
		    },
		      success: function(response){
		        if(response.status == 1){
		        	//$('#signup-success-msg').removeClass('hide');
		        	$('#signup-error-msg').addClass('hide');
		        	$('#login-page-btn').trigger('click');
		        	$('#login-error-msg').removeClass('hide')
		        	$('#login-error-msg').html('Sign up successful. Please log in using email address and password provided during sign up')
		        }
		        else if(response.status == 2){
		        	$('#signup-error-msg').text('Email already exists.');
		        	$('#signup-error-msg').removeClass('hide');
		        }
		        else if(response.status == 0){
		        	$('#signup-error-msg').text('Email already exists.');
		        	$('#signup-error-msg').removeClass('hide');
		        }
		        that.attr('disabled', false)
			    	.removeClass('disabled')
			    	.text('CREATE');
		      },
		    error: function(error){
		    	alert("Fail");
		    }
		  });

		}else{
			return 1
		}
	});


	$('#reset-btn').click(function(){
		var that = $(this);
		console.log('hello')
		$('.reset-error,.reset-msg').addClass('hide')
		if(validateEmail($('#registeredEmail').val())){
			that.text('PLEASE WAIT');
			$.ajax({
				method:"POST",
				url:"/api/doctor/link/hash",
				data:JSON.stringify({email:$('#registeredEmail').val()}),
				success:function(data){
					if(data.status==1)
					{
						$('.reset-msg').text('A link to reset password has been sent to your email.')
					}	
					if(data.status==0){
						$('.reset-msg').text('This is not a registered email id.')
					}
					$('.reset-msg').removeClass('hide');
					that.text('RESET');
					setTimeout(function(){
						$('.reset-error,.reset-msg').addClass('hide')
					},10000)
				}
			})
		}
		else
		{
			$('.reset-error').removeClass('hide')
		}
	})
})
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}