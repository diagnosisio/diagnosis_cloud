    // cornerstoneWADOImageLoader.configure({
    //     beforeSend: function(xhr) {
    //         // Add custom headers here (e.g. auth tokens)
    //         //xhr.setRequestHeader('x-auth-token', 'my auth token');
    //     }
    // });

//var loaded = false;


    var hounsfieldArray = []; 
    var intercept, slope;
    // h = slope*pxl+intercept
   function loadAndViewImage(imageId) {
        try {
            cornerstone.loadAndCacheImage(imageId).then(function(image) {
                intercept = image.intercept;
                slope = image.slope;
                var myData =  image.getPixelData();
                hounsfieldArray = myData;
            }, function(err) {
                alert(err);
            });
        }
        catch(err) {
            alert(err);
        }
    }


    function loadAll(){
        var ctr = 0;
        var len = $('.img-thumbnail-small img').length;
        $.each($.find(".img-thumbnail-small img"),function(i,e){
            var x = $(this);
            cornerstone.loadAndCacheImage("wadouri:"+x.attr('data-dicomurl')).then(function(image){
                x.css({"color":"black"});
                ctr++;
                $('#loader-div #msg-loader').html('Loading  '+(ctr*100/len).toFixed(2)+'%');
                $('#loader-div .progress-bar').attr('aria-valuenow',(ctr*100/len).toFixed(2));
                $('#loader-div .progress-bar').css('width',(ctr*100/len).toFixed(2)+'%')
                if(ctr==len){
                    $('#loader-div').hide();
                }
            })
        })
    }
    
    function downloadAndViewURL(url)
    {
        // prefix the url with wadouri: so cornerstone can find the image loader
        url = "wadouri:" + url;
        // image enable the dicomImage element and activate a few tools
        loadAndViewImage(url);
    }

    $(document).ready(function() {
        $(".dcmurl").click(function(e){
            url = $(this).attr("data-url");
            downloadAndViewURL(url);
        });
    });