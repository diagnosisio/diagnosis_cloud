var userState = 1;
$(document).ready(function(){
	var base_url = window.location.origin;
 	var drag=false,drag_initX,drag_initY; 
 	var dragged_element;
 	$('.draggable').mousedown(function(evt){
 		dragged_element = $(this).parent();
 		drag=true;
 		drag_initX = evt.pageX;
 		drag_initY = evt.pageY;
 	})
 	$('.draggable').mouseup(function(){
 		drag=false;
 	})
 	$(document).mouseup(function(){
 		drag = false;
 	})
 	$(document).mousemove(function(evt){
 		if(drag==true && dragged_element.find('.draggable').hasClass('not')==false)
 		{
 			drag_top=(dragged_element.css('top')).toString();
 			drag_top=parseInt(drag_top.slice(0,drag_top.length-2));
 			drag_left=(dragged_element.css('left')).toString();
 			drag_left=parseInt(drag_left.slice(0,drag_left.length-2));
 			dragged_element.css("top",(drag_top + (evt.pageY-drag_initY))+"px");
 			dragged_element.css("left",(drag_left + (evt.pageX- drag_initX))+"px");	
			dragged_element.css("top",(drag_top + (evt.pageY-drag_initY))+"px");
 			dragged_element.css("left",(drag_left + (evt.pageX- drag_initX))+"px");
 			drag_initX=evt.pageX;
 			drag_initY=evt.pageY;
 			dragged_element.css('right','auto')
 			dragged_element.css('bottom','auto')
 		}
 	})
 	$('.draggable').mousemove(function(evt){
 		if(drag==true && dragged_element.find('.draggable').hasClass('not')==false)
 		{
 			drag_top=(dragged_element.css('top')).toString();
 			drag_top=parseInt(drag_top.slice(0,drag_top.length-2));
 			drag_left=(dragged_element.css('left')).toString();
 			drag_left=parseInt(drag_left.slice(0,drag_left.length-2));
 			dragged_element.css("top",(drag_top + (evt.pageY-drag_initY))+"px");
 			dragged_element.css("left",(drag_left + (evt.pageX- drag_initX))+"px");	
 			drag_initX=evt.pageX;
 			drag_initY=evt.pageY;
 			dragged_element.css('right','auto')
 			dragged_element.css('bottom','auto')
 		}
 	})


 	$('.zoom-btn').click(function(){
 		$(this).toggleClass('active');
 		if($(this).hasClass('active')){
 			$('#main-view').removeClass('not')
 			$('#main-view.draggable').css('cursor','move');
 		}
 		else{
 			$('#main-view').addClass('not')
 			$('#main-view.draggable').css('cursor','default')	
 		}
 	})

 	$(document).ajaxStart(function() {
 		$('body').append('<div id="wait-screen">Please wait..</div>');
  		$('#wait-screen').css('display','flex');
    });
    $(document).ajaxComplete(function() {
  		$('#wait-screen').remove();
    });


 	$('[data-toggle="tooltip"]').tooltip({
       container: 'body'
    });
 	$('[data-toggle="tooltip"]').tooltip({
      trigger : 'hover'
  	}) 
 	$(document).on('click','.show-more-btn',function(){
 		var $_this = $(this);
		$_this.next().toggleClass('hide')
		if($_this.next().hasClass('hide')){
			$_this.find('.zmdi').addClass('zmdi-chevron-right')
			$_this.find('.zmdi').removeClass('zmdi-chevron-down')
		}
		else
		{
			$_this.find('.zmdi').removeClass('zmdi-chevron-right')
			$_this.find('.zmdi').addClass('zmdi-chevron-down')
		}
 	})
 	$(document).idle({
	  onIdle: function(){
	  	userState = 0;
	  },
	  idle: 1000*60
	})
	$(document).idle({
		onActive: function(){
			userState = 1;
		},
		idle:1000*60
	})
	getLocation();
	setInterval(function(){
		keepAlive(userState,geoLoc)
	},1000*60)
})

function keepAlive(state,geolocation){
	$.ajax({
		method:"POST",
		url:base_url+"/apis/activity/keepalive",
		global:false,
		data:JSON.stringify({
			active:state,
			location:geolocation
		}),
		success:function(data){
			// console.log(data)
		}
	})
}

var geoLoc=[],geoError;

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    } else { 
        geoError = "Geolocation is not supported by this browser.";
        geoLoc = [];
    }
}

function showPosition(position) {
    var latitude  = position.coords.latitude;
    var longitude = position.coords.longitude;
    geoLoc = [];
    geoLoc.push(latitude);
    geoLoc.push(longitude);
    keepAlive(userState,geoLoc);
}

function showError(error) {
    switch(error.code) {
        case error.PERMISSION_DENIED:
            geoError = "User denied the request for Geolocation.";
            geoLoc = [];
            break;
        case error.POSITION_UNAVAILABLE:
            geoError = "Location information is unavailable.";
            geoLoc = [];
            break;
        case error.TIMEOUT:
            geoError = "The request to get user location timed out.";
            geoLoc = [];
            break;
        case error.UNKNOWN_ERROR:
            geoError = "An unknown error occurred.";
            geoLoc = [];
            break;
    }
}
