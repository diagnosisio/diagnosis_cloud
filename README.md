Build Status: [![CircleCI](https://circleci.com/bb/diagnosisio/diagnosis_cloud.svg?style=svg)](https://circleci.com/bb/diagnosisio/diagnosis_cloud)

# DIAGNOSIS Tagging Tool
The tagging tool is divided into three stages namely stage 1, stage 2 and stage 3. 

## Stage 1
In this stage only tagging is allowed. A technician/radiologist can mark annotations, instance information and series information.

## Stage 2
In this stage tagging as well as verification can be done. A technician/radiologist can verify annotations/instance info/series info tagged by other users in stage 1. They can also add new annotation/instance info and series info.

## Stage 3

In this stage tagging as well as verification can be done but only radiologists are authorized for this stage. A radiologist can verify annotations/instance info/series info tagged by other users in stage 1 or stage 2. They can also add new annotation/instance info and series info.

## APIS 

# STAGE 1

### Stage 1: Series Info
#### Stage 1: Series Info : Add
~~~~
    Endpoint    :   /apis/series/allstages/add
    Type        :   POST
    Params      :   series_uuid [string]
                    label [string]
                    value [string]
                    comments [string]
                    stage [int]
~~~~
#### Stage 1: Series Info : Update
~~~~
    Endpoint    :   /apis/series/allstages/update
    Type        :   POST
    Params      :   series_info_id [int]
                    series_uuid [string]
                    label [string]
                    value [string]
                    comments [string]
                    stage [int]
~~~~
#### Stage 1: Series Info : Delete 
~~~~
    Endpoint    :   /apis/series/allstages/delete
    Type        :   POST
    Params      :   series_info_id [int]
                    stage [int]
~~~~

### Stage 1 : Instance Info
#### Stage 1: Instance Info : Add
~~~~
    Endpoint    :   /apis/instance/allstages/add
    Type        :   POST
    Params      :   series_uuid [string]
                    slice_id [int]
                    pathology_lvl1 [string]
                    anatomy_lvl1 [string]
                    anatomy_lvl2 [string]
                    comments [string]
                    severity [string]
                    stage [int]
~~~~
#### Stage 1: Instance Info : Update
~~~~
    Endpoint    :   /apis/instance/allstages/update
    Type        :   POST
    Params      :   instance_info_id [int]
                    series_uuid [string]
                    slice_id [int]
                    pathology_lvl1 [string]
                    anatomy_lvl1 [string]
                    anatomy_lvl2 [string]
                    severity [string]
                    comments [string]
                    stage [int]
~~~~
#### Stage 1: Instance Info : Delete
~~~~
    Endpoint    :   /apis/instance/allstages/delete
    Type        :   POST
    Params      :   instance_info_id [int]
                    stage [int]
~~~~

### Stage 1: Annotation
#### Stage 1: Annotation : Add
~~~~
    Endpoint    :   /apis/annotation/allstages/add
    Type        :   POST
    Params      :   series_uuid [string]
                    slice_id [int]
                    coords [json]
                    shape [string]
                    pathology_lvl1 [string]
                    anatomy_lvl1 [string]
                    anatomy_lvl2 [string]
                    comments [string]
                    severity [string]
                    stage [int]
~~~~
#### Stage 1: Annotation : Update
~~~~
    Endpoint    :   /apis/annotation/allstages/update
    Type        :   POST
    Params      :   annotation_id [int]
                    series_uuid [string]
                    slice_id [int]
                    coords [json]
                    shape [string]
                    pathology_lvl1 [string]
                    anatomy_lvl1 [string]
                    anatomy_lvl2 [string]
                    comments [string]
                    severity [string]
                    stage [int]
~~~~
#### Stage 1 : Annotation : Delete
~~~~
    Endpoint    :   /apis/annotation/allstages/delete
    Type        :   POST
    Params      :   annotation_id [int]
                    stage [int]
~~~~
# STAGE 2 : TAGGING 
### Stage 2: Tagging : Series Info
#### Stage 2: tagging : Series Info : Add 
~~~~
    Endpoint    :   /apis/series/allstages/add
    Type        :   POST
    Params      :   series_uuid [string]
                    label [string]
                    value [string]
                    comments [string]
                    stage [int]
~~~~
#### Stage 2: Tagging: Series Info : Update
~~~~
    Endpoint    :   /apis/series/allstages/update
    Type        :   POST
    Params      :   series_info_id [int]
                    series_uuid [string]
                    label [string]
                    value [string]
                    comments [string]
                    stage [int]
~~~~
#### Stage 2: Tagging: Series Info: Delete 
~~~~
    Endpoint    :   /apis/series/allstages/delete
    Type        :   POST
    Params      :   series_info_id [int]
                    stage [int]
~~~~

### Stage 2: Tagging: Instance Info
#### Stage 2: Tagging: Instance Info : Add
~~~~
    Endpoint    :   /apis/instance/allstages/add
    Type        :   POST
    Params      :   series_uuid [string]
                    slice_id [int]
                    pathology_lvl1 [string]
                    anatomy_lvl1 [string]
                    anatomy_lvl2 [string]
                    comments [string]
                    stage [int]
~~~~
#### Stage 2: Tagging : Instance Info: Update
~~~~
    Endpoint    :   /apis/instance/allstages/update
    Type        :   POST
    Params      :   instance_info_id [int]
                    series_uuid [string]
                    slice_id [int]
                    pathology_lvl1 [string]
                    anatomy_lvl1 [string]
                    anatomy_lvl2 [string]
                    comments [string]
                    stage [int]
~~~~
#### Stage 2: Tagging: Instance Info : Delete
~~~~
    Endpoint    :   /apis/instance/allstages/delete
    Type        :   POST
    Params      :   instance_info_id [int]
                    stage [int]
~~~~

### Stage 2: Tagging: Annotations
#### Stage 2: Tagging: Annotations: Add
~~~~
    Endpoint    :   /apis/annotation/allstages/add
    Type        :   POST
    Params      :   series_uuid [string]
                    slice_id [int]
                    coords [json]
                    shape [string]
                    pathology_lvl1 [string]
                    anatomy_lvl1 [string]
                    anatomy_lvl2 [string]
                    comments [string]
                    severity [string]
                    stage [int]
~~~~
#### Stage 2: Tagging : Annotations : Update
~~~~
    Endpoint    :   /apis/annotation/allstages/update
    Type        :   POST
    Params      :   annotation_id [int]
                    series_uuid [string]
                    slice_id [int]
                    coords [json]
                    shape [string]
                    pathology_lvl1 [string]
                    anatomy_lvl1 [string]
                    anatomy_lvl2 [string]
                    comments [string]
                    severity [string]
                    stage [int]
~~~~
#### Stage 2: Tagging : Annotations : Delete
~~~~
    Endpoint    :   /apis/annotation/allstages/delete
    Type        :   POST
    Params      :   annotation_id [int]
                    stage [int]
~~~~
# STAGE 2: Verification
### Stage 2: Verification: Series Info:
#### Stage 2: Verification : Series Info : Add 
~~~~
    Endpoint    :   /apis/verification/series/stage2/add
    Type        :   POST
    Params      :   stage_1_series_info_id [int]
                    series_uuid [string]
                    status [string]
                    edit [int]
                    label [string]
                    value [string]
                    comments [string]
~~~~
#### Stage 2: Verification : Series Info : Update 
~~~~
    Endpoint    :   /apis/verification/series/stage2/update
    Type        :   POST
    Params      :   stage_2_series_info_id [int]
                    stage_1_series_info_id [int]
                    series_uuid [string]
                    status [string]
                    edit [int]
                    label [string]
                    value [string]
                    comments [string]
~~~~
#### Stage 2: Verification: Series Info : Delete 
~~~~
    Endpoint    :   /apis/verification/series/stage2/delete
    Type        :   POST
    Params      :   stage_2_series_info_id [int]
~~~~

### STAGE 2 : Verification: Instance Info:
#### Stage 2 : Verification: Instance Info : Add 
~~~~
    Endpoint    :   /apis/verification/instance/stage2/add
    Type        :   POST
    Params      :   stage_1_instance_info_id [int]
                    series_uuid [string]
                    slice_id [int]
                    status [string]
                    comments [string]
                    edit [int]
                    pathology_lvl1 [string]
                    anatomy_lvl1 [string]
                    anatomy_lvl2 [string]
                    severity [string]
~~~~
#### Stage 2 : Verification : Instance Info : Update 
~~~~
    Endpoint    :   /apis/verification/instance/stage2/update
    Type        :   POST
    Params      :   stage_2_instance_info_id [int]
                    stage_1_instance_info_id [int]
                    series_uuid [string]
                    slice_id [int]
                    status [string]
                    comments [string]
                    edit [int]
                    pathology_lvl1 [string]
                    anatomy_lvl1 [string]
                    anatomy_lvl2 [string]
                    severity [string]
~~~~
#### Stage 2: Verification: Instance Info: Delete 
~~~~
    Endpoint    :   /apis/verification/instance/delete
    Type        :   POST
    Params      :   stage_2_instance_info_id [int]
~~~~

### Stage 2: Verification : Annotations
#### Stage 2: Verification: Annotation: Add 
~~~~
    Endpoint    :   /apis/verification/annotations/stage2/add
    Type        :   POST
    Params      :   series_uuid [string]
                    slice_id [int]
                    stage_1_annotation_id [int]
                    status [string]
                    comments [string]
                    edit [int]
                    coords [json]
                    shape [string]
                    pathology_lvl1 [string]
                    anatomy_lvl1 [string]
                    anatomy_lvl2 [string]
                    severity [string]
~~~~
#### Stage 2: Verification: Annotation: Update 
~~~~
    Endpoint    :   /apis/verification/annotations/stage2/update
    Type        :   POST
    Params      :   stage_2_annotation_id [int]
                    series_uuid [string]
                    slice_id [int]
                    stage_1_annotation_id [int]
                    status [string]
                    comments [string]
                    edit [int]
                    coords [json]
                    shape [string]
                    pathology_lvl1 [string]
                    anatomy_lvl1 [string]
                    anatomy_lvl2 [string]
                    severity [string]
~~~~
#### Stage 2: Verification: Annotation : Delete 
~~~~
    Endpoint    :   /apis/verification/annotations/stage2/delete
    Type        :   POST
    Params      :   stage_2_annotation_id [int]
~~~~
# STAGE 3 : TAGGING 
### Stage 3: Tagging : Series Info
#### Stage 3: tagging : Series Info : Add 
~~~~
    Endpoint    :   /apis/series/allstages/add
    Type        :   POST
    Params      :   series_uuid [string]
                    label [string]
                    value [string]
                    comments [string]
                    stage [int]
~~~~
#### Stage 3: Tagging: Series Info : Update
~~~~
    Endpoint    :   /apis/series/allstages/update
    Type        :   POST
    Params      :   series_info_id [int]
                    series_uuid [string]
                    label [string]
                    value [string]
                    comments [string]
                    stage [int]
~~~~
#### Stage 3: Tagging: Series Info: Delete 
~~~~
    Endpoint    :   /apis/series/allstages/delete
    Type        :   POST
    Params      :   series_info_id [int]
                    stage [int]
~~~~

### Stage 3: Tagging: Instance Info
#### Stage 3: Tagging: Instance Info : Add
~~~~
    Endpoint    :   /apis/instance/allstages/add
    Type        :   POST
    Params      :   series_uuid [string]
                    slice_id [int]
                    pathology_lvl1 [string]
                    anatomy_lvl1 [string]
                    anatomy_lvl2 [string]
                    comments [string]
                    stage [int]
~~~~
#### Stage 3: Tagging : Instance Info: Update
~~~~
    Endpoint    :   /apis/instance/allstages/update
    Type        :   POST
    Params      :   instance_info_id [int]
                    series_uuid [string]
                    slice_id [int]
                    pathology_lvl1 [string]
                    anatomy_lvl1 [string]
                    anatomy_lvl2 [string]
                    comments [string]
                    stage [int]
~~~~
#### Stage 3: Tagging: Instance Info : Delete
~~~~
    Endpoint    :   /apis/instance/allstages/delete
    Type        :   POST
    Params      :   instance_info_id [int]
                    stage [int]
~~~~

### Stage 3: Tagging: Annotations
#### Stage 3: Tagging: Annotations: Add
~~~~
    Endpoint    :   /apis/annotation/allstages/add
    Type        :   POST
    Params      :   series_uuid [string]
                    slice_id [int]
                    coords [json]
                    shape [string]
                    pathology_lvl1 [string]
                    anatomy_lvl1 [string]
                    anatomy_lvl2 [string]
                    comments [string]
                    severity [string]
                    stage [int]
~~~~
#### Stage 3: Tagging : Annotations : Update
~~~~
    Endpoint    :   /apis/annotation/allstages/update
    Type        :   POST
    Params      :   annotation_id [int]
                    series_uuid [string]
                    slice_id [int]
                    coords [json]
                    shape [string]
                    pathology_lvl1 [string]
                    anatomy_lvl1 [string]
                    anatomy_lvl2 [string]
                    comments [string]
                    severity [string]
                    stage [int]
~~~~
#### Stage 3: Tagging : Annotations : Delete
~~~~
    Endpoint    :   /apis/annotation/allstages/delete
    Type        :   POST
    Params      :   annotation_id [int]
                    stage [int]
~~~~
# STAGE 3: Verification
### Stage 3: Verification: Series Info:
#### Stage 3: Verification : Series Info : Add 
~~~~
    Endpoint    :   /apis/verification/series/stage3/add
    Type        :   POST
    Params      :   stage_1_series_info_id [int]
                :   stage_2_series_info_id [int]
                    series_uuid [string]
                    status [string]
                    edit [int]
                    label [string]
                    value [string]
                    comments [string]
~~~~
#### Stage 3: Verification : Series Info : Update 
~~~~
    Endpoint    :   /apis/verification/series/stage3/update
    Type        :   POST
    Params      :   stage_3_series_inof_id [int]
                    stage_2_series_info_id [int]
                    stage_1_series_info_id [int]
                    series_uuid [string]
                    status [string]
                    edit [int]
                    label [string]
                    value [string]
                    comments [string]
~~~~
#### Stage 3: Verification: Series Info : Delete 
~~~~
    Endpoint    :   /apis/verification/series/stage3/update
    Type        :   POST
    Params      :   stage_3_series_info_id [int]
~~~~

### STAGE 3 : Verification: Instance Info:
#### Stage 3 : Verification: Instance Info : Add 
~~~~
    Endpoint    :   /apis/verification/instance/stage3/add
    Type        :   POST
    Params      :   stage_2_instance_info_id [int]
                    stage_1_instance_info_id [int]
                    series_uuid [string]
                    slice_id [int]
                    status [string]
                    comments [string]
                    edit [int]
                    pathology_lvl1 [string]
                    anatomy_lvl1 [string]
                    anatomy_lvl2 [string]
                    severity [string]
~~~~
#### Stage 3 : Verification : Instance Info : Update 
~~~~
    Endpoint    :   /apis/verification/instance/stage3/update
    Type        :   POST
    Params      :   stage_3_instance_info_id [int]
                    stage_2_instance_info_id [int]
                    stage_1_instance_info_id [int]
                    series_uuid [string]
                    slice_id [int]
                    status [string]
                    comments [string]
                    edit [int]
                    pathology_lvl1 [string]
                    anatomy_lvl1 [string]
                    anatomy_lvl2 [string]
                    severity [string]
~~~~
#### Stage 3: Verification: Instance Info: Delete 
~~~~
    Endpoint    :   /apis/verification/instance/stage3/delete
    Type        :   POST
    Params      :   stage_3_instance_info_id [int]
~~~~

### Stage 3: Verification : Annotations
#### Stage 3: Verification: Annotation: Add 
~~~~
    Endpoint    :   /apis/verification/annotations/stage3/add
    Type        :   POST
    Params      :   stage_2_annotation_id [int]
                    series_uuid [string]
                    slice_id [int]
                    stage_1_annotation_id [int]
                    status [string]
                    comments [string]
                    edit [int]
                    coords [json]
                    shape [string]
                    pathology_lvl1 [string]
                    anatomy_lvl1 [string]
                    anatomy_lvl2 [string]
                    severity [string]
                    verification_stage [string: 1/2/3]
~~~~
#### Stage 3: Verification: Annotation: Update 
~~~~
    Endpoint    :   /apis/verification/annotations/stage3/update
    Type        :   POST
    Params      :   stage_3_annotation_id [int]
                    stage_2_annotation_id [int]
                    series_uuid [string]
                    slice_id [int]
                    stage_1_annotation_id [int]
                    status [string]
                    comments [string]
                    edit [int]
                    coords [json]
                    shape [string]
                    pathology_lvl1 [string]
                    anatomy_lvl1 [string]
                    anatomy_lvl2 [string]
                    severity [string]
~~~~
#### Stage 3: Verification: Annotation : Delete 
~~~~
    Endpoint    :   /apis/verification/annotations/stage3/delete
    Type        :   POST
    Params      :   stage_3_annotation_id [int]
~~~~
# View Series
~~~~
    Endpoint    :  /apis/view/<params>
    Type        : POST
    Params      : series_uuid
    Returns     : Data containing info about the particular series_uuid
~~~~
# View Reports
~~~~
    Endpoint    : /apis/reports/<params>
    Type        : POST
    Returns     : List of data containing reports for a particular patient id
~~~~


    
