import MySQLdb
from db_config import db_connection


def logdata(userid, event, ip, user_agent, location):
    diagdb = db_connection()
    cursor = diagdb.cursor()
    sql = "INSERT INTO ids_track_activity(user_id, \
    event,ip,user_agent,location) \
    VALUES ('%s','%s','%s','%s','%s')" % \
        (userid, event, ip, user_agent, location)
    x = cursor.execute(sql)
    uid = cursor.lastrowid
    diagdb.commit()
    cursor.close()
    diagdb.close()
