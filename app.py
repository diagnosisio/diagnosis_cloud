import os
import json
import datetime
from apis_serve import *
from apis_stage_1_2_3_annotate import *
from apis_stage_1_2_3_instance import *
from apis_stage_1_2_3_series import *
from apis_stage_2_verify_annotations import *
from apis_stage_2_verify_instance import *
from apis_stage_2_verify_series import *
from apis_stage_3_verify_annotations import *
from apis_stage_3_verify_instance import *
from apis_stage_3_verify_series import *
from apis_activity import logdata
from db_config import db_connection
from conf import CLIENT_ID, CLIENT_SECRET, REDIRECT_URI
import flask
from flask import Flask, url_for, redirect, \
    render_template, session, request, make_response
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager, login_required, login_user, \
    logout_user, current_user, UserMixin
from flask_cors import CORS, cross_origin
from requests_oauthlib import OAuth2Session
from requests.exceptions import HTTPError

basedir = os.path.abspath(os.path.dirname(__file__))
os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'

def set_custom_headers(r):
    r.headers.set('Access-Control-Allow-Origin', ' *')
    r.headers.set('Access-Control-Allow-Credentials', 'true')
    r.headers.set('Access-Control-Allow-Methods',
                  'GET, POST, PUT, DELETE, OPTIONS')
    r.headers.set('Access-Control-Allow-Headers',
                  'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
    r.headers.set("Content-Type", "application/json")
    return r

"""App Configuration"""


class Auth:
    """Google Project Credentials"""
    CLIENT_ID = CLIENT_ID
    CLIENT_SECRET = CLIENT_SECRET
    REDIRECT_URI = REDIRECT_URI
    AUTH_URI = 'https://accounts.google.com/o/oauth2/auth'
    TOKEN_URI = 'https://accounts.google.com/o/oauth2/token'
    USER_INFO = 'https://www.googleapis.com/userinfo/v2/me'
    SCOPE = ['profile', 'email']
    HD = 'diagnosis.io'


class Config:
    """Base config"""
    APP_NAME = "Diagnosis.io"
    SECRET_KEY = os.environ.get("SECRET_KEY") or "ahwan'ssupersecret"
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevConfig(Config):
    """Dev config"""
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, "test.db")


class ProdConfig(Config):
    """Production config"""
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, "prod.db")


config = {
    "dev": DevConfig,
    "prod": ProdConfig,
    "default": DevConfig
}

"""APP creation and configuration"""
app = Flask(__name__)
CORS(app)
app.config.from_object(config['dev'])
db = SQLAlchemy(app)
login_manager = LoginManager(app)
login_manager.login_view = "login"
login_manager.session_protection = "strong"

""" DB Models """


class User(db.Model, UserMixin):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True, nullable=False)
    userid = db.Column(db.String(100), unique=True, nullable=False)
    name = db.Column(db.String(100), nullable=True)
    scope = db.Column(db.Integer, nullable=True)
    avatar = db.Column(db.String(200))
    tokens = db.Column(db.Text)
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow())


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))
""" OAuth Session creation """


def get_google_auth(state=None, token=None):
    if token:
        return OAuth2Session(Auth.CLIENT_ID, token=token)
    if state:
        return OAuth2Session(
            Auth.CLIENT_ID,
            state=state,
            redirect_uri=Auth.REDIRECT_URI)
    oauth = OAuth2Session(
        Auth.CLIENT_ID,
        redirect_uri=Auth.REDIRECT_URI,
        scope=Auth.SCOPE,)
    return oauth



@app.route('/add/user')
def add_new():
    if current_user.userid == "DIAG62482639" or current_user.userid == "DIAG12345678":
        return render_template('/add_new.html')
    else:
        return "Unauthorized"
@app.route('/add/new_member',methods=['POST'])
def add_member():
    if current_user.userid == "DIAG62482639" or current_user.userid == "DIAG12345678":
        email = request.form["email"]
        fname = request.form["fname"]
        lname = request.form["lname"]
        plevel = request.form["plevel"]
        salutation = request.form["sal"]
        response = add_new_member(plevel,email,fname,lname,salutation,current_user.userid)
        return response
    else:
        return "Unauthorized"

@app.route('/')
@login_required
def index():
    if current_user.scope == 15:
        return render_template('dashboard.html')
    elif current_user.scope in [1, 2, 3]:
        return redirect(url_for('tagging_launcher'))
    else:
        return render_template('notregistered.html')


@app.route('/login')
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    google = get_google_auth()
    auth_url, state = google.authorization_url(
        Auth.AUTH_URI, access_type='offline')
    session['oauth_state'] = state
    return render_template('login3.html', auth_url=auth_url)

@app.route('/login/admin')
def login_admin():
    return render_template('/login_admin.html')

@app.route('/login/admin/gCallback',methods=['GET','POST'])
def admin_callback():
    diagdb = db_connection()
    cursor = diagdb.cursor()
    user_name = request.form['user_name']
    password = request.form['password']
    cursor.execute("SELECT * from users where user_id='%s' and status='active'"%(user_name,))
    userdata = cursor.fetchone()
    cursor.close()
    diagdb.close()
    if len(userdata)>=1:
        if password == "diagnosis_admin":
            user = User()
            if userdata:
                user.name = userdata["salutation"]+" "+userdata["first_name"]+" "+userdata["last_name"]
                user.scope = userdata["privilege_level"]
                user.userid = userdata["user_id"]
                user.email = userdata["email"]
            else:
                user.scope = 0
            print user.scope
            db.session.add(user)
            db.session.commit()
            login_user(user)
            logdata(current_user.userid, "logged_in",
                    request.remote_addr, request.headers.get('User-Agent'), [])
            return redirect(url_for('index'))
        else:
            return "Wrong password"
    else:
        return login_admin()



@app.route('/gCallback')
def callback():
    if current_user is not None and current_user.is_authenticated:
        return redirect(url_for('index'))
    if 'error' in request.args:
        if request.args.get('error') == 'access_denied':
            return 'You denied access.'
        return 'Error encountered.'
    if 'code' not in request.args and 'state' not in request.args:
        return redirect(url_for('login'))
    else:
        google = get_google_auth(state=session['oauth_state'])
        try:
            token = google.fetch_token(
                Auth.TOKEN_URI,
                client_secret=Auth.CLIENT_SECRET,
                authorization_response=request.url)
        except HTTPError:
            return 'HTTPError occurred.'
        google = get_google_auth(token=token)
        resp = google.get(Auth.USER_INFO)
        if resp.status_code == 200:
            user_data = resp.json()
            email = user_data['email']
            user = User.query.filter_by(email=email).first()
            if user is None:
                user = User()
                user.email = email
            user.name = user_data['name']
            diagdb = db_connection()
            cursor = diagdb.cursor()
            cursor.execute(
                "SELECT privilege_level,user_id,status FROM users WHERE email = %s and status = 'active'", (user.email,))
            userdata = cursor.fetchone()

            print userdata
            cursor.close()
            diagdb.close()
            if userdata:
               user.scope = userdata["privilege_level"]
               user.userid = userdata["user_id"]
               user.status = userdata["status"]
               if user.status != "active":
                return 'Could not fetch your information.'
            else:
                user.scope = 0
            print user.scope
            print(token)
            user.tokens = json.dumps(token)
            user.avatar = user_data['picture']
            db.session.add(user)
            db.session.commit()
            login_user(user)
            logdata(current_user.userid, "logged_in",
                    request.remote_addr, request.headers.get('User-Agent'), [])
            return redirect(url_for('index'))
        return 'Could not fetch your information.'

#@app.before_request
#def before_request():
 #    flask.session.permanent = True
  #   app.permanent_session_lifetime = datetime.timedelta(seconds=60)
   #  flask.session.modified = True

@app.route('/logout')
@login_required
def logout():
    logdata(current_user.userid, "logged_out", request.remote_addr,
            request.headers.get('User-Agent'), [])
    logout_user()
    return redirect(url_for('index'))


@app.route('/activate')
@login_required
def requestactivation():
    print current_user.email

    return json.dumps({"status": 1, "message": "Pending for activation", "timestamp": str(datetime.datetime.now())})


@app.route('/tag/launch')
@login_required
def tagging_launcher():
    return render_template('tagging_launch.html')


@app.route('/tag/stage3')
@login_required
def tagging_stage3():
    if current_user.scope in [3]:
        return render_template('stage3.html')
    else:
        return redirect(url_for('index'))

@app.route('/tag/home')
@login_required
def tagging_home():
    print current_user.email
    if current_user.scope in [1, 2]:
        return render_template('tagging.html')
    else:
        return redirect(url_for('index'))


@app.route('/apis/tagging/')
@login_required
def api_check():
    print hello()
    return json.dumps({"message": "Everything Working!", "status": 1})

@app.route('/apis/view/<params>',methods=['POST', 'GET'])
def view(params):
    data = view_response(params)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r
@app.route('/apis/reports/<params>',methods=['POST', 'GET'])
def reports(params):
    data = reports_response(params)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r

@app.route('/apis/tagging/serve/<params>', methods=['POST', 'GET'])
@login_required
def serve(params):
    # print "L", current_user.scope
    userid = current_user.userid
    privilege_level = current_user.scope
    data = ImageDataServe(userid, privilege_level, params)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r

#---------------------------------------- ALL STAGES SERIES APIS--------------------------------------------------------#

# Done


@app.route('/apis/series/allstages/add', methods=['POST'])
@login_required
def add_series_info_all_stages():
    params = json.loads(request.get_data())
    userid = current_user.userid
    series_uuid = params.get("series_uuid")
    label = params.get("label")
    value = params.get("value")
    stage = params.get("stage")
    comments = params.get("comments")
    data = AddSeriesInfo(
        userid=userid, series_uuid=series_uuid, label=label, value=value, comments=comments, stage=stage)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r

# Done


@app.route('/apis/series/allstages/update', methods=['POST'])
@login_required
def edit_series_info_all_stages():
    params = json.loads(request.get_data())
    seriesinfo_uid = params.get("series_info_id")
    userid = current_user.userid
    series_uuid = params.get("series_uuid")
    label = params.get("label")
    value = params.get("value")
    stage = params.get("stage")
    comments = params.get("comments")
    data = UpdateSeriesInfo(userid=userid, series_uuid=series_uuid, stage=stage,
                            label=label, value=value, series_info_id=seriesinfo_uid, comments=comments)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r

# Done


@app.route('/apis/series/allstages/delete', methods=['POST'])
@login_required
def delete_series_info_all_stages():
    params = json.loads(request.get_data())
    userid = current_user.userid
    seriesinfo_uid = params.get("series_info_id")
    stage = params.get("stage")
    data = DeleteSeriesInfo(series_info_id=seriesinfo_uid, user_id=userid, stage=stage)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r

#================================================= ALL STAGES ============================================================#
#---------------------------------------- ALL STAGES INSTANCE APIS--------------------------------------------------------#

# Done


@app.route('/apis/instance/allstages/add', methods=['POST'])
@login_required
def add_instance_info_all_stages():
    params = json.loads(request.get_data())
    userid = current_user.userid
    series_uuid = params.get("series_uuid")
    slice_uid = params.get("slice_id")
    pathology_lvl1 = params.get("pathology_lvl1")
    anatomy_lvl1 = params.get("anatomy_lvl1")
    anatomy_lvl2 = params.get("anatomy_lvl2")
    severity = params.get("severity")
    comments = params.get("comments")
    stage = params.get("stage")
    data = AddInstanceInfo(userid=userid, series_uuid=series_uuid, slice_id=slice_uid,
                           pathology_lvl1=pathology_lvl1, anatomy_lvl1=anatomy_lvl1, anatomy_lvl2=anatomy_lvl2, severity=severity, comments=comments, stage=stage)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r

# Done


@app.route('/apis/instance/allstages/update', methods=['POST'])
@login_required
def edit_instance_info_all_stages():
    params = json.loads(request.get_data())
    instanceinfo_uid = params.get("instance_info_id")
    userid = current_user.userid
    series_uuid = params.get("series_uuid")
    slice_uid = params.get("slice_id")
    pathology_lvl1 = params.get("pathology_lvl1")
    anatomy_lvl1 = params.get("anatomy_lvl1")
    anatomy_lvl2 = params.get("anatomy_lvl2")
    severity = params.get("severity")
    comments = params.get("comments")
    stage = params.get("stage")
    data = UpdateInstanceInfo(userid=userid, series_uuid=series_uuid, slice_id=slice_uid, pathology_lvl1=pathology_lvl1,
                              anatomy_lvl1=anatomy_lvl1, anatomy_lvl2=anatomy_lvl2, instance_info_id=instanceinfo_uid, stage=stage, severity=severity, comments=comments)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r

# Done


@app.route('/apis/instance/allstages/delete', methods=['POST'])
@login_required
def delete_instance_info_all_stages():
    params = json.loads(request.get_data())
    userid = current_user.userid
    instanceinfo_uid = params.get("instance_info_id")
    stage = params.get("stage")
    data = DeleteInstanceInfo(instance_info_id=instanceinfo_uid, userid=userid, stage=stage)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r

#-------------------------------------- ALL STAGES ANNOTATION APIS--------------------------------------------------------#

# Done


@app.route('/apis/annotation/allstages/add', methods=['POST'])
@login_required
def add_annotations_all_stages():
    params = json.loads(request.get_data())
    userid = current_user.userid
    series_uuid = params.get("series_uuid")
    slice_uid = params.get("slice_id")
    coords = params.get("coords")
    pathology_lvl1 = params.get("pathology_lvl1")
    anatomy_lvl1 = params.get("anatomy_lvl1")
    anatomy_lvl2 = params.get("anatomy_lvl2")
    comments = params.get("comments")
    shape = params.get("shape")
    severity = params.get("severity")
    stage = params.get("stage")
    data = Add_annotations_stage_1(userid=userid, series_uuid=series_uuid, slice_id=slice_uid, coords=coords, pathology_lvl1=pathology_lvl1,
                                   anatomy_lvl1=anatomy_lvl1, anatomy_lvl2=anatomy_lvl2, comments=comments, shape=shape, severity=severity, stage=stage)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r

# Done


@app.route('/apis/annotation/allstages/update', methods=['POST'])
#@login_required
def edit_annotations_all_stages():
    params = json.loads(request.get_data())
    annotation_uid = params.get("annotation_id")
    userid = current_user.userid
    series_uuid = params.get("series_uuid")
    slice_uid = params.get("slice_id")
    coords = params.get("coords")
    pathology_lvl1 = params.get("pathology_lvl1")
    anatomy_lvl1 = params.get("anatomy_lvl1")
    anatomy_lvl2 = params.get("anatomy_lvl2")
    comments = params.get("comments")
    shape = params.get("shape")
    severity = params.get("severity")
    stage = params.get("stage")
    data = Update_annotations_stage_1(userid=userid, series_uuid=series_uuid, slice_id=slice_uid, coords=coords, pathology_lvl1=pathology_lvl1, anatomy_lvl1=anatomy_lvl1,
                                      anatomy_lvl2=anatomy_lvl2, comments=comments, annotation_id=annotation_uid, shape=shape, severity=severity, stage=stage)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r

# Done


@app.route('/apis/annotation/allstages/delete', methods=['POST'])
@login_required
def delete_annotations_all_stages():
    params = json.loads(request.get_data())
    userid = current_user.userid
    annotation_uid = params.get("annotation_id")
    stage = params.get("stage")
    data = Delete_annotations_stage_1(annotation_id=annotation_uid, userid=userid, stage=stage)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r


#================================================== END OF ALL STAGES ==============================================================#


#======================================================= STAGE 2 ===================================================================#
#-----------------------------------------VERIFICATION STAGE 2 SERIES APIS----------------------------------------------------------#

# Done
@app.route('/apis/verification/series/stage2/add', methods=['POST'])
@login_required
def add_series_info_stage_2():
    params = json.loads(request.get_data())
    verifier_user_id = current_user.userid
    series_uuid = params.get("series_uuid")
    status = params.get("status")
    comments = params.get("comments")
    edit = params.get("edit")
    label = params.get("label")
    value = params.get("value")
    stage_1_series_info_id = params.get("stage_1_series_info_id")

    data = Add_verified_series_info_stage_2(series_uuid=series_uuid, label=label, value=value, stage_1_series_info_id=stage_1_series_info_id,
                                            verifier_user_id=verifier_user_id, status=status, comments=comments, edit=edit)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r

# Done


@app.route('/apis/verification/series/stage2/update', methods=['POST'])
@login_required
def edit_series_info_stage_2():
    params = json.loads(request.get_data())
    verifier_user_id = current_user.userid
    series_uuid = params.get("series_uuid")
    status = params.get("status")
    comments = params.get("comments")
    edit = params.get("edit")
    label = params.get("label")
    value = params.get("value")
    stage_1_series_info_id = params.get("stage_1_series_info_id")
    stage_2_series_info_id = params.get("stage_2_series_info_id")

    data = Update_verified_series_info_stage_2(stage_2_series_info_id=stage_2_series_info_id, series_uuid=series_uuid, label=label, value=value,
                                               stage_1_series_info_id=stage_1_series_info_id, verifier_user_id=verifier_user_id, status=status, comments=comments, edit=edit)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r
# Done


@app.route('/apis/verification/series/stage2/delete', methods=['POST'])
@login_required
def delete_series_info_stage_2():
    params = json.loads(request.get_data())
    verifier_user_id = current_user.userid
    stage_2_series_info_id = params.get("stage_2_series_info_id")
    data = Delete_verified_series_info_stage_2(
        stage_2_series_info_id=stage_2_series_info_id, verifier_user_id=verifier_user_id)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r
#----------------------------------------VERIFICATION STAGE 2 INSTANCE APIS--------------------------------------------------------#

# Done


@app.route('/apis/verification/instance/stage2/add', methods=['POST'])
@login_required
def add_instance_info_stage_2():
    params = json.loads(request.get_data())
    verifier_user_id = current_user.userid
    series_uuid = params.get("series_uuid")
    slice_id = params.get("slice_id")
    stage_1_instance_info_id = params.get("stage_1_instance_info_id")
    status = params.get("status")
    comments = params.get("comments")
    edit = params.get("edit")
    pathology_lvl1 = params.get("pathology_lvl1")
    anatomy_lvl1 = params.get("anatomy_lvl1")
    anatomy_lvl2 = params.get("anatomy_lvl2")
    severity = params.get("severity")
    data = Add_verified_instance_info_stage_2(stage_1_instance_info_id=stage_1_instance_info_id, edit=edit, verifier_user_id=verifier_user_id, series_uuid=series_uuid, slice_id=slice_id, comments=comments,
                                              severity=severity, pathology_lvl1=pathology_lvl1, anatomy_lvl1=anatomy_lvl1, anatomy_lvl2=anatomy_lvl2, status=status)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r

# Done


@app.route('/apis/verification/instance/stage2/update', methods=['POST'])
@login_required
def edit_instance_info_stage_2():
    params = json.loads(request.get_data())
    verifier_user_id = current_user.userid
    stage_2_instance_info_id = params.get("stage_2_instance_info_id")
    stage_1_instance_info_id = params.get("stage_1_instance_info_id")
    series_uuid = params.get("series_uuid")
    slice_id = params.get("slice_id")
    status = params.get("status")
    comments = params.get("comments")
    edit = params.get("edit")
    pathology_lvl1 = params.get("pathology_lvl1")
    anatomy_lvl1 = params.get("anatomy_lvl1")
    anatomy_lvl2 = params.get("anatomy_lvl2")
    severity = params.get("severity")
    data = Update_verified_instance_info_stage_2(verifier_user_id=verifier_user_id, stage_1_instance_info_id=stage_1_instance_info_id, stage_2_instance_info_id=stage_2_instance_info_id, series_uuid=series_uuid, slice_id=slice_id,
                                                 status=status, comments=comments, edit=edit, pathology_lvl1=pathology_lvl1, anatomy_lvl1=anatomy_lvl1, anatomy_lvl2=anatomy_lvl2, severity=severity)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r

# Done


@app.route('/apis/verification/instance/stage2/delete', methods=['POST'])
@login_required
def delete_instance_info_stage_2():
    params = json.loads(request.get_data())
    verifier_user_id = current_user.userid
    stage_2_instance_info_id = params.get("stage_2_instance_info_id")
    data = Delete_verified_instance_info_stage_2(
        stage_2_instance_info_id=stage_2_instance_info_id, verifier_user_id=verifier_user_id)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r
#----------------------------------------VERIFICATION STAGE 2 ANNOTATIONS APIS-----------------------------------------------------#


@app.route('/apis/verification/annotations/stage2/add', methods=['POST'])
def add_annotations_stage_2():
    params = json.loads(request.get_data())
    userid = current_user.userid
    series_uuid = params.get("series_uuid")
    slice_id = params.get("slice_id")
    stage_1_annotation_id = params.get("stage_1_annotation_id")
    status = params.get("status")
    comments = params.get("comments")
    edit = params.get("edit")
    coords = params.get("coords")
    shape = params.get("shape")
    pathology_lvl1 = params.get("pathology_lvl1")
    anatomy_lvl1 = params.get("anatomy_lvl1")
    anatomy_lvl2 = params.get("anatomy_lvl2")
    severity = params.get("severity")
    data = Add_verified_annotations_stage_2(userid=userid, series_uuid=series_uuid, slice_id=slice_id, stage_1_annotation_id=stage_1_annotation_id, status=status, comments=comments,
                                            edit=edit, coords=coords, shape=shape, pathology_lvl1=pathology_lvl1, anatomy_lvl1=anatomy_lvl1, anatomy_lvl2=anatomy_lvl2, severity=severity)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r


@app.route('/apis/verification/annotations/stage2/update', methods=['POST'])
def edit_annotations_stage_2():
    params = json.loads(request.get_data())
    userid = current_user.userid
    stage_2_annotation_id = params.get("stage_2_annotation_id")
    params = json.loads(request.get_data())
    series_uuid = params.get("series_uuid")
    slice_id = params.get("slice_id")
    stage_1_annotation_id = params.get("stage_1_annotation_id")
    status = params.get("status")
    comments = params.get("comments")
    edit = params.get("edit")
    coords = params.get("coords")
    shape = params.get("shape")
    pathology_lvl1 = params.get("pathology_lvl1")
    anatomy_lvl1 = params.get("anatomy_lvl1")
    anatomy_lvl2 = params.get("anatomy_lvl2")
    severity = params.get("severity")
    data = Update_verified_annotations_stage_2(stage_2_annotation_id=stage_2_annotation_id, userid=userid, series_uuid=series_uuid, slice_id=slice_id, stage_1_annotation_id=stage_1_annotation_id,
                                               status=status, comments=comments, edit=edit, coords=coords, shape=shape, pathology_lvl1=pathology_lvl1, anatomy_lvl1=anatomy_lvl1, anatomy_lvl2=anatomy_lvl2, severity=severity)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r


@app.route('/apis/verification/annotations/stage2/delete', methods=['POST'])
def delete_annotations_stage_2():
    params = json.loads(request.get_data())
    userid = current_user.userid
    stage_2_annotation_id = params.get("stage_2_annotation_id")
    data = Delete_verified_annotations_stage_2(
        stage_2_annotation_id=stage_2_annotation_id, userid=userid)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r


#=================================================== END OF STAGE 2 ================================================================#


#======================================================= STAGE 3 ===================================================================#
#-----------------------------------------VERIFICATION STAGE 3 SERIES APIS----------------------------------------------------------#


@app.route('/apis/verification/series/stage3/add', methods=['POST'])
#@login_required
def add_series_info_stage_3():
    params = json.loads(request.get_data())
    verifier_user_id = current_user.userid
    series_uuid = params.get("series_uuid")
    status = params.get("status")
    comments = params.get("comments")
    edit = params.get("edit")
    label = params.get("label")
    value = params.get("value")
    verification_stage = params.get("verification_stage")
    stage_1_series_info_id = params.get("stage_1_series_info_id")
    stage_2_series_info_id = params.get("stage_2_series_info_id")
    data = Add_verified_series_info_stage_3(verification_stage=verification_stage,series_uuid=series_uuid, label=label, value=value, stage_1_series_info_id=stage_1_series_info_id,
                                            verifier_user_id=verifier_user_id, status=status, comments=comments, edit=edit, stage_2_series_info_id=stage_2_series_info_id)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r


@app.route('/apis/verification/series/stage3/update', methods=['POST'])
#@login_required
def edit_series_info_stage_3():
    params = json.loads(request.get_data())
    verifier_user_id = current_user.userid
    series_uuid = params.get("series_uuid")
    status = params.get("status")
    comments = params.get("comments")
    edit = params.get("edit")
    label = params.get("label")
    value = params.get("value")
    stage_3_series_info_id = params.get("stage_3_series_info_id")
    data = Update_verified_series_info_stage_3( series_uuid=series_uuid, label=label, value=value, verifier_user_id=verifier_user_id, status=status, comments=comments, edit=edit, stage_3_series_info_id=stage_3_series_info_id)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r


@app.route('/apis/verification/series/stage3/delete', methods=['POST'])
#@login_required
def delete_series_info_stage_3():
    params = json.loads(request.get_data())
    verifier_user_id = current_user.userid
    stage_3_series_info_id = params.get("stage_3_series_info_id")
    data = Delete_verified_series_info_stage_3(
        stage_3_series_info_id=stage_3_series_info_id, verifier_user_id=verifier_user_id)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r
#----------------------------------------VERIFICATION STAGE 3 INSTANCE APIS--------------------------------------------------------#


@app.route('/apis/verification/instance/stage3/add', methods=['POST'])
#@login_required
def add_instance_info_stage_3():
    params = json.loads(request.get_data())
    verifier_user_id = current_user.userid
    stage_2_instance_info_id = params.get("stage_2_instance_info_id")
    stage_1_instance_info_id = params.get("stage_1_instance_info_id")
    series_uuid = params.get("series_uuid")
    slice_id = params.get("slice_id")
    status = params.get("status")
    comments = params.get("comments")
    edit = params.get("edit")
    pathology_lvl1 = params.get("pathology_lvl1")
    anatomy_lvl1 = params.get("anatomy_lvl1")
    anatomy_lvl2 = params.get("anatomy_lvl2")
    severity = params.get("severity")
    verification_stage = params.get('verification_stage')
    data = Add_verified_instance_info_stage_3( stage_1_instance_info_id=stage_1_instance_info_id, stage_2_instance_info_id=stage_2_instance_info_id, edit=edit, verifier_user_id=verifier_user_id, series_uuid=series_uuid, slice_id=slice_id, comments=comments,
                                              severity=severity, pathology_lvl1=pathology_lvl1, anatomy_lvl1=anatomy_lvl1, anatomy_lvl2=anatomy_lvl2, status=status, verification_stage=verification_stage)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r


@app.route('/apis/verification/instance/stage3/update', methods=['POST'])
#@login_required
def edit_instance_info_stage_3():
    params = json.loads(request.get_data())
    verifier_user_id = current_user.userid
    stage_3_instance_info_id = params.get("stage_3_instance_info_id")
    series_uuid = params.get("series_uuid")
    slice_id = params.get("slice_id")
    status = params.get("status")
    comments = params.get("comments")
    edit = params.get("edit")
    pathology_lvl1 = params.get("pathology_lvl1")
    anatomy_lvl1 = params.get("anatomy_lvl1")
    anatomy_lvl2 = params.get("anatomy_lvl2")
    severity = params.get("severity")
    data = Update_verified_instance_info_stage_3(stage_3_instance_info_id=stage_3_instance_info_id, verifier_user_id=verifier_user_id, series_uuid=series_uuid, slice_id=slice_id,
                                                 status=status, comments=comments, edit=edit, pathology_lvl1=pathology_lvl1, anatomy_lvl1=anatomy_lvl1, anatomy_lvl2=anatomy_lvl2, severity=severity)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r


@app.route('/apis/verification/instance/stage3/delete', methods=['POST'])
#@login_required
def delete_instance_info_stage_3():
    params = json.loads(request.get_data())
    verifier_user_id = current_user.userid
    stage_3_instance_info_id = params.get("stage_3_instance_info_id")
    data = Delete_verified_instance_info_stage_3(
        stage_3_instance_info_id=stage_3_instance_info_id, verifier_user_id=verifier_user_id)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r
#---------------------------------------- STAGE 3 ANNOTATIONS APIS-----------------------------------------------------#


@app.route('/apis/verification/annotations/stage3/add', methods=['POST'])
#@login_required
def add_annotations_stage_3():
    params = json.loads(request.get_data())
    userid = current_user.userid
    series_uuid = params.get("series_uuid")
    slice_id = params.get("slice_id")
    stage_1_annotation_id = params.get("stage_1_annotation_id")
    stage_2_annotation_id = params.get("stage_2_annotation_id")
    status = params.get("status")
    comments = params.get("comments")
    edit = params.get("edit")
    coords = params.get("coords")
    shape = params.get("shape")
    pathology_lvl1 = params.get("pathology_lvl1")
    anatomy_lvl1 = params.get("anatomy_lvl1")
    anatomy_lvl2 = params.get("anatomy_lvl2")
    severity = params.get("severity")
    verification_stage = params.get("verification_stage")
    data = Add_verified_annotations_stage_3(userid=userid, series_uuid=series_uuid, slice_id=slice_id, stage_1_annotation_id=stage_1_annotation_id, status=status, comments=comments,
                                            edit=edit, coords=coords, shape=shape, pathology_lvl1=pathology_lvl1, anatomy_lvl1=anatomy_lvl1, anatomy_lvl2=anatomy_lvl2, severity=severity, stage_2_annotation_id=stage_2_annotation_id,verification_stage=verification_stage)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r


@app.route('/apis/verification/annotations/stage3/update', methods=['POST'])
#@login_required
def edit_annotations_stage_3():
    params = json.loads(request.get_data())
    params = json.loads(request.get_data())
    #userid = current_user.userid
    userid = current_user.userid
    series_uuid = params.get("series_uuid")
    slice_id = params.get("slice_id")
    stage_3_annotation_id = params.get("stage_3_annotation_id")
    status = params.get("status")
    comments = params.get("comments")
    edit = params.get("edit")
    coords = params.get("coords")
    shape = params.get("shape")
    pathology_lvl1 = params.get("pathology_lvl1")
    anatomy_lvl1 = params.get("anatomy_lvl1")
    anatomy_lvl2 = params.get("anatomy_lvl1")
    severity = params.get("severity")
    verification_stage = params.get("verification_stage")
    data = Update_verified_annotations_stage_3(userid=userid, series_uuid=series_uuid, slice_id=slice_id,
                                               status=status, comments=comments, edit=edit, coords=coords, shape=shape, pathology_lvl1=pathology_lvl1, anatomy_lvl1=anatomy_lvl1, anatomy_lvl2=anatomy_lvl2, severity=severity, stage_3_annotation_id=stage_3_annotation_id,verification_stage=verification_stage)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r


@app.route('/apis/verification/annotations/stage3/delete', methods=['POST'])
#@login_required
def delete_annotations_stage_3():
    params = json.loads(request.get_data())
    userid = current_user.userid
    #userid = current_user.userid
    stage_3_annotation_id = params.get("stage_3_annotation_id")
    data = Delete_verified_annotations_stage_3(
        stage_3_annotation_id=stage_3_annotation_id, userid=userid)
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r
#-----------------------------------------ACTIVITY TRACKING APIS-------------------------------------------------------#


@app.route('/apis/activity/keepalive', methods=['POST'])
@login_required
def activity_log():
    params = json.loads(request.get_data())
    userid = current_user.userid
    active = int(params.get("active"))
    location = params.get("location")
    status = 'active' if active == 1 else 'inactive'
    logdata(current_user.userid, status, request.remote_addr,
            request.headers.get('User-Agent'), location)
    data = {"status": 1, "message": "Received"}
    r = make_response(json.dumps(data))
    r = set_custom_headers(r)
    return r
