import MySQLdb
import datetime
import json
from itertools import groupby
import dicom,pandas as pd
from db_config import db_connection
import operator,traceback
import re
import random

_PRIORITY = {"STAGE3": 1, "STAGE2": 2, "STAGE1": 3}
stage_dict = {1: "STAGE1", 2: "STAGE2", 3: "STAGE3"}
_STAGE_DICT_REV = {"STAGE1": 1, "STAGE2": 2, "STAGE3": 3}


def add_new_member(plevel,email,fname,lname,sal,added_by):
    """
    Function to add new member to to the tagging tool
    :param plevel: privilege_level of a particular user
    :param email : email of the user
    :param fname : First name of the user 
    :param lname : Last name of the user 
    :param sal   : Salutation of the user 
    :param added_by : Datetime for the user added
    :return : Success or failure whether the user is added or not.
    """
    diagdb = db_connection()
    cursor = diagdb.cursor()
    userid = "DIAG"+str(random.randint(10000000,99999999))
    sql = "INSERT into users(status,category,privilege_level,user_id,email,first_name,last_name,salutation,added_by) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s')"%("active","pro",plevel,userid,email,fname,lname,sal,added_by) 
    try:
        x = cursor.execute(sql)
        diagdb.commit()
        cursor.close()
        diagdb.close()
        return "Success"
    except:
        return "Fail"

def make_medical_tree():
    """
    Function to create medical_tree for diagnosis.
    :return : Medical tree
    """
    diagdb = db_connection()
    cursor = diagdb.cursor()
    cursor.execute("SELECT * from ids_raw_medical_tree")
    tree_cur = cursor.fetchall()
    tree_dict = {}
    for row in tree_cur:
        if row["body_protocol"] is not None and row["body_protocol"] not in tree_dict.keys():
            tree_dict[row["body_protocol"]] = {}
        if row["tag_category"] is not None and row["tag_category"] not in tree_dict[row["body_protocol"]].keys():
            tree_dict[row["body_protocol"]][row["tag_category"]] = {}
        if row["tag_shape"] is not None and row["tag_shape"] not in tree_dict[row["body_protocol"]][row["tag_category"]].keys():
            tree_dict[row["body_protocol"]][
                row["tag_category"]][row["tag_shape"]] = {}
        if row["pathology_lvl1"] is not None and row["pathology_lvl1"] not in tree_dict[row["body_protocol"]][row["tag_category"]][row["tag_shape"]].keys():
            tree_dict[row["body_protocol"]][row["tag_category"]][
                row["tag_shape"]][row["pathology_lvl1"]] = {}
        if row["anatomy_lvl1"] is not None and row["anatomy_lvl1"] not in tree_dict[row["body_protocol"]][row["tag_category"]][row["tag_shape"]][row["pathology_lvl1"]].keys():
            tree_dict[row["body_protocol"]][row["tag_category"]][
                row["tag_shape"]][row["pathology_lvl1"]][row["anatomy_lvl1"]] = {}
        if row["anatomy_lvl2"] is not None and row["anatomy_lvl2"] not in tree_dict[row["body_protocol"]][row["tag_category"]][row["tag_shape"]][row["pathology_lvl1"]][row["anatomy_lvl1"]].keys():
            tree_dict[row["body_protocol"]][row["tag_category"]][row["tag_shape"]][
                row["pathology_lvl1"]][row["anatomy_lvl1"]][row["anatomy_lvl2"]] = {}
    cursor.close()
    diagdb.close()
    return tree_dict


def updateskipnext(serve_id, stage, skip, next):
    """
    Function to update the required table when a user skip a series or clicks next.
    :param serve_id : serve_id of the series
    :param stage : stage in which the series is present 
    :param skip : if skip is clicked 
    :param next : if next is clicked
    """
    diagdb = db_connection()
    cursor = diagdb.cursor()
    if stage == "STAGE1":
        if next:
            sql = "UPDATE ids_serve_stage_1 SET is_stage_1_completed = %d WHERE stage_1_serve_id = %d" % (1, int(serve_id))
            cursor.execute(sql)
        elif skip:
            sql = "UPDATE ids_serve_stage_1 SET is_stage_1_skipped = %d WHERE stage_1_serve_id = %d" % (1, int(serve_id))
            cursor.execute(sql)

    elif stage == "STAGE2":
        if next:
            sql = "UPDATE ids_serve_stage_2 SET is_stage_2_completed = %d WHERE stage_2_serve_id = %d" % (1, int(serve_id))
            cursor.execute(sql)
        elif skip:
            sql = "UPDATE ids_serve_stage_2 SET is_stage_2_skipped = %d WHERE stage_2_serve_id = %d" % (1, int(serve_id))
            cursor.execute(sql)

    elif stage == "STAGE3":
        if next:
            sql = "UPDATE ids_serve_stage_3 SET is_stage_3_completed = %d WHERE stage_3_serve_id = %d" % (1, int(serve_id))
            cursor.execute(sql)
        elif skip:
            sql = "UPDATE ids_serve_stage_3 SET is_stage_3_skipped = %d WHERE stage_3_serve_id = %d" % (1, int(serve_id))
            cursor.execute(sql)        
    diagdb.commit()
    cursor.close()
    diagdb.close()


def getStage(userid, privilege_level):
    """
    Function to get the stage for which the user will get data based on its privilege Privilege level
    :param userid : id of the user.
    :param privilege_level : Privilege level of the user
    :return : stage for which data has to sent for the user.
    """
    print privilege_level
    diagdb = db_connection()
    cursor = diagdb.cursor()
    cursor.execute(
        "SELECT stage_1,stage_2,stage_3 FROM user_privileges WHERE privilege_level = %s", (privilege_level,))
    cursordata = cursor.fetchone()
    stage_1 = cursordata["stage_1"]
    stage_2 = cursordata["stage_2"]
    stage_3 = cursordata["stage_3"]
    eligible_stages = {}
    if stage_1 == 1:
        eligible_stages["STAGE1"] = _PRIORITY["STAGE1"]
    if stage_2 == 1:
        eligible_stages["STAGE2"] = _PRIORITY["STAGE2"]
    if stage_3 == 1:
        eligible_stages["STAGE3"] = _PRIORITY["STAGE3"]
    # Get stage with maximum priority
    stage = min(eligible_stages.iteritems(), key=operator.itemgetter(1))[0]
    print stage
    cursor.close()
    diagdb.close()
    return stage

def threshold_tagging(user_id):
    """
    Function to return stage based on the distribution of data that is sent for tagging and verification.
    This function is used normalize the distribution as in every user should get data for tagging as well as for verification in a desired ratio.
    :param userid : id for the user.
    :return : stage for which data has to be sent.
    """
    diagdb = db_connection()
    cursor = diagdb.cursor()
    date_now = datetime.datetime.now().date()
    date_seven = date_now - datetime.timedelta(days =7)
    check = str(date_seven)
    sql = "SELECT user_id,count(*) FROM tyrion.ids_serve_stage_1 where added_timestamp>='%s'  group by user_id"%(check,)
    cursor.execute(sql)
    row = cursor.fetchall()
    count = 0
    df_stage_1 = pd.DataFrame(columns=["count","user_id"])
    for data in row:
        df_stage_1.loc[count,"user_id"] = data["user_id"]
        df_stage_1.loc[count,"count"] = data["count(*)"]
        count+=1
    sql = "SELECT user_id,count(*) FROM tyrion.ids_serve_stage_2 where added_timestamp>='%s' group by user_id"%(check,)
    cursor.execute(sql)
    row = cursor.fetchall()
    count = 0
    df_stage_2 = pd.DataFrame(columns=["count","user_id"])
    for data in row:
        df_stage_2.loc[count,"user_id"] = data["user_id"]
        df_stage_2.loc[count,"count"] = data["count(*)"]
        count+=1
    for i in range(0,len(df_stage_1)):
        try:
            df_stage_1.loc[i,"count_stage_2"] = (df_stage_2[df_stage_2["user_id"]==df_stage_1["user_id"][i]]["count"].unique()[0])
            df_stage_1.loc[i,"ratio"] =(df_stage_2[df_stage_2["user_id"]==df_stage_1["user_id"][i]]["count"].unique()[0])/ float(df_stage_1["count"][i])
        except:
            traceback.format_exc()
            df_stage_1.loc[i,"ratio"] = 0
    mean = (df_stage_1["ratio"].mean())
    for i in range(0,len(df_stage_1)):
        df_stage_1.loc[i,"Variance"] = (df_stage_1["ratio"][i]-mean)*(df_stage_1["ratio"][i]-mean)

    threshold_value = df_stage_1["Variance"].min()
    threshold_ratio = df_stage_1[df_stage_1["Variance"]==threshold_value]["ratio"].unique()[0]
    print(threshold_ratio)
    if len(df_stage_1[df_stage_1["user_id"]==user_id]["ratio"].unique()) >0 : 
        value_of_tagger = df_stage_1[df_stage_1["user_id"]==user_id]["ratio"].unique()[0]
        print value_of_tagger
        if (value_of_tagger == 0) :
            print "gone"
            count_stage_1 = df_stage_1[df_stage_1["user_id"]==user_id]["count"].unique()
            count_s_1 = count_stage_1[0] if len(count_stage_1) != 0 else 0 
            count_stage_2 = df_stage_1[df_stage_1["user_id"]==user_id]["count_stage_2"].unique()
            count_s_2 = count_stage_2[0] if len(count_stage_2) !=0 else 0
            if count_s_2>count_s_1:
                return "STAGE1"
                
            else:
                return "STAGE2"
                
        if (value_of_tagger<=threshold_ratio):
            return "STAGE2"
        else:
            return "STAGE1"
    else :
        return '-1'
        
def assign_new_series(userid, privilege_level):
    """
    Function for assigning a new series to a user based on its privilege level.
    :param userid: id for the user.
    :param privilege_level: Privilege level of the user.
    :return: series_uuid,stage and serve_id related to a series.
    """
    print "ASSIGN NEW SERIES"
    diagdb = db_connection()
    cursor = diagdb.cursor()
    #Threshold decider
    if privilege_level == '2' :
        stage = threshold_tagging(userid)
    else:
        stage = getStage(userid, privilege_level)

    #If for serving data 
    if stage =='-1' :
        stage = getStage(userid, privilege_level)


    # stage = "STAGE3"

    if stage == "STAGE3":
        cursor.execute("SELECT stage_1_serve_id,stage_2_serve_id,series_uuid from ids_stage3_priority where stage_2_serve_id NOT IN (SELECT stage_2_serve_id from ids_serve_stage_3) order by stage_2_serve_id LIMIT 1")
        cursordata = cursor.fetchone()
        if len(cursordata) == 0 :
            cursor.execute("SELECT stage_1_serve_id,stage_2_serve_id,series_uuid from ids_serve_stage_2 where is_stage_2_completed = %s and user_id <> %s and stage_2_serve_id NOT IN (SELECT stage_2_serve_id from ids_serve_stage_3) order by stage_2_serve_id LIMIT 1",(1,userid))
            cursordata = cursor.fetchone()
        if cursordata:
            series_uuid = cursordata["series_uuid"]
            #series marked in stage 3
            stage_1_serve_id = int(cursordata["stage_1_serve_id"])
            stage_2_serve_id = int(cursordata["stage_2_serve_id"])
            # series_uuid = "0c75ed00-d261-4ddf-a409-6f40d54e9b07"
            # stage_2_serve_id = 2457
            # stage_1_serve_id = 96

            cursor.execute(
                "INSERT INTO ids_serve_stage_3(series_uuid,user_id,stage_1_serve_id,stage_2_serve_id,is_stage_3_served) VALUES ('%s','%s','%d','%d','%d')" % (series_uuid, userid, stage_1_serve_id,stage_2_serve_id, 1))
            serve_id = cursor.lastrowid
            cursor.execute(
                "INSERT INTO ids_serve_current (user_id, serve_id, stage) VALUES('%s','%d','%d') ON DUPLICATE KEY UPDATE serve_id=%d, stage = %d" % (userid, serve_id, 3, serve_id, 3))
        else:
            print "NO Data available for tagging in stage 3. Setting stage = 2"
            stage = "STAGE2"
    
    if stage == "STAGE2":
        cursor.execute(
            "SELECT stage_1_serve_id,series_uuid from ids_serve_stage_1 where is_stage_1_completed = %s and user_id <> %s and stage_1_serve_id NOT IN (SELECT stage_1_serve_id from ids_serve_stage_2) order by stage_1_serve_id LIMIT 1", (1, userid))
        cursordata = cursor.fetchone()
        if cursordata:
            series_uuid = cursordata.get("series_uuid")
            print "S", series_uuid
            stage_1_serve_id = cursordata["stage_1_serve_id"]
            cursor.execute(
                "INSERT INTO ids_serve_stage_2(series_uuid,user_id,stage_1_serve_id,is_stage_2_served) VALUES ('%s','%s','%d','%d')" % (series_uuid, userid, stage_1_serve_id, 1))
            serve_id = cursor.lastrowid
            cursor.execute(
                "INSERT INTO ids_serve_current (user_id, serve_id, stage) VALUES('%s','%d','%d') ON DUPLICATE KEY UPDATE serve_id=%d, stage = %d" % (userid, serve_id, 2, serve_id, 2))
        else:
            print "NO Data available for tagging in stage 2. Setting stage = 1"
            stage = "STAGE1"
    if stage == "STAGE1":
        cursor.execute(
            "SELECT series_uuid from ids_raw_series where served = %s LIMIT 1", (0,))
        cursordata = cursor.fetchone()
        if cursordata :
            series_uuid = cursordata["series_uuid"]
            cursor.execute("UPDATE ids_raw_series set served = %s where series_uuid = %s", (1, series_uuid))
            cursor.execute("INSERT INTO ids_serve_stage_1(series_uuid,user_id,is_stage_1_served) VALUES ('%s','%s','%d')" % (series_uuid, userid, 1))
            serve_id = cursor.lastrowid
            cursor.execute("INSERT INTO ids_serve_current (user_id, serve_id, stage) VALUES('%s','%d','%d') ON DUPLICATE KEY UPDATE serve_id=%d, stage = %d" % (userid, serve_id, 1, serve_id, 1))
        elif privilege_level == 2 :
            cursor.execute("SELECT stage_1_serve_id,series_uuid from ids_serve_stage_1 where is_stage_1_completed = %s and user_id <> %s and stage_1_serve_id NOT IN (SELECT stage_1_serve_id from ids_serve_stage_2) order by stage_1_serve_id LIMIT 1", (1, userid))
            cursordata = cursor.fetchone()
            if cursordata:
                series_uuid = cursordata.get("series_uuid")
                print "S", series_uuid
                stage_1_serve_id = cursordata["stage_1_serve_id"]
                cursor.execute("INSERT INTO ids_serve_stage_2(series_uuid,user_id,stage_1_serve_id,is_stage_2_served) VALUES ('%s','%s','%d','%d')" % (series_uuid, userid, stage_1_serve_id, 1))
                serve_id = cursor.lastrowid
                cursor.execute("INSERT INTO ids_serve_current (user_id, serve_id, stage) VALUES('%s','%d','%d') ON DUPLICATE KEY UPDATE serve_id=%d, stage = %d" % (userid, serve_id, 2, serve_id, 2))
            else:
                print "NO Data available for tagging in stage 2."

    diagdb.commit()
    cursor.close()
    diagdb.close()
    return series_uuid,stage, serve_id


def getCurrentData(userid, privilege_level):
    """
    Function to get data of the user on which he has left his work. It is fetched from ids_serve_current table.
    :param userid: id of the user.
    :privilege_level : Privilege level of the user.
    """
    diagdb = db_connection()
    cursor = diagdb.cursor()
    cursor.execute(
        "SELECT serve_id,stage FROM ids_serve_current WHERE user_id = %s", (userid,))
    cursordata = cursor.fetchone()
    if cursordata:
        # OLD USER
        print "SERVING OLD SERIES"
        serve_id = cursordata['serve_id']
        print serve_id
        stage = cursordata['stage']
        print stage
        if stage == 3:
            cursor.execute(
                "SELECT series_uuid FROM ids_serve_stage_3 WHERE stage_3_serve_id = %s", (serve_id,))
            cursordata = cursor.fetchone()
            print "SERIES: ", cursordata
            series_uuid = cursordata["series_uuid"]
            
        if stage == 2:
            cursor.execute(
                "SELECT series_uuid FROM ids_serve_stage_2 WHERE stage_2_serve_id = %s", (serve_id,))
            cursordata = cursor.fetchone()
            print "SERIES: ", cursordata
            series_uuid = cursordata["series_uuid"]
        if stage == 1:
            cursor.execute(
                "SELECT series_uuid FROM ids_serve_stage_1 WHERE stage_1_serve_id = %s", (serve_id,))
            cursordata = cursor.fetchone()
            print "X", cursordata
            series_uuid = cursordata["series_uuid"]

    else:
        # FIRST TIME USER
        series_uuid, stage, serve_id = assign_new_series(
            userid, privilege_level)
        stage = _STAGE_DICT_REV[stage]
    cursor.close()
    diagdb.close()
    return series_uuid, stage_dict[stage], serve_id


def serve_stage_1_response(series_uuid):
    """
    Function for giving stage 1 response to frontend in a fixed pattern.
    :param series_uuid: Series_uuid for the series
    :response : response for frontend
    """
    diagdb = db_connection()
    cursor = diagdb.cursor()
    resp = {}
    resp["status"] = 1
    resp["stage"] = 1
    resp["timestamp"] = str(datetime.datetime.now())
    resp["data"] = {}
    resp["data"]["series"] = {}

    # Get Series Data
    cursor.execute("SELECT * FROM ids_raw_series WHERE series_uuid = %s", (series_uuid,))
    series_db_data = cursor.fetchone()
    resp["data"]["series"]["series_uuid"] = series_db_data.get("series_uuid")
    resp["data"]["series"]["date_of_scan"] = str(series_db_data.get("date_of_scan"))
    resp["data"]["series"]["study_description"] = series_db_data.get("study_description", "")
    resp["data"]["series"]["series_description"] = series_db_data.get("series_description", "")
    resp["data"]["series"]["patient_id"] = series_db_data.get("patient_id")
    resp["data"]["series"]["patient_age"] = series_db_data.get("patient_age", "")
    resp["data"]["series"]["patient_sex"] = series_db_data.get("patient_sex", "")
    resp["data"]["series"]["series_info"] = {}

    # Get Series Info
    cursor.execute("SELECT * FROM ids_marked_series_info WHERE series_uuid = %s AND stage = 1 ORDER BY series_info_id", (series_uuid,))
    series_db_data = cursor.fetchall()
    for data in series_db_data:
        resp["data"]["series"]["series_info"]["stage 1"]["series_info_id"] = data["series_info_id"]
        resp["data"]["series"]["series_info"]["stage 1"]["added_timestamp"] = str(data["added_timestamp"])
        resp["data"]["series"]["series_info"]["stage 1"]["label"] = data["label"]
        resp["data"]["series"]["series_info"]["stage 1"]["value"] = data["value"]
        resp["data"]["series"]["series_info"]["stage 1"]["comments"] = data["comments"]

    # Fetch Image Data from DB
    cursor.execute("SELECT slice_id,dcmlink,jpglink_brain_window FROM ids_raw_images WHERE series_uuid = %s", (series_uuid,))
    imagedata = cursor.fetchall()
    # Fetch Instance Info from DB
    cursor.execute("SELECT * from ids_marked_instance_info I1 WHERE series_uuid = %s AND stage = 1 order by slice_id; ", (series_uuid,))
    instance_info_db_data = cursor.fetchall()
    # Fetch Annotation data from DB
    cursor.execute("SELECT * from ids_marked_annotations WHERE series_uuid = %s AND stage = 1 order by slice_id;", (series_uuid,))
    annotation_db_data = cursor.fetchall()
    image_resp = []
    i = 0
    for data in imagedata:
        imagedict = {}
        imagedict["dcmlink"] = data["dcmlink"].replace('http://', 'https://')
        imagedict["jpglink_brain_window"] = data["jpglink_brain_window"].replace('http://', 'https://')
        imagedict["slice_id"] = data["slice_id"]
        imagedict["slice_sequence_id"] = i

        # Add Instance Info to Imagedict
        instance_info_dict = {}
        stage_1_instance_resp = []
        for idata in instance_info_db_data:
            idata_dict = {}
            if idata["slice_id"] == data["slice_id"]:
                idata_dict["instanceinfo_uid"] = idata["instanceinfo_id"]
                idata_dict["pathology_lvl1"] = idata["pathology_lvl1"]
                idata_dict["anatomy_lvl1"] = idata["anatomy_lvl1"]
                idata_dict["anatomy_lvl2"] = idata["anatomy_lvl2"]
                idata_dict["comments"] = idata["comments"]
                idata_dict["severity"] = idata["severity"]
                stage_1_instance_resp.append(idata_dict)
            if stage_1_instance_resp:
                instance_info_dict["stage_1"] = stage_1_instance_resp
        if instance_info_dict:
            imagedict["instance_info"] = instance_info_dict
        image_resp.append(imagedict)
        i += 1

        # Add Annotation Info to Imagedict
        annotation_dict = {}
        stage_1_annotation_resp = []
        for adata in annotation_db_data:
            adata_dict = {}
            if adata["slice_id"] == data["slice_id"]:
                adata_dict["annotation_id"] = adata["annotation_id"]
                adata_dict["added_timestamp"] = str(adata["timestamp"])
                adata_dict["coords"] = json.loads(adata["coords"])
                adata_dict["shape"] = adata["shape"]
                adata_dict["pathology_lvl1"] = adata["pathology_lvl1"]
                adata_dict["anatomy_lvl1"] = adata["anatomy_lvl1"]
                adata_dict["anatomy_lvl2"] = adata["anatomy_lvl2"]
                adata_dict["severity"] = adata["severity"]
                adata_dict["comments"] = adata["comments"]
                stage_1_annotation_resp.append(adata_dict)
            if stage_1_annotation_resp:
                annotation_dict["stage_1"] = stage_1_annotation_resp

        if annotation_dict:
            imagedict["annotations"] = annotation_dict

    resp["data"]["series"]["instances"] = image_resp

    tree_dict = make_medical_tree()
    resp["meta"] = {}
    resp["meta"]["medical_tree"] = tree_dict

    return resp


def serve_stage_2_response(userid, series_uuid, serve_id):
    """
    Function for giving stage 2 response to frontend in a fixed pattern.
    :param series_uuid: Series_uuid for the series
    """
    diagdb = db_connection()
    cursor = diagdb.cursor()
    resp = {}
    resp["status"] = 1
    resp["stage"] = 2
    resp["timestamp"] = str(datetime.datetime.now())
    resp["data"] = {}
    resp["data"]["series"] = {}

    # Get Series Data
    cursor.execute("SELECT * FROM ids_raw_series WHERE series_uuid = %s", (series_uuid,))
    series_db_data = cursor.fetchone()
    resp["data"]["series"]["series_uuid"] = series_db_data.get("series_uuid")
    resp["data"]["series"]["date_of_scan"] = str(series_db_data.get("date_of_scan"))
    resp["data"]["series"]["study_description"] = series_db_data.get("study_description", "")
    resp["data"]["series"]["series_description"] = series_db_data.get("series_description", "")
    resp["data"]["series"]["patient_id"] = series_db_data.get("patient_id")
    resp["data"]["series"]["patient_age"] = series_db_data.get("patient_age", "")
    resp["data"]["series"]["patient_sex"] = series_db_data.get("patient_sex", "")
    resp["data"]["series"]["series_info"] = {}

    # Get Series Info
    cursor.execute("SELECT S1.series_info_id,S1.added_timestamp,S1.label,S1.value,S1.comments,S1.stage,S2.stage_2_series_info_id,S2.added_timestamp,S2.status,S2.comments,S2.edit,S2.label,S2.value from ids_marked_series_info S1 LEFT JOIN ids_stage_2_verify_series_info S2 ON S2.stage_1_series_info_id = S1.series_info_id WHERE S1.series_uuid = %s  order by S1.series_info_id;", (series_uuid,))
    series_info_db_data = cursor.fetchall()
    stage_1_series_resp = []
    stage_2_series_resp = []
    for data in series_info_db_data:
        if data["stage"] == 1:
            series_dict = {}
            series_dict["series_info_id"] = data["series_info_id"]
            series_dict["added_timestamp"] = str(data["added_timestamp"])
            series_dict["label"] = data["label"]
            series_dict["value"] = data["value"]
            series_dict["comments"] = data["comments"]
            if data["stage_2_series_info_id"]:
                stage_2_series_dict = {}
                stage_2_series_dict["stage_2_series_info_id"] = data["stage_2_series_info_id"]
                stage_2_series_dict["added_timestamp"] = str(data["S2.added_timestamp"])
                stage_2_series_dict["status"] = data["status"]
                stage_2_series_dict["comments"] = data["S2.comments"]
                stage_2_series_dict["edit"] = data["edit"]
                stage_2_series_dict["label"] = data["S2.label"]
                stage_2_series_dict["value"] = data["S2.value"]
                series_dict["stage_2"] = stage_2_series_dict
            stage_1_series_resp.append(series_dict)
        elif data["stage"] == 2:
            series_dict = {}
            series_dict["series_info_id"] = data["series_info_id"]
            series_dict["added_timestamp"] = str(data["added_timestamp"])
            series_dict["label"] = data["label"]
            series_dict["value"] = data["value"]
            series_dict["comments"] = data["comments"]
            stage_2_series_resp.append(series_dict)


    # Add resp
    resp["data"]["series"]["series_info"]["stage_1"] = stage_1_series_resp
    resp["data"]["series"]["series_info"]["stage_2"] = stage_2_series_resp

    # Fetch Image Data from DB
    cursor.execute("SELECT slice_id,dcmlink,jpglink_brain_window FROM ids_raw_images WHERE series_uuid = %s", (series_uuid,))
    imagedata = cursor.fetchall()
    # Fetch Instance Info from DB
    cursor.execute("SELECT I1.instanceinfo_id,I1.added_timestamp,I1.series_uuid,I1.slice_id,I1.pathology_lvl1,I1.anatomy_lvl1,I1.anatomy_lvl2,I1.severity,I1.comments,I1.stage,I2.stage_2_instance_info_id,I2.added_timestamp,I2.status,I2.comments,I2.edit,I2.pathology_lvl1,I2.anatomy_lvl1,I2.anatomy_lvl2,I2.severity from ids_marked_instance_info I1 LEFT JOIN ids_stage_2_verify_instance_info I2 ON I2.stage_1_instance_info_id = I1.instanceinfo_id WHERE I1.series_uuid = %s order by I1.slice_id; ", (series_uuid,))
    instance_info_db_data = cursor.fetchall()
    # Fetch Annotation data from DB
    cursor.execute("SELECT A1.annotation_id,A1.timestamp,A1.slice_id,A1.coords,A1.shape,A1.pathology_lvl1,A1.anatomy_lvl1,A1.anatomy_lvl2,A1.severity,A1.comments,A1.stage,A2.verification_id,A2.added_timestamp,A2.status,A2.comments,A2.edit,A2.coords,A2.shape,A2.pathology_lvl1,A2.anatomy_lvl1,A2.anatomy_lvl2,A2.severity from ids_marked_annotations A1 LEFT JOIN ids_verification A2 ON A2.stage_1_annotation_id = A1.annotation_id WHERE A1.series_uuid = %s order by A1.slice_id;", (series_uuid,))
    annotation_db_data = cursor.fetchall()
    image_resp = []
    i = 0
    for data in imagedata:
        imagedict = {}
        imagedict["dcmlink"] = data["dcmlink"].replace('http://', 'https://')
        imagedict["jpglink_brain_window"] = data["jpglink_brain_window"].replace('http://', 'https://')
        imagedict["slice_id"] = data["slice_id"]
        imagedict["slice_sequence_id"] = i
       

        # Add Instance Info to Imagedict
        instance_info_dict = {}
        stage_1_instance_resp = []
        stage_2_instance_resp = []
        count_verify_annotation_stage1 = 0
        count_total_annotation =0
        for idata in instance_info_db_data:
            if idata["stage"] == 1:
                idata_dict = {}
                if idata["slice_id"] == data["slice_id"]:
                    count_total_annotation+=1
                    idata_dict["instanceinfo_uid"] = idata["instanceinfo_id"]
                    idata_dict["pathology_lvl1"] = idata["pathology_lvl1"]
                    idata_dict["anatomy_lvl1"] = idata["anatomy_lvl1"]
                    idata_dict["anatomy_lvl2"] = idata["anatomy_lvl2"]
                    idata_dict["comments"] = idata["comments"]
                    idata_dict["severity"] = idata["severity"]
                    if idata["stage_2_instance_info_id"]:
                        count_verify_annotation_stage1+=1
                        stage_2_instance_dict = {}
                        stage_2_instance_dict["stage_2_instance_info_id"] = idata["stage_2_instance_info_id"]
                        stage_2_instance_dict["added_timestamp"] = str(idata["I2.added_timestamp"])
                        stage_2_instance_dict["status"] = idata["status"]
                        stage_2_instance_dict["comments"] = idata["I2.comments"]
                        stage_2_instance_dict["edit"] = idata["edit"]
                        stage_2_instance_dict["pathology_lvl1"] = idata["I2.pathology_lvl1"]
                        stage_2_instance_dict["anatomy_lvl1"] = idata["I2.anatomy_lvl1"]
                        stage_2_instance_dict["anatomy_lvl2"] = idata["I2.anatomy_lvl2"]
                        stage_2_instance_dict["severity"] = idata["I2.severity"]
                        idata_dict["stage_2"] = stage_2_instance_dict
                    stage_1_instance_resp.append(idata_dict)
                if stage_1_instance_resp:
                    instance_info_dict["stage_1"] = stage_1_instance_resp
            elif idata["stage"] == 2:
                idata_dict = {}
                if idata["slice_id"] == data["slice_id"]:
                    idata_dict["instanceinfo_uid"] = idata["instanceinfo_id"]
                    idata_dict["pathology_lvl1"] = idata["pathology_lvl1"]
                    idata_dict["anatomy_lvl1"] = idata["anatomy_lvl1"]
                    idata_dict["anatomy_lvl2"] = idata["anatomy_lvl2"]
                    idata_dict["comments"] = idata["comments"]
                    idata_dict["severity"] = idata["severity"]
                    stage_2_instance_resp.append(idata_dict)
                if stage_2_instance_resp:
                    instance_info_dict["stage_2"] = stage_2_instance_resp
        if instance_info_dict:
            imagedict["instance_info"] = instance_info_dict
        
        image_resp.append(imagedict)
        i += 1

        # Add Annotation Info to Imagedict
        annotation_dict = {}
        stage_1_annotation_resp = []
        stage_2_annotation_resp = []
        

        for adata in annotation_db_data:
            if adata["stage"] == 1:
                adata_dict = {}
                if adata["slice_id"] == data["slice_id"]:
                    count_total_annotation = count_total_annotation +1
                    adata_dict["annotation_id"] = adata["annotation_id"]
                    adata_dict["added_timestamp"] = str(adata["timestamp"])
                    adata_dict["coords"] = json.loads(adata["coords"])
                    adata_dict["shape"] = adata["shape"]
                    adata_dict["pathology_lvl1"] = adata["pathology_lvl1"]
                    adata_dict["anatomy_lvl1"] = adata["anatomy_lvl1"]
                    adata_dict["anatomy_lvl2"] = adata["anatomy_lvl2"]
                    adata_dict["severity"] = adata["severity"]
                    adata_dict["comments"] = adata["comments"]
                    if adata["verification_id"]:
                        stage_2_annotation_dict = {}
                        stage_2_annotation_dict["stage_2_annotation_id"] = adata["verification_id"]
                        stage_2_annotation_dict["added_timestamp"] = str(adata["added_timestamp"])
                        stage_2_annotation_dict["status"] = adata["status"]
                        stage_2_annotation_dict["comments"] = adata["A2.comments"]
                        stage_2_annotation_dict["edit"] = adata["edit"]
                        stage_2_annotation_dict["coords"] = json.loads(adata["A2.coords"])
                        stage_2_annotation_dict["shape"] = adata["A2.shape"]
                        stage_2_annotation_dict["pathology_lvl1"] = adata["A2.pathology_lvl1"]
                        stage_2_annotation_dict["anatomy_lvl1"] = adata["A2.anatomy_lvl1"]
                        stage_2_annotation_dict["anatomy_lvl2"] = adata["A2.anatomy_lvl2"]
                        stage_2_annotation_dict["severity"] = adata["A2.severity"]
                        count_verify_annotation_stage1 = count_verify_annotation_stage1+1
                        adata_dict["stage_2"] = stage_2_annotation_dict

                    stage_1_annotation_resp.append(adata_dict)
                if stage_1_annotation_resp:
                    annotation_dict["stage_1"] = stage_1_annotation_resp


            elif adata["stage"] == 2:
                adata_dict = {}
                if adata["slice_id"] == data["slice_id"]:
                    adata_dict["annotation_id"] = adata["annotation_id"]
                    adata_dict["added_timestamp"] = str(adata["timestamp"])
                    adata_dict["coords"] = json.loads(adata["coords"])
                    adata_dict["shape"] = adata["shape"]
                    adata_dict["pathology_lvl1"] = adata["pathology_lvl1"]
                    adata_dict["anatomy_lvl1"] = adata["anatomy_lvl1"]
                    adata_dict["anatomy_lvl2"] = adata["anatomy_lvl2"]
                    adata_dict["severity"] = adata["severity"]
                    adata_dict["comments"] = adata["comments"]
                    stage_2_annotation_resp.append(adata_dict)
                if stage_2_annotation_resp:
                    annotation_dict["stage_2"] = stage_2_annotation_resp
        if annotation_dict:
            imagedict["annotations"] = annotation_dict
        imagedict["Total_annotations"] = count_total_annotation
        imagedict["Total_verified_annotation"] = count_verify_annotation_stage1
             
    resp["data"]["series"]["Total_instance"] = len(image_resp)        
    resp["data"]["series"]["instances"] = image_resp

    tree_dict = make_medical_tree()
    resp["meta"] = {}
    resp["meta"]["medical_tree"] = tree_dict
    return resp

def serve_stage_3_respone(userid, series_uuid, serve_id):
    """
    Function for giving stage 3 response to frontend in a fixed pattern.
    :param series_uuid: Series_uuid for the series
    :return : response for frontend
    """
    diagdb = db_connection()
    cursor = diagdb.cursor()
    resp = {}
    resp["status"] = 1
    resp["stage"] = 3
    resp["timestamp"] = str(datetime.datetime.now())
    resp["data"] = {}
    resp["data"]["series"] = {}
    count_total_annotation = 0
    count_verify_annotation = 0

    # Get Series Data
    cursor.execute("SELECT * FROM ids_raw_series WHERE series_uuid = %s", (series_uuid,))
    series_db_data = cursor.fetchone()
    resp["data"]["series"]["series_uuid"] = series_db_data.get("series_uuid")
    resp["data"]["series"]["date_of_scan"] = str(series_db_data.get("date_of_scan"))
    resp["data"]["series"]["study_description"] = series_db_data.get("study_description", "")
    resp["data"]["series"]["series_description"] = series_db_data.get("series_description", "")
    resp["data"]["series"]["patient_id"] = series_db_data.get("patient_id")
    resp["data"]["series"]["patient_age"] = series_db_data.get("patient_age", "")
    resp["data"]["series"]["patient_sex"] = series_db_data.get("patient_sex", "")
    resp["data"]["series"]["series_info"] = {}

    # Get Series Info
    cursor.execute("SELECT S1.*,S2.*,S3.* from ids_marked_series_info S1 LEFT JOIN ids_stage_2_verify_series_info S2 ON S2.stage_1_series_info_id = S1.series_info_id left join ids_stage_3_verify_series_info S3 on S3.stage_1_series_info_id = S1.series_info_id  WHERE S1.series_uuid = %s and (S2.stage_2_series_info_id in (SELECT MIN(stage_2_series_info_id) from `ids_stage_2_verify_series_info` group by stage_1_series_info_id) or S2.stage_2_series_info_id is Null ) order by S1.series_info_id;", (series_uuid,))
    series_info_db_data = cursor.fetchall()
    stage_1_series_resp = []
    stage_2_series_resp = []
    stage_3_series_resp = []
    for data in series_info_db_data:
        if data["stage"] == 1:
            count_total_annotation = count_total_annotation + 1
            series_dict = {}
            series_dict["series_info_id"] = data["series_info_id"]
            series_dict["added_timestamp"] = str(data["added_timestamp"])
            series_dict["label"] = data["label"]
            series_dict["value"] = data["value"]
            series_dict["comments"] = data["comments"]
            if data["stage_2_series_info_id"]:
                stage_2_series_dict = {}
                stage_2_series_dict["stage_2_series_info_id"] = data["stage_2_series_info_id"]
                stage_2_series_dict["added_timestamp"] = str(data["added_timestamp"])
                stage_2_series_dict["status"] = data["status"]
                stage_2_series_dict["comments"] = data["comments"]
                stage_2_series_dict["edit"] = data["edit"]
                stage_2_series_dict["label"] = data["label"]
                stage_2_series_dict["value"] = data["value"]
                series_dict["stage_2"] = stage_2_series_dict
                if data["stage_3_series_info_id"]:
                    stage_3_series_dict = {}
                    count_verify_annotation = count_verify_annotation + 1
                    stage_3_series_dict["stage_3_series_info_id"] = data["stage_3_series_info_id"]
                    stage_3_series_dict["added_timestamp"] = str(data["S3.added_timestamp"])
                    stage_3_series_dict["status"] = data["S3.status"]
                    stage_3_series_dict["comments"] = data["S3.comments"]
                    stage_3_series_dict["edit"] = data["S3.edit"]
                    stage_3_series_dict["label"] = data["S3.label"]
                    stage_3_series_dict["value"] = data["S3.value"]
                    stage_3_series_dict["verification_stage"] = data["verification_stage"]
                    series_dict["stage_2"]["stage_3"] = stage_3_series_dict
            else :
                if data["stage_3_series_info_id"]:
                    series_dict['stage_2'] = {} 
                    count_verify_annotation = count_verify_annotation + 1
                    stage_3_series_dict = {}
                    stage_3_series_dict["stage_3_series_info_id"] = data["S3.stage_3_series_info_id"]
                    stage_3_series_dict["added_timestamp"] = str(data["S3.added_timestamp"])
                    stage_3_series_dict["status"] = data["S3.status"]
                    stage_3_series_dict["comments"] = data["S3.comments"]
                    stage_3_series_dict["edit"] = data["S3.edit"]
                    stage_3_series_dict["label"] = data["S3.label"]
                    stage_3_series_dict["value"] = data["S3.value"]
                    stage_3_series_dict["verification_stage"] = data["verification_stage"]
                    series_dict["stage_2"]["stage_3"] = stage_3_series_dict
            stage_1_series_resp.append(series_dict)
        elif data["stage"] == 2 :
            count_total_annotation = count_total_annotation + 1
            series_dict = {}
            series_dict["series_info_id"] = data["series_info_id"]
            series_dict["added_timestamp"] = str(data["added_timestamp"])
            series_dict["label"] = data["label"]
            series_dict["value"] = data["value"]
            series_dict["comments"] = data["comments"]
            if data["stage_3_series_info_id"]:
                count_verify_annotation = count_verify_annotation + 1
                stage_3_series_dict = {}
                stage_3_series_dict["stage_3_series_info_id"] = data["stage_3_series_info_id"]
                stage_3_series_dict["added_timestamp"] = str(data["S3.added_timestamp"])
                stage_3_series_dict["status"] = data["S3.status"]
                stage_3_series_dict["comments"] = data["S3.comments"]
                stage_3_series_dict["edit"] = data["S3.edit"]
                stage_3_series_dict["label"] = data["S3.label"]
                stage_3_series_dict["value"] = data["S3.value"]
                stage_3_series_dict["verification_stage"] = data["verification_stage"]
                series_dict["stage_3"] = stage_3_series_dict
            stage_2_series_resp.append(series_dict)


        elif data["stage"] == 3 :
            series_dict = {}
            series_dict["series_info_id"] = data["series_info_id"]
            series_dict["added_timestamp"] = str(data["added_timestamp"])
            series_dict["label"] = data["label"]
            series_dict["value"] = data["value"]
            series_dict["comments"] = data["comments"]
            stage_3_series_resp.append(series_dict)

    # Add resp

    resp["data"]["series"]["series_info"]["stage_1"] = stage_1_series_resp
    resp["data"]["series"]["series_info"]["stage_2"] = stage_2_series_resp
    resp["data"]["series"]["series_info"]["stage_3"] = stage_3_series_resp

     # Fetch Image Data from DB
    cursor.execute("SELECT slice_id,dcmlink,jpglink_brain_window FROM ids_raw_images WHERE series_uuid = %s", (series_uuid,))
    imagedata = cursor.fetchall()
    # Fetch Instance Info from DB
    cursor.execute("SELECT I1.*,I2.*,I3.* from ids_marked_instance_info I1 LEFT JOIN ids_stage_2_verify_instance_info I2 ON I2.stage_1_instance_info_id = I1.instanceinfo_id left join ids_stage_3_verify_instance_info I3 on I1.instanceinfo_id = I3.stage_1_instance_info_id WHERE I1.series_uuid = %s and (I2.stage_2_instance_info_id in (SELECT MIN(stage_2_instance_info_id) from `ids_stage_2_verify_instance_info` group by stage_1_instance_info_id) or I2.stage_2_instance_info_id is Null ) order by I1.slice_id;", (series_uuid,))
    instance_info_db_data = cursor.fetchall()
    # Fetch Annotation data from DB
    cursor.execute("SELECT A1.*,A2.*,A3.* from ids_marked_annotations A1 LEFT JOIN ids_verification A2 ON A2.stage_1_annotation_id = A1.annotation_id left join ids_stage_3_verify_annotations A3 on A3.stage_1_annotation_id  = A1.annotation_id WHERE A1.series_uuid = %s order by A1.slice_id;", (series_uuid,))
    annotation_db_data = cursor.fetchall()
    image_resp = []
    i = 0
    for data in imagedata:
        imagedict = {}
        imagedict["dcmlink"] = data["dcmlink"].replace('http://', 'https://')
        imagedict["jpglink_brain_window"] = data["jpglink_brain_window"].replace('http://', 'https://')
        imagedict["slice_id"] = data["slice_id"]
        imagedict["slice_sequence_id"] = i
        # Add Instance Info to Imagedict
        instance_info_dict = {}
        stage_1_instance_resp = []
        stage_2_instance_resp = []
        stage_3_instance_resp = []
        for idata in instance_info_db_data:
            if idata["stage"] == 1:
                idata_dict = {}
                if idata["slice_id"] == data["slice_id"]:
                    count_total_annotation = count_total_annotation + 1
                    idata_dict["instanceinfo_uid"] = idata["instanceinfo_id"]
                    idata_dict["pathology_lvl1"] = idata["pathology_lvl1"]
                    idata_dict["anatomy_lvl1"] = idata["anatomy_lvl1"]
                    idata_dict["anatomy_lvl2"] = idata["anatomy_lvl2"]
                    idata_dict["comments"] = idata["comments"]
                    idata_dict["severity"] = idata["severity"]
                    if idata["stage_2_instance_info_id"]:
                        stage_2_instance_dict = {}
                        stage_2_instance_dict["stage_2_instance_info_id"] = idata["stage_2_instance_info_id"]
                        stage_2_instance_dict["added_timestamp"] = str(idata["added_timestamp"])
                        stage_2_instance_dict["status"] = idata["status"]
                        stage_2_instance_dict["comments"] = idata["comments"]
                        stage_2_instance_dict["edit"] = idata["edit"]
                        stage_2_instance_dict["pathology_lvl1"] = idata["pathology_lvl1"]
                        stage_2_instance_dict["anatomy_lvl1"] = idata["anatomy_lvl1"]
                        stage_2_instance_dict["anatomy_lvl2"] = idata["anatomy_lvl2"]
                        stage_2_instance_dict["severity"] = idata["severity"]
                        idata_dict["stage_2"] = stage_2_instance_dict
                        if idata["stage_3_instance_info_id"]:
                            count_verify_annotation = count_verify_annotation + 1
                            stage_3_instance_dict = {}
                            stage_3_instance_dict["stage_3_instance_info_id"] = idata["stage_3_instance_info_id"]
                            stage_3_instance_dict["added_timestamp"] = str(idata["I3.added_timestamp"])
                            stage_3_instance_dict["status"] = idata["I3.status"]
                            stage_3_instance_dict["comments"] = idata["I3.comments"]
                            stage_3_instance_dict["edit"] = idata["I3.edit"]
                            stage_3_instance_dict["pathology_lvl1"] = idata["I3.pathology_lvl1"]
                            stage_3_instance_dict["anatomy_lvl1"] = idata["I3.anatomy_lvl1"]
                            stage_3_instance_dict["anatomy_lvl2"] = idata["I3.anatomy_lvl2"]
                            stage_3_instance_dict["severity"] = idata["I3.severity"]
                            stage_3_instance_dict["verification_stage"] = idata["verification_stage"]
                            idata_dict["stage_2"]["stage_3"] = stage_3_instance_dict
                    else:
                        if idata["stage_3_instance_info_id"]:
                            idata_dict["stage_2"] = {}
                            count_verify_annotation = count_verify_annotation + 1
                            stage_3_instance_dict = {}
                            stage_3_instance_dict["stage_3_instance_info_id"] = idata["stage_3_instance_info_id"]
                            stage_3_instance_dict["added_timestamp"] = str(idata["I3.added_timestamp"])
                            stage_3_instance_dict["status"] = idata["I3.status"]
                            stage_3_instance_dict["comments"] = idata["I3.comments"]
                            stage_3_instance_dict["edit"] = idata["I3.edit"]
                            stage_3_instance_dict["pathology_lvl1"] = idata["I3.pathology_lvl1"]
                            stage_3_instance_dict["anatomy_lvl1"] = idata["I3.anatomy_lvl1"]
                            stage_3_instance_dict["anatomy_lvl2"] = idata["I3.anatomy_lvl2"]
                            stage_3_instance_dict["severity"] = idata["I3.severity"]
                            stage_3_instance_dict["verification_stage"] = idata["verification_stage"]
                            idata_dict["stage_2"]["stage_3"] = stage_3_instance_dict
                            

                    stage_1_instance_resp.append(idata_dict)
                if stage_1_instance_resp:
                    instance_info_dict["stage_1"] = stage_1_instance_resp
            
            elif idata["stage"] ==2 :
                idata_dict = {}
                if idata["slice_id"] == data["slice_id"]:
                    count_total_annotation = count_total_annotation + 1
                    idata_dict["instanceinfo_uid"] = idata["instanceinfo_id"]
                    idata_dict["pathology_lvl1"] = idata["pathology_lvl1"]
                    idata_dict["anatomy_lvl1"] = idata["anatomy_lvl1"]
                    idata_dict["anatomy_lvl2"] = idata["anatomy_lvl2"]
                    idata_dict["comments"] = idata["comments"]
                    idata_dict["severity"] = idata["severity"]
                    if idata["stage_3_instance_info_id"]:
                        count_verify_annotation = count_verify_annotation + 1
                        stage_3_instance_dict = {}
                        stage_3_instance_dict["stage_3_instance_info_id"] = idata["stage_3_instance_info_id"]
                        stage_3_instance_dict["added_timestamp"] = str(idata["I3.added_timestamp"])
                        stage_3_instance_dict["status"] = idata["I3.status"]
                        stage_3_instance_dict["comments"] = idata["I3.comments"]
                        stage_3_instance_dict["edit"] = idata["I3.edit"]
                        stage_3_instance_dict["pathology_lvl1"] = idata["I3.pathology_lvl1"]
                        stage_3_instance_dict["anatomy_lvl1"] = idata["I3.anatomy_lvl1"]
                        stage_3_instance_dict["anatomy_lvl2"] = idata["I3.anatomy_lvl2"]
                        stage_3_instance_dict["severity"] = idata["I3.severity"]
                        stage_3_instance_dict["verification_stage"] = idata["verification_stage"]
                        idata_dict["stage_3"] = stage_3_instance_dict
                    stage_2_instance_resp.append(idata_dict)
                    if stage_2_instance_resp:
                        instance_info_dict["stage_2"] = stage_2_instance_resp



            elif idata["stage"] == 3:
                idata_dict = {}
                if idata["slice_id"] == data["slice_id"]:
                    idata_dict["instanceinfo_uid"] = idata["instanceinfo_id"]
                    idata_dict["pathology_lvl1"] = idata["pathology_lvl1"]
                    idata_dict["anatomy_lvl1"] = idata["anatomy_lvl1"]
                    idata_dict["anatomy_lvl2"] = idata["anatomy_lvl2"]
                    idata_dict["comments"] = idata["comments"]
                    idata_dict["severity"] = idata["severity"]
                    stage_3_instance_resp.append(idata_dict)
                if stage_3_instance_resp:
                    instance_info_dict["stage_3"] = stage_3_instance_resp
        if instance_info_dict:
            imagedict["instance_info"] = instance_info_dict
       
        
        i += 1
        annotation_dict = {}
        stage_1_annotation_resp = []
        stage_2_annotation_resp = []
        stage_3_annotation_resp = []
        
        for adata in annotation_db_data:
            if adata["stage"] == 1:
                adata_dict = {}
                if adata["slice_id"] == data["slice_id"]:
                    count_total_annotation = count_total_annotation +1
                    adata_dict["annotation_id"] = adata["annotation_id"]
                    adata_dict["added_timestamp"] = str(adata["timestamp"])
                    adata_dict["coords"] = json.loads(adata["coords"])
                    adata_dict["shape"] = adata["shape"]
                    adata_dict["pathology_lvl1"] = adata["pathology_lvl1"]
                    adata_dict["anatomy_lvl1"] = adata["anatomy_lvl1"]
                    adata_dict["anatomy_lvl2"] = adata["anatomy_lvl2"]
                    adata_dict["severity"] = adata["severity"]
                    adata_dict["comments"] = adata["comments"]
                    if adata["verification_id"]:
                        stage_2_annotation_dict = {}
                        stage_2_annotation_dict["stage_2_annotation_id"] = adata["verification_id"]
                        stage_2_annotation_dict["added_timestamp"] = str(adata["added_timestamp"])
                        stage_2_annotation_dict["status"] = adata["status"]
                        stage_2_annotation_dict["comments"] = adata["comments"]
                        stage_2_annotation_dict["edit"] = adata["edit"]
                        stage_2_annotation_dict["coords"] = json.loads(adata["coords"])
                        stage_2_annotation_dict["shape"] = adata["shape"]
                        stage_2_annotation_dict["pathology_lvl1"] = adata["pathology_lvl1"]
                        stage_2_annotation_dict["anatomy_lvl1"] = adata["anatomy_lvl1"]
                        stage_2_annotation_dict["anatomy_lvl2"] = adata["anatomy_lvl2"]
                        stage_2_annotation_dict["severity"] = adata["severity"]
                        adata_dict["stage_2"] = stage_2_annotation_dict
                        if adata["A3.verification_id"]:
                            count_verify_annotation = count_verify_annotation + 1
                            stage_3_annotation_dict = {}
                            stage_3_annotation_dict["stage_3_annotation_id"] = adata["A3.verification_id"]
                            stage_3_annotation_dict["added_timestamp"] = str(adata["A3.added_timestamp"])
                            stage_3_annotation_dict["status"] = adata["A3.status"]
                            stage_3_annotation_dict["comments"] = adata["A3.comments"]
                            stage_3_annotation_dict["edit"] = adata["A3.edit"]
                            stage_3_annotation_dict["coords"] = json.loads(adata["A3.coords"])
                            stage_3_annotation_dict["shape"] = adata["A3.shape"]
                            stage_3_annotation_dict["pathology_lvl1"] = adata["A3.pathology_lvl1"]
                            stage_3_annotation_dict["anatomy_lvl1"] = adata["A3.anatomy_lvl1"]
                            stage_3_annotation_dict["anatomy_lvl2"] = adata["A3.anatomy_lvl2"]
                            stage_3_annotation_dict["verification_stage"] = adata["verification_stage"]
                            #stage_3_annotation_dict["anatomy_lvl2"] = "justchecking"
                            stage_3_annotation_dict["severity"] = adata["A3.severity"]
                            adata_dict["stage_2"]["stage_3"] = stage_3_annotation_dict
                    else:
                        if adata["A3.verification_id"]:
                            adata_dict["stage_2"] = {}
                            count_verify_annotation = count_verify_annotation + 1
                            stage_3_annotation_dict = {}
                            stage_3_annotation_dict["stage_3_annotation_id"] = adata["A3.verification_id"]
                            stage_3_annotation_dict["added_timestamp"] = str(adata["A3.added_timestamp"])
                            stage_3_annotation_dict["status"] = adata["A3.status"]
                            stage_3_annotation_dict["comments"] = adata["A3.comments"]
                            stage_3_annotation_dict["edit"] = adata["A3.edit"]
                            stage_3_annotation_dict["coords"] = json.loads(adata["A3.coords"])
                            stage_3_annotation_dict["shape"] = adata["A3.shape"]
                            stage_3_annotation_dict["pathology_lvl1"] = adata["A3.pathology_lvl1"]
                            stage_3_annotation_dict["anatomy_lvl1"] = adata["A3.anatomy_lvl1"]
                            stage_3_annotation_dict["anatomy_lvl2"] = adata["A3.anatomy_lvl2"]
                            #stage_3_annotation_dict["anatomy_lvl2"] = "justchecking"
                            stage_3_annotation_dict["severity"] = adata["A3.severity"]
                            stage_3_annotation_dict["verification_stage"] = adata["verification_stage"]
                            adata_dict["stage_2"]["stage_3"] = stage_3_annotation_dict

                    stage_1_annotation_resp.append(adata_dict)

                if stage_1_annotation_resp:
                    annotation_dict["stage_1"] = stage_1_annotation_resp

            elif adata["stage"] == 2:
                adata_dict = {}
                if adata["slice_id"] == data["slice_id"]:
                    count_total_annotation = count_total_annotation + 1
                    adata_dict["annotation_id"] = adata["annotation_id"]
                    adata_dict["added_timestamp"] = str(adata["timestamp"])
                    adata_dict["coords"] = json.loads(adata["coords"])
                    adata_dict["shape"] = adata["shape"]
                    adata_dict["pathology_lvl1"] = adata["pathology_lvl1"]
                    adata_dict["anatomy_lvl1"] = adata["anatomy_lvl1"]
                    adata_dict["anatomy_lvl2"] = adata["anatomy_lvl2"]
                    adata_dict["severity"] = adata["severity"]
                    adata_dict["comments"] = adata["comments"]
                    if adata["A3.verification_id"]:
                        count_verify_annotation = count_verify_annotation + 1
                        stage_3_annotation_dict = {}
                        stage_3_annotation_dict["stage_3_annotation_id"] = adata["A3.verification_id"]
                        stage_3_annotation_dict["added_timestamp"] = str(adata["A3.added_timestamp"])
                        stage_3_annotation_dict["status"] = adata["A3.status"]
                        stage_3_annotation_dict["comments"] = adata["A3.comments"]
                        stage_3_annotation_dict["edit"] = adata["A3.edit"]
                        stage_3_annotation_dict["coords"] = json.loads(adata["A3.coords"])
                        stage_3_annotation_dict["shape"] = adata["A3.shape"]
                        stage_3_annotation_dict["pathology_lvl1"] = adata["A3.pathology_lvl1"]
                        stage_3_annotation_dict["anatomy_lvl1"] = adata["A3.anatomy_lvl1"]
                        stage_3_annotation_dict["anatomy_lvl2"] = adata["A3.anatomy_lvl2"]
                        #stage_3_annotation_dict["anatomy_lvl2"] = "justchecking"
                        stage_3_annotation_dict["severity"] = adata["A3.severity"]
                        stage_3_annotation_dict["verification_stage"] = adata["verification_stage"]
                        adata_dict["stage_3"] = stage_3_annotation_dict

                    stage_2_annotation_resp.append(adata_dict)
                    if stage_2_annotation_resp:
                        annotation_dict["stage_2"] = stage_2_annotation_resp
       
            elif adata["stage"]==3:
                adata_dict = {}
                if adata["slice_id"] == data["slice_id"]:
                    adata_dict["annotation_id"] = adata["annotation_id"]
                    adata_dict["added_timestamp"] = str(adata["timestamp"])
                    adata_dict["coords"] = json.loads(adata["coords"])
                    adata_dict["shape"] = adata["shape"]
                    adata_dict["pathology_lvl1"] = adata["pathology_lvl1"]
                    adata_dict["anatomy_lvl1"] = adata["anatomy_lvl1"]
                    adata_dict["anatomy_lvl2"] = adata["anatomy_lvl2"]
                    adata_dict["severity"] = adata["severity"]
                    adata_dict["comments"] = adata["comments"]
                    stage_3_annotation_resp.append(adata_dict)
                if stage_3_annotation_resp:
                    annotation_dict["stage_3"] = stage_3_annotation_resp
        if annotation_dict:
            imagedict["annotations"] = annotation_dict
        imagedict["Total_annotations"] = count_total_annotation
        imagedict["Total_verified_annotation"] = count_verify_annotation
        count_total_annotation = 0
        count_verify_annotation = 0
        image_resp.append(imagedict)
             
    #resp["data"]["series"]["Total_instance"] = len(image_resp)        
    resp["data"]["series"]["instances"] = image_resp
  
    tree_dict = make_medical_tree()
    resp["meta"] = {}
    resp["meta"]["medical_tree"] = tree_dict
    return resp


def ImageDataServe(userid, privilege_level, params):
    print "USERID:", userid
    if userid is None:
        return {"status": 0, "message": "Unknown userid. Cannot render Data.", "timestamp": str(datetime.datetime.now())}
    else:
        if params == "current":
            series_uuid, stage, serve_id = getCurrentData(
                userid, privilege_level)

        elif params == "next":
            series_uuid, stage, serve_id = getCurrentData(
                userid, privilege_level)
            updateskipnext(serve_id=serve_id, stage=stage, skip=False, next=True)
            series_uuid, stage, serve_id = assign_new_series(
                userid, privilege_level)

        elif params == "skip":
            series_uuid, stage, serve_id = getCurrentData(
                userid, privilege_level)
            updateskipnext(serve_id=serve_id, stage=stage, skip=True, next=False)
            series_uuid, stage, serve_id = assign_new_series(
                userid, privilege_level)

        print series_uuid, stage, serve_id
        if stage == "STAGE3":
            data = serve_stage_3_respone(userid, series_uuid, serve_id)
        elif stage == "STAGE2":
            data = serve_stage_2_response(userid, series_uuid, serve_id)
        elif stage == "STAGE1":
            data = serve_stage_1_response(series_uuid)
    return data


def check():
    """
    Demo function for checking sql queries
    """
    diagdb = db_connection()
    cursor = diagdb.cursor()
    cursor.execute("update tyrion.ids_stage_3_verify_annotations set series_uuid = '36c7c361-33b5-4337-8a00-944b34af3633', stage_1_annotation_id = 69806,slice_id = 267339 where verification_id = 4")

def view_response(series_uuid):
    """
    Function for viewing data for a particular series
    :param series_uuid : series_uuid for a series
    :return: response for frontend
    :
    """
    diagdb = db_connection()
    cursor = diagdb.cursor()
    resp = {}
    resp["status"] = 1
    resp["timestamp"] = str(datetime.datetime.now())
    resp["data"] = {}
    resp["data"]["series"] = {}

    cursor.execute("Select * from users")
    user_name = cursor.fetchall()
    df = pd.DataFrame()
    count = 0
    for data in user_name:
        df.loc[count,"user_id"]=data["user_id"]
        df.loc[count,"first_name"]=data["first_name"]
        df.loc[count,"salutation"]=data["salutation"]
        df.loc[count,"last_name"]=data["last_name"]
        count+=1
    # Get Series Data
    cursor.execute("SELECT * FROM ids_raw_series WHERE series_uuid = %s", (series_uuid,))
    series_db_data = cursor.fetchone()
    resp["data"]["series"]["series_uuid"] = series_db_data.get("series_uuid")
    resp["data"]["series"]["date_of_scan"] = str(series_db_data.get("date_of_scan"))
    resp["data"]["series"]["study_description"] = series_db_data.get("study_description", "")
    resp["data"]["series"]["series_description"] = series_db_data.get("series_description", "")
    resp["data"]["series"]["patient_id"] = series_db_data.get("patient_id")
    resp["data"]["series"]["patient_age"] = series_db_data.get("patient_age", "")
    resp["data"]["series"]["patient_sex"] = series_db_data.get("patient_sex", "")
    resp["data"]["series"]["series_info"] = {}

    # Get Series Info
    cursor.execute("SELECT S1.*,S2.*,S3.* from ids_marked_series_info S1 LEFT JOIN ids_stage_2_verify_series_info S2 ON S2.stage_1_series_info_id = S1.series_info_id left join ids_stage_3_verify_series_info S3 on S3.stage_1_series_info_id = S1.series_info_id  WHERE S1.series_uuid = %s order by S1.series_info_id;", (series_uuid,))
    series_info_db_data = cursor.fetchall()
    stage_1_series_resp = []
    stage_2_series_resp = []
    stage_3_series_resp = []
    for data in series_info_db_data:
            series_dict = {}
            series_dict["series_info_id"] = data["series_info_id"]
            series_dict["added_timestamp"] = str(data["added_timestamp"])
            series_dict["label"] = data["label"]
            series_dict["value"] = data["value"]
            series_dict["comments"] = data["comments"]
            series_dict["Name_of_Tagger"] = df[df["user_id"]==data["user_id"]]["salutation"].unique()[0]+" "+df[df["user_id"]==data["user_id"]]["first_name"].unique()[0]+" "+df[df["user_id"]==data["user_id"]]["last_name"].unique()[0]
            if data["stage_2_series_info_id"]:
                stage_2_series_dict = {}
                stage_2_series_dict["stage_2_series_info_id"] = data["stage_2_series_info_id"]
                stage_2_series_dict["added_timestamp"] = str(data["added_timestamp"])
                stage_2_series_dict["status"] = data["status"]
                stage_2_series_dict["comments"] = data["comments"]
                stage_2_series_dict["edit"] = data["edit"]
                stage_2_series_dict["label"] = data["label"]
                stage_2_series_dict["value"] = data["value"]
                stage_2_series_dict["Name_of_verifier"] = df[df["user_id"]==data["verifier_user_id"]]["salutation"].unique()[0]+" "+df[df["user_id"]==data["verifier_user_id"]]["first_name"].unique()[0]+" "+df[df["user_id"]==data["verifier_user_id"]]["last_name"].unique()[0]
                series_dict["stage_2"] = stage_2_series_dict
                if data["stage_3_series_info_id"]:
                    stage_3_series_dict = {}
                    stage_3_series_dict["stage_3_series_info_id"] = data["S3.stage_3_series_info_id"]
                    stage_3_series_dict["added_timestamp"] = str(data["S3.added_timestamp"])
                    stage_3_series_dict["status"] = data["S3.status"]
                    stage_3_series_dict["comments"] = data["S3.comments"]
                    stage_3_series_dict["edit"] = data["S3.edit"]
                    stage_3_series_dict["label"] = data["S3.label"]
                    stage_3_series_dict["value"] = data["S3.value"]
                    stage_3_series_dict["Name_of_verifier"] = df[df["user_id"]==data["S3.verifier_user_id"]]["salutation"].unique()[0]+" "+df[df["user_id"]==data["S3.verifier_user_id"]]["first_name"].unique()[0]+" "+df[df["user_id"]==data["S3.verifier_user_id"]]["last_name"].unique()[0]
                    series_dict["stage_2"]["stage_3"] = stage_3_series_dict

            stage_1_series_resp.append(series_dict)

   

    # Add resp
    resp["data"]["series"]["series_info"]["stage_1"] = stage_1_series_resp

     # Fetch Image Data from DB
    cursor.execute("SELECT slice_id,dcmlink,jpglink_brain_window FROM ids_raw_images WHERE series_uuid = %s", (series_uuid,))
    imagedata = cursor.fetchall()
    # Fetch Instance Info from DB
    cursor.execute("SELECT I1.*,I2.*,I3.* from ids_marked_instance_info I1 LEFT JOIN ids_stage_2_verify_instance_info I2 ON I2.stage_1_instance_info_id = I1.instanceinfo_id left join ids_stage_3_verify_instance_info I3 on I1.instanceinfo_id = I3.stage_1_instance_info_id WHERE I1.series_uuid = %s order by I1.slice_id;", (series_uuid,))
    instance_info_db_data = cursor.fetchall()
    # Fetch Annotation data from DB
    cursor.execute("SELECT A1.*,A2.*,A3.* from ids_marked_annotations A1 LEFT JOIN ids_verification A2 ON A2.stage_1_annotation_id = A1.annotation_id left join ids_stage_3_verify_annotations A3 on A3.stage_1_annotation_id  = A1.annotation_id WHERE A1.series_uuid = %s order by A1.slice_id;", (series_uuid,))
    annotation_db_data = cursor.fetchall()
    image_resp = []
    i = 0
    for data in imagedata:
        imagedict = {}
        imagedict["dcmlink"] = data["dcmlink"].replace('http://', 'https://')
        imagedict["jpglink_brain_window"] = data["jpglink_brain_window"].replace('http://', 'https://')
        imagedict["slice_id"] = data["slice_id"]
        imagedict["slice_sequence_id"] = i
        # Add Instance Info to Imagedict
        instance_info_dict = {}
        stage_1_instance_resp = []
        stage_2_instance_resp = []
        stage_3_instance_resp = []
        for idata in instance_info_db_data:
                idata_dict = {}
                if idata["slice_id"] == data["slice_id"]:
                    idata_dict["instanceinfo_uid"] = idata["instanceinfo_id"]
                    idata_dict["pathology_lvl1"] = idata["pathology_lvl1"]
                    idata_dict["anatomy_lvl1"] = idata["anatomy_lvl1"]
                    idata_dict["anatomy_lvl2"] = idata["anatomy_lvl2"]
                    idata_dict["comments"] = idata["comments"]
                    idata_dict["severity"] = idata["severity"]
                    idata_dict["Name_of_Tagger"] = df[df["user_id"]==idata["user_id"]]["salutation"].unique()[0]+" "+df[df["user_id"]==idata["user_id"]]["first_name"].unique()[0]+" "+df[df["user_id"]==idata["user_id"]]["last_name"].unique()[0]
                    if idata["stage_2_instance_info_id"]:
                        stage_2_instance_dict = {}
                        stage_2_instance_dict["stage_2_instance_info_id"] = idata["stage_2_instance_info_id"]
                        stage_2_instance_dict["added_timestamp"] = str(idata["added_timestamp"])
                        stage_2_instance_dict["status"] = idata["status"]
                        stage_2_instance_dict["comments"] = idata["comments"]
                        stage_2_instance_dict["edit"] = idata["edit"]
                        stage_2_instance_dict["pathology_lvl1"] = idata["pathology_lvl1"]
                        stage_2_instance_dict["anatomy_lvl1"] = idata["anatomy_lvl1"]
                        stage_2_instance_dict["anatomy_lvl2"] = idata["anatomy_lvl2"]
                        stage_2_instance_dict["severity"] = idata["severity"]
                        stage_2_instance_dict["Name_of_verifier"] = df[df["user_id"]==idata["verifier_user_id"]]["salutation"].unique()[0]+" "+df[df["user_id"]==idata["verifier_user_id"]]["first_name"].unique()[0]+" "+df[df["user_id"]==idata["verifier_user_id"]]["last_name"].unique()[0]
                        idata_dict["stage_2"] = stage_2_instance_dict
                        if idata["stage_3_instance_info_id"]:
                            stage_3_instance_dict = {}
                            stage_3_instance_dict["stage_3_instance_info_id"] = idata["stage_3_instance_info_id"]
                            stage_3_instance_dict["added_timestamp"] = str(idata["I3.added_timestamp"])
                            stage_3_instance_dict["status"] = idata["status"]
                            stage_3_instance_dict["comments"] = idata["I3.comments"]
                            stage_3_instance_dict["edit"] = idata["edit"]
                            stage_3_instance_dict["pathology_lvl1"] = idata["I3.pathology_lvl1"]
                            stage_3_instance_dict["anatomy_lvl1"] = idata["I3.anatomy_lvl1"]
                            stage_3_instance_dict["anatomy_lvl2"] = idata["I3.anatomy_lvl2"]
                            stage_3_instance_dict["severity"] = idata["I3.severity"]
                            stage_3_instance_dict["Name_of_verifier"] = df[df["user_id"]==idata["I3.verifer_user_id"]]["salutation"].unique()[0]+" "+df[df["user_id"]==idata["I3.verifier_user_id"]]["first_name"].unique()[0]+" "+df[df["user_id"]==idata["I3.verifier_user_id"]]["last_name"].unique()[0]
                            idata_dict["stage_2"]["stage_3"] = stage_3_instance_dict

                    stage_1_instance_resp.append(idata_dict)
                if stage_1_instance_resp:
                    instance_info_dict["stage_1"] = stage_1_instance_resp
            
            
        if instance_info_dict:
            imagedict["instance_info"] = instance_info_dict

        
        i += 1
        annotation_dict = {}
        stage_1_annotation_resp = []
        stage_2_annotation_resp = []
        stage_3_annotation_resp = []
        
        count_verify_annotation_stage1 = 0
        count_total_annotation =0
        for adata in annotation_db_data:
                adata_dict = {}
                if adata["slice_id"] == data["slice_id"]:
                    count_total_annotation = count_total_annotation +1
                    adata_dict["annotation_id"] = adata["annotation_id"]
                    adata_dict["added_timestamp"] = str(adata["timestamp"])
                    adata_dict["coords"] = json.loads(adata["coords"])
                    adata_dict["shape"] = adata["shape"]
                    adata_dict["pathology_lvl1"] = adata["pathology_lvl1"]
                    adata_dict["anatomy_lvl1"] = adata["anatomy_lvl1"]
                    adata_dict["anatomy_lvl2"] = adata["anatomy_lvl2"]
                    adata_dict["severity"] = adata["severity"]
                    adata_dict["comments"] = adata["comments"]
                    adata_dict["Name_of_Tagger"] = df[df["user_id"]==adata["user_id"]]["salutation"].unique()[0]+" "+df[df["user_id"]==adata["user_id"]]["first_name"].unique()[0]+" "+df[df["user_id"]==adata["user_id"]]["last_name"].unique()[0]

                    if adata["verification_id"]:
                        stage_2_annotation_dict = {}
                        stage_2_annotation_dict["stage_2_annotation_id"] = adata["verification_id"]
                        stage_2_annotation_dict["added_timestamp"] = str(adata["added_timestamp"])
                        stage_2_annotation_dict["status"] = adata["status"]
                        stage_2_annotation_dict["comments"] = adata["comments"]
                        stage_2_annotation_dict["edit"] = adata["edit"]
                        stage_2_annotation_dict["coords"] = json.loads(adata["coords"])
                        stage_2_annotation_dict["shape"] = adata["shape"]
                        stage_2_annotation_dict["pathology_lvl1"] = adata["pathology_lvl1"]
                        stage_2_annotation_dict["anatomy_lvl1"] = adata["anatomy_lvl1"]
                        stage_2_annotation_dict["anatomy_lvl2"] = adata["anatomy_lvl2"]
                        stage_2_annotation_dict["severity"] = adata["severity"]
                        stage_2_annotation_dict["Name_of_verifier"] = df[df["user_id"]==adata["verifier_user_id"]]["salutation"].unique()[0]+" "+df[df["user_id"]==adata["verifier_user_id"]]["first_name"].unique()[0]+" "+df[df["user_id"]==adata["verifier_user_id"]]["last_name"].unique()[0]
                        count_verify_annotation_stage1 = count_verify_annotation_stage1+1
                        adata_dict["stage_2"] = stage_2_annotation_dict
                        if adata["A3.verification_id"]:
                            stage_3_annotation_dict = {}
                            stage_3_annotation_dict["stage_3_annotation_id"] = adata["A3.verification_id"]
                            stage_3_annotation_dict["added_timestamp"] = str(adata["added_timestamp"])
                            stage_3_annotation_dict["status"] = adata["A3.status"]
                            stage_3_annotation_dict["comments"] = adata["A3.comments"]
                            stage_3_annotation_dict["edit"] = adata["A3.edit"]
                            stage_3_annotation_dict["coords"] = json.loads(adata["A3.coords"])
                            stage_3_annotation_dict["shape"] = adata["A3.shape"]
                            stage_3_annotation_dict["pathology_lvl1"] = adata["A3.pathology_lvl1"]
                            stage_3_annotation_dict["anatomy_lvl1"] = adata["A3.anatomy_lvl1"]
                            stage_3_annotation_dict["anatomy_lvl2"] = adata["A3.anatomy_lvl2"]
                            stage_3_annotation_dict["severity"] = adata["A3.severity"]
                            stage_3_annotation_dict["Name_of_verifier"] = df[df["user_id"]==adata["A3.verifier_user_id"]]["salutation"].unique()[0]+" "+df[df["user_id"]==adata["A3.verifier_user_id"]]["first_name"].unique()[0]+" "+df[df["user_id"]==adata["A3.verifier_user_id"]]["last_name"].unique()[0]
                            count_verify_annotation_stage1 = count_verify_annotation_stage1+1
                            adata_dict["stage_2"]["stage_3"] = stage_3_annotation_dict
         
                    stage_1_annotation_resp.append(adata_dict)

                if stage_1_annotation_resp:
                    annotation_dict["stage_1"] = stage_1_annotation_resp
        if annotation_dict:
            imagedict["annotations"] = annotation_dict
        #imagedict["Total annotations"] = count_total_annotation
        #imagedict["Total verified annotation"] = count_verify_annotation_stage1
        image_resp.append(imagedict)
             
    #resp["data"]["series"]["Total_instance"] = len(image_resp)        
    resp["data"]["series"]["instances"] = image_resp
  
    tree_dict = make_medical_tree()
    resp["meta"] = {}
    resp["meta"]["medical_tree"] = tree_dict
    return resp


def reports_response(params):
    """
    Function for viewing report based on its patient_id
    :param params: patient_id 
    :return:response containing data about the report
    """
    diagdb = db_connection()
    cursor = diagdb.cursor()
    resp = []
    sql = ("SELECT * FROM tyrion.ids_raw_reports where patient_id like '%s'"%("%"+params+"%",))
    cursor.execute(sql)
    row = cursor.fetchall()
    for data in row:
        data_dict = {}
        data_dict["diag_report_id"] = data["diag_report_id"]
        data_dict["added_timestamp"] = str(data["added_timestamp"])
        data_dict["hospital_id"] = data["hospital_id"]
        data_dict["patient_id"] = data["patient_id"]
        data_dict["report_date"] = str(data["report_date"])
        data_dict["report_text"] = re.sub(r"Dr*.*","Doctor",data["report_text"])
        resp.append(data_dict)
    return resp

