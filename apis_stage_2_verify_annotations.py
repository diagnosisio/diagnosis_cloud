import MySQLdb
import datetime
import json
from db_config import db_connection

_TABLENAME = "ids_verification"


def Add_verified_annotations_stage_2(userid, series_uuid, slice_id, stage_1_annotation_id, status, comments, edit, coords, shape, pathology_lvl1, anatomy_lvl1, anatomy_lvl2, severity):
    diagdb = db_connection()
    cursor = diagdb.cursor()
    sql = "INSERT INTO " + _TABLENAME + "(verifier_user_id, series_uuid, slice_id, stage_1_annotation_id, status, comments, edit, coords, shape, pathology_lvl1, anatomy_lvl1, anatomy_lvl2, severity) \
   VALUES ('%s','%s','%d','%d','%s','%s','%d','%s','%s','%s','%s','%s','%s')" % \
        (userid, series_uuid, slice_id, stage_1_annotation_id, status, comments,
         edit, coords, shape, pathology_lvl1, anatomy_lvl1, anatomy_lvl2, severity)
    x = cursor.execute(sql)
    stage_2_annotation_id = cursor.lastrowid
    diagdb.commit()
    cursor.close()
    diagdb.close()

    return {"status": 1, "message": "Stage 2 Annotation Added", "timestamp": str(datetime.datetime.now()), "stage_2_annotation_id": stage_2_annotation_id}


def Update_verified_annotations_stage_2(stage_2_annotation_id, userid, series_uuid, slice_id, stage_1_annotation_id, status, comments, edit, coords, shape, pathology_lvl1, anatomy_lvl1, anatomy_lvl2, severity):
    diagdb = db_connection()
    cursor = diagdb.cursor()
    sql = "UPDATE " + _TABLENAME + " SET series_uuid = '%s', slice_id = %d, stage_1_annotation_id = %d, status = '%s', comments = '%s', edit = %d, coords = '%s', shape = '%s', pathology_lvl1 = '%s', anatomy_lvl1 = '%s', anatomy_lvl2 = '%s', severity = '%s' WHERE verification_id = %d AND verifier_user_id = '%s'" % (
        series_uuid, int(slice_id), int(stage_1_annotation_id), status, comments, int(edit), coords, shape, pathology_lvl1, anatomy_lvl1, anatomy_lvl2, severity, int(stage_2_annotation_id), userid)
    resp = cursor.execute(sql)
    diagdb.commit()
    cursor.close()
    diagdb.close()
    if resp == 1:
        return {"status": 1, "message": "Stage 2 Annotation Updated!", "timestamp": str(datetime.datetime.now()), "stage_2_annotation_id": stage_2_annotation_id}
    else:
        return {"status": 0, "message": "Cannot update stage 2 annotation info.", "timestamp": str(datetime.datetime.now()), "stage_2_annotation_id": stage_2_annotation_id}


def Delete_verified_annotations_stage_2(stage_2_annotation_id, userid):
    diagdb = db_connection()
    cursor = diagdb.cursor()
    resp = cursor.execute(
        "DELETE from " + _TABLENAME + " WHERE verification_id = %s AND verifier_user_id = %s", (stage_2_annotation_id, userid,))
    diagdb.commit()
    cursor.close()
    diagdb.close()
    if resp == 1:
        return {"status": 1, "message": "Stage 2 Annotation Info Deleted", "timestamp": str(datetime.datetime.now()), "stage_2_annotation_id": stage_2_annotation_id}
    else:
        return {"status": 0, "message": "Cannot delete stage 2 verification info", "timestamp": str(datetime.datetime.now()), "stage_2_annotation_id": stage_2_annotation_id}
