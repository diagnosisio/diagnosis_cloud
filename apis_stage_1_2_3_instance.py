import MySQLdb
import datetime
import json
from db_config import db_connection

_TABLENAME = "ids_marked_instance_info"


def AddInstanceInfo(userid, series_uuid, slice_id, pathology_lvl1, anatomy_lvl1, anatomy_lvl2, stage,severity,comments):
    diagdb = db_connection()
    cursor = diagdb.cursor()
    sql = "INSERT INTO "+ _TABLENAME+"(series_uuid,slice_id,user_id, \
   pathology_lvl1,anatomy_lvl1,anatomy_lvl2,stage,severity,comments) \
   VALUES ('%s','%d','%s','%s','%s','%s','%d','%s','%s')" % \
        (series_uuid, slice_id, userid, pathology_lvl1,
         anatomy_lvl1, anatomy_lvl2, int(stage),severity,comments)
    x = cursor.execute(sql)
    instance_info_id = cursor.lastrowid
    diagdb.commit()
    cursor.close()
    diagdb.close()

    return {"status": 1, "message": "Instance info added!", "timestamp": str(datetime.datetime.now()), "instance_info_id": instance_info_id}


def UpdateInstanceInfo(userid, series_uuid, slice_id, pathology_lvl1, anatomy_lvl1, anatomy_lvl2, instance_info_id, stage, severity, comments):
    diagdb = db_connection()
    cursor = diagdb.cursor()
    sql = "UPDATE "+ _TABLENAME+" SET pathology_lvl1 = '%s' ,anatomy_lvl1 = '%s', anatomy_lvl2 = '%s', severity = '%s', comments = '%s' WHERE instanceinfo_id = %s AND user_id = '%s' AND stage = '%d' AND slice_id = '%s'" % (
        pathology_lvl1, anatomy_lvl1, anatomy_lvl2, severity, comments, int(instance_info_id), userid, int(stage), int(slice_id))
    resp = cursor.execute(sql)
    diagdb.commit()
    cursor.close()
    diagdb.close()
    if resp == 1:
        return {"status": 1, "message": "Instance info updated!", "timestamp": str(datetime.datetime.now()), "instance_info_id": instance_info_id}
    else:
        return {"status": 0, "message": "Instance info not updated!", "timestamp": str(datetime.datetime.now()), "instance_info_id": instance_info_id}


def DeleteInstanceInfo(instance_info_id, userid, stage):
    diagdb = db_connection()
    cursor = diagdb.cursor()
    resp = cursor.execute(
        "DELETE from "+ _TABLENAME +" WHERE instanceinfo_id = %s AND user_id = %s and stage=%s", (instance_info_id, userid, int(stage)))
    diagdb.commit()
    cursor.close()
    diagdb.close()
    if resp == 1:
        return {"status": 1, "message": "Instance info deleted!", "timestamp": str(datetime.datetime.now()), "instance_info_id": instance_info_id}
    else:
        return {"status": 0, "message": "Instance info not deleted!", "timestamp": str(datetime.datetime.now()), "instance_info_id": instance_info_id}
