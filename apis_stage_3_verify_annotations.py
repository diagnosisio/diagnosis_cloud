import MySQLdb
import datetime
import json
from db_config import db_connection

_TABLENAME = "ids_stage_3_verify_annotations"


def Add_verified_annotations_stage_3(userid, series_uuid, slice_id, stage_1_annotation_id, stage_2_annotation_id, status, comments, edit, coords, shape, pathology_lvl1, anatomy_lvl1, anatomy_lvl2, severity, verification_stage):
    diagdb = db_connection()
    cursor = diagdb.cursor()
    if stage_1_annotation_id == "":
        print "CASE1"
        sql = "INSERT INTO " + _TABLENAME + "(verifier_user_id, series_uuid, slice_id, stage_1_annotation_id, stage_2_annotation_id, status, comments, edit, coords, shape, pathology_lvl1, anatomy_lvl1, anatomy_lvl2, severity,verification_stage) \
       VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')" % \
            (userid, series_uuid, int(slice_id), int(stage_2_annotation_id), int(stage_2_annotation_id), status, comments,
             int(edit), coords, shape, pathology_lvl1, anatomy_lvl1, anatomy_lvl2, severity, verification_stage)
    elif stage_2_annotation_id == "":
        print "CASE2"
        sql = "INSERT INTO " + _TABLENAME + "(verifier_user_id, series_uuid, slice_id, stage_1_annotation_id, status, comments, edit, coords, shape, pathology_lvl1, anatomy_lvl1, anatomy_lvl2, severity,verification_stage) \
       VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')" % \
            (userid, series_uuid, int(slice_id), int(stage_1_annotation_id), status, comments,
             int(edit), coords, shape, pathology_lvl1, anatomy_lvl1, anatomy_lvl2, severity, verification_stage)
    else:
        print "CASE ELSE"
        sql = "INSERT INTO " + _TABLENAME + "(verifier_user_id, series_uuid, slice_id, stage_1_annotation_id, stage_2_annotation_id, status, comments, edit, coords, shape, pathology_lvl1, anatomy_lvl1, anatomy_lvl2, severity,verification_stage) \
       VALUES ('%s','%s','%s','%s', '%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')" % \
            (userid, series_uuid, int(slice_id), int(stage_1_annotation_id), int(stage_2_annotation_id), status, comments,
             int(edit), coords, shape, pathology_lvl1, anatomy_lvl1, anatomy_lvl2, severity, verification_stage)
    x = cursor.execute(sql)
    stage_3_annotation_id = cursor.lastrowid
    diagdb.commit()
    cursor.close()
    diagdb.close()

    return {"status": 1, "message": "Stage 3 Annotation Added", "timestamp": str(datetime.datetime.now()), "stage_3_annotation_id": stage_3_annotation_id}


# def Update_verified_annotations_stage_3(stage_3_annotation_id, userid, series_uuid, slice_id, status, comments, edit, coords, shape, pathology_lvl1, anatomy_lvl1, anatomy_lvl2, severity, verification_stage):
#     diagdb = db_connection()
#     cursor = diagdb.cursor()
#     if stage_1_annotation_id == "":
#         print "CASE1"
#         sql = "UPDATE " + _TABLENAME + " SET verifier_user_id='%s',series_uuid='%s',slice_id='%s',status='%s',comments='%s',edit='%s',coords='%s',shape='%s',pathology_lvl1='%s',anatomy_lvl1='%s',anatomy_lvl2='%s',severity='%s',verification_stage='%s' where verification_id='%s'"%(userid, series_uuid, int(slice_id), int(stage_2_annotation_id), int(stage_2_annotation_id), status, comments,int(edit), coords, shape, pathology_lvl1, anatomy_lvl1, anatomy_lvl2, severity, verification_stage,stage_3_annotation_id)
#     elif stage_2_annotation_id == "":
#         print "CASE2"
#         sql = "UPDATE " + _TABLENAME + " SET verifier_user_id='%s',series_uuid='%s',slice_id='%s',stage_1_annotation_id='%s',status='%s',comments='%s',edit='%s',coords='%s',shape='%s',pathology_lvl1='%s',anatomy_lvl1='%s',anatomy_lvl2='%s',severity='%s',verification_stage='%s' where verification_id='%s'"%(userid, series_uuid, int(slice_id), int(stage_2_annotation_id), status, comments,int(edit), coords, shape, pathology_lvl1, anatomy_lvl1, anatomy_lvl2, severity, verification_stage,stage_3_annotation_id)
#     else:
#         print "CASE ELSE"
#         sql = "UPDATE " + _TABLENAME + " SET verifier_user_id='%s',series_uuid='%s',slice_id='%s',stage_1_annotation_id='%s',stage_2_annotation_id='%s',status='%s',comments='%s',edit='%s',coords='%s',shape='%s',pathology_lvl1='%s',anatomy_lvl1='%s',anatomy_lvl2='%s',severity='%s',verification_stage='%s' where verification_id='%s'"%(userid, series_uuid, int(slice_id), int(stage_1_annotation_id), int(stage_2_annotation_id), status, comments,int(edit), coords, shape, pathology_lvl1, anatomy_lvl1, anatomy_lvl2, severity, verification_stage,stage_3_annotation_id)
#     x = cursor.execute(sql)
#     diagdb.commit()
#     cursor.close()
#     diagdb.close()
#     if resp == 1:
#         return {"status": 1, "message": "Stage 3 Annotation Updated!", "timestamp": str(datetime.datetime.now()), "stage_3_annotation_id": stage_3_annotation_id}
#     else:
#         return {"status": 0, "message": "Cannot update stage 3 annotation info.", "timestamp": str(datetime.datetime.now()), "stage_3_annotation_id": stage_3_annotation_id}

def Update_verified_annotations_stage_3(stage_3_annotation_id, userid, series_uuid, slice_id, status, comments, edit, coords, shape, pathology_lvl1, anatomy_lvl1, anatomy_lvl2, severity, verification_stage):
    diagdb = db_connection()
    cursor = diagdb.cursor()
    sql = "UPDATE " + _TABLENAME + " SET series_uuid = '%s', slice_id = %d,status = '%s', comments = '%s', edit = %d, coords = '%s', shape = '%s', pathology_lvl1 = '%s', anatomy_lvl1 = '%s', anatomy_lvl2 = '%s', severity = '%s',verification_stage='%s' WHERE verification_id = %d AND verifier_user_id = '%s'" % (
        series_uuid, int(slice_id), status, comments, int(edit), coords, shape, pathology_lvl1, anatomy_lvl1, anatomy_lvl2, severity, verification_stage, int(stage_3_annotation_id), userid)
    resp = cursor.execute(sql)
    diagdb.commit()
    cursor.close()
    diagdb.close()
    if resp == 1:
        return {"status": 1, "message": "Stage 3 Annotation Updated!", "timestamp": str(datetime.datetime.now()), "stage_3_annotation_id": stage_3_annotation_id}
    else:
        return {"status": 0, "message": "Cannot update stage 3 annotation info.", "timestamp": str(datetime.datetime.now()), "stage_3_annotation_id": stage_3_annotation_id}


def Delete_verified_annotations_stage_3(stage_3_annotation_id, userid):
    diagdb = db_connection()
    cursor = diagdb.cursor()
    resp = cursor.execute(
        "DELETE from " + _TABLENAME + " WHERE verification_id = %s AND verifier_user_id = %s", (stage_3_annotation_id, userid,))
    diagdb.commit()
    cursor.close()
    diagdb.close()
    if resp == 1:
        return {"status": 1, "message": "Stage 3 Annotation Info Deleted", "timestamp": str(datetime.datetime.now()), "stage_3_annotation_id": stage_3_annotation_id}
    else:
        return {"status": 0, "message": "Cannot delete stage 3 verification info", "timestamp": str(datetime.datetime.now()), "stage_3_annotation_id": stage_3_annotation_id}




