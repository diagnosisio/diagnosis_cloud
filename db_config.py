import MySQLdb
import MySQLdb.cursors
import base64
import os
import sys

HOST = base64.b64decode('bXlzcWwxNDE5OC5kaWFnbm9zaXMuaW8=')
USER = base64.b64decode('cm9vdA==')
PASS = base64.b64decode('R3I4OWlvcEAzN1ZSbm0=')
DATABASE = "tyrion"

KEY = os.environ.get("DIAG_MYSQL_KEY", None)
CERT = os.environ.get("DIAG_MYSQL_CERT", None)
#KEY = "/home/gaurav/Downloads/client-key.pem"
#CERT = "/home/gaurav/Downloads/client-cert.pem"
CIPHER = "AES256-SHA"

if not (KEY and CERT):
    print "DIAGNOSIS.IO ERROR: NO ENVIRONMENT VARIABLES DIAG_MYSQL_KEY OR DIAG_MYSQL_CERT"
    sys.exit(1)

ssl_settings = {'key': KEY, 'cert': CERT, 'cipher': CIPHER}


def db_connection():
    #mydb = MySQLdb.connect(HOST,USER,PASS,DATABASE)
    mydb = MySQLdb.connect(HOST, USER, PASS, DATABASE,
                           ssl=ssl_settings, cursorclass=MySQLdb.cursors.DictCursor)
    return mydb
