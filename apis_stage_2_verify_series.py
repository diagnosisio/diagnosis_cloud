import MySQLdb
import datetime
import json
from db_config import db_connection

_TABLENAME = "ids_stage_2_verify_series_info"

def Add_verified_series_info_stage_2(series_uuid, label, value,verifier_user_id, stage_1_series_info_id, status, comments, edit):
    diagdb = db_connection()
    cursor = diagdb.cursor()
    sql = "INSERT INTO "+ _TABLENAME+"(series_uuid,\
   label,value, verifier_user_id,stage_1_series_info_id,status,comments,edit) \
   VALUES ('%s','%s','%s','%s', '%d','%s','%s','%d')" % \
        (series_uuid, label, value, verifier_user_id,int(stage_1_series_info_id),status,comments,int(edit))
    x = cursor.execute(sql)
    seriesinfo_uid = cursor.lastrowid
    diagdb.commit()
    cursor.close()
    diagdb.close()
    return {"status": 1, "message": "Added series info", "timestamp": str(datetime.datetime.now()), "seriesinfo_uid": seriesinfo_uid}


def Update_verified_series_info_stage_2(series_uuid, label, value,verifier_user_id, stage_1_series_info_id, status, comments, edit, stage_2_series_info_id):
    diagdb = db_connection()
    cursor = diagdb.cursor()
    sql = "UPDATE ids_stage_2_verify_series_info SET label = '%s' ,value = '%s',stage_1_series_info_id='%d',status='%s',comments='%s',edit='%s' WHERE stage_2_series_info_id = %d AND verifier_user_id = '%s' AND series_uuid = '%s'" % (
        label, value, stage_1_series_info_id, status, comments, edit, int(stage_2_series_info_id), verifier_user_id, series_uuid)
    print sql
    resp = cursor.execute(sql)
    diagdb.commit()
    cursor.close()
    diagdb.close()
    if resp == 1:
        return {"status": 1, "message": "Series info updated!", "timestamp": str(datetime.datetime.now()), "seriesinfo_uid": stage_2_series_info_id}
    else:
        return {"status": 0, "message": "Cannot update series info.", "timestamp": str(datetime.datetime.now()), "seriesinfo_uid": stage_2_series_info_id}


def Delete_verified_series_info_stage_2(stage_2_series_info_id, verifier_user_id):
    diagdb = db_connection()
    cursor = diagdb.cursor()
    resp = cursor.execute(
        "DELETE from "+_TABLENAME+ " WHERE stage_2_series_info_id = %s AND verifier_user_id = %s", (int(stage_2_series_info_id), verifier_user_id))
    diagdb.commit()
    cursor.close()
    diagdb.close()
    if resp == 1:
        return {"status": 1, "message": "Seried Info Deleted", "timestamp": str(datetime.datetime.now()), "seriesinfo_uid": stage_2_series_info_id}
    else:
        return {"status": 0, "message": "Cannot delete series info", "timestamp": str(datetime.datetime.now()), "seriesinfo_uid": stage_2_series_info_id}
