import MySQLdb
import datetime
import json
from db_config import db_connection

_TABLENAME = "ids_marked_annotations"

"""
Crud for all stage annotation.
Whenever a new annotation is marked (at any stage) it gets stored in ids_marked_annotations.
"""

def Add_annotations_stage_1(userid, coords, pathology_lvl1, anatomy_lvl1, anatomy_lvl2, comments, series_uuid, slice_id, shape, severity, stage):
    diagdb = db_connection()
    cursor = diagdb.cursor()
    sql = "INSERT INTO "+ _TABLENAME+"(series_uuid,slice_id, \
   user_id,coords,pathology_lvl1,anatomy_lvl1,anatomy_lvl2,comments,shape,severity,stage) \
   VALUES ('%s','%d', '%s','%s','%s','%s','%s','%s','%s','%s','%d')" % \
        (series_uuid, int(slice_id), userid, coords, pathology_lvl1,
         anatomy_lvl1, anatomy_lvl2, comments, shape, severity, int(stage))
    x = cursor.execute(sql)
    annotation_id = cursor.lastrowid
    diagdb.commit()
    cursor.close()
    diagdb.close()

    return {"status": 1, "message": "Annotation added!", "timestamp": str(datetime.datetime.now()), "annotation_id": annotation_id}


def Update_annotations_stage_1(annotation_id, userid, coords, pathology_lvl1, anatomy_lvl1, anatomy_lvl2, comments, series_uuid, slice_id, shape, severity, stage):
    print "ANN", annotation_id, type(annotation_id)
    print slice_id,type(slice_id)
    print stage,type(stage)
    diagdb = db_connection()
    cursor = diagdb.cursor()
    sql = "UPDATE "+ _TABLENAME+" SET series_uuid='%s', coords = '%s', pathology_lvl1 = '%s', anatomy_lvl1 = '%s', anatomy_lvl2 = '%s', comments = '%s', shape = '%s', severity = '%s', stage = %s WHERE annotation_id = %s AND user_id = '%s' AND slice_id = %s and stage=%s" % (
        series_uuid, coords, pathology_lvl1, anatomy_lvl1, anatomy_lvl2, comments, shape, severity, int(stage), int(annotation_id), userid, int(slice_id), int(stage))
    resp = cursor.execute(sql)
    diagdb.commit()
    cursor.close()
    diagdb.close()
    if resp == 1:
        return {"status": 1, "message": "Annotation updated!", "timestamp": str(datetime.datetime.now()), "annotation_id": annotation_id}
    else:
        return {"status": 0, "message": "Annotation not updated!", "timestamp": str(datetime.datetime.now()), "annotation_id": annotation_id}


def Delete_annotations_stage_1(annotation_id, userid, stage):
    diagdb = db_connection()
    cursor = diagdb.cursor()
    resp = cursor.execute(
        "DELETE from "+ _TABLENAME+" WHERE annotation_id = %s AND user_id = %s and stage=%s", (int(annotation_id), userid, int(stage)))
    diagdb.commit()
    cursor.close()
    diagdb.close()
    if resp == 1:
        return {"status": 1, "message": "Annotation deleted!", "timestamp": str(datetime.datetime.now()), "annotation_id": annotation_id}
    else:
        return {"status": 0, "message": "Annotation not deleted!", "timestamp": str(datetime.datetime.now()), "annotation_id": annotation_id}
