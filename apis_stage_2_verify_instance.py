import MySQLdb
import datetime
import json
from db_config import db_connection

_TABLENAME = "ids_stage_2_verify_instance_info"


def Add_verified_instance_info_stage_2(stage_1_instance_info_id, series_uuid, slice_id, pathology_lvl1, anatomy_lvl1, anatomy_lvl2, severity, comments,edit,verifier_user_id,status):
    diagdb = db_connection()
    cursor = diagdb.cursor()
    sql = "INSERT INTO "+_TABLENAME+"(series_uuid,slice_id,stage_1_instance_info_id, \
   pathology_lvl1,anatomy_lvl1,anatomy_lvl2,severity,comments,edit,verifier_user_id,status) \
   VALUES ('%s','%d','%d','%s','%s','%s','%s','%s','%s','%s','%s')" % \
        (series_uuid, int(slice_id), int(stage_1_instance_info_id), pathology_lvl1,
         anatomy_lvl1, anatomy_lvl2, severity, comments,int(edit),verifier_user_id,status)
    x = cursor.execute(sql)
    instance_info_id = cursor.lastrowid
    diagdb.commit()
    cursor.close()
    diagdb.close()

    return {"status": 1, "message": "Added instance info", "timestamp": str(datetime.datetime.now()), "instance_info_id": instance_info_id}


def Update_verified_instance_info_stage_2(stage_2_instance_info_id ,stage_1_instance_info_id, series_uuid, slice_id, pathology_lvl1, anatomy_lvl1, anatomy_lvl2, severity, comments,edit,verifier_user_id,status):
    diagdb = db_connection()
    cursor = diagdb.cursor()
    sql = "UPDATE " +_TABLENAME+ " SET pathology_lvl1 = '%s' ,anatomy_lvl1 = '%s', anatomy_lvl2 = '%s', stage_1_instance_info_id = '%d', severity = '%s', comments = '%s', edit = '%s', status = '%s'   WHERE stage_2_instance_info_id = %d AND verifier_user_id = '%s' AND slice_id = '%s'" % (
        pathology_lvl1, anatomy_lvl1, anatomy_lvl2, int(stage_1_instance_info_id), severity, comments, int(edit), status, int(stage_2_instance_info_id),verifier_user_id,slice_id)
    resp = cursor.execute(sql)
    diagdb.commit()
    cursor.close()
    diagdb.close()
    if resp == 1:
        return {"status": 1, "message": "Instance info updated!", "timestamp": str(datetime.datetime.now()), "instance_info_id": stage_2_instance_info_id}
    else:
        return {"status": 0, "message": "Cannot update instance info.", "timestamp": str(datetime.datetime.now()), "instance_info_id": stage_2_instance_info_id}


def Delete_verified_instance_info_stage_2(stage_2_instance_info_id, verifier_user_id):
    diagdb = db_connection()
    cursor = diagdb.cursor()
    resp = cursor.execute(
        "DELETE from "+_TABLENAME+ " WHERE stage_2_instance_info_id = %s AND verifier_user_id = %s", (int(stage_2_instance_info_id), verifier_user_id))
    diagdb.commit()
    cursor.close()
    diagdb.close()
    if resp == 1:
        return {"status": 1, "message": "Seried Info Deleted", "timestamp": str(datetime.datetime.now()), "seriesinfo_uid": stage_2_instance_info_id}
    else:
        return {"status": 0, "message": "Cannot delete series info", "timestamp": str(datetime.datetime.now()), "seriesinfo_uid": stage_2_instance_info_id}
