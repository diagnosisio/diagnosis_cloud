import MySQLdb
import datetime
import json
from db_config import db_connection

_TABLENAME = "ids_marked_series_info"


def AddSeriesInfo(userid, series_uuid, label, value, stage,comments):
    diagdb = db_connection()
    cursor = diagdb.cursor()
    sql = "INSERT INTO "+ _TABLENAME+"(series_uuid,user_id, \
   label,value,stage,comments) \
   VALUES ('%s','%s','%s','%s','%d','%s')" % \
        (series_uuid, userid, label, value, int(stage),comments)
    x = cursor.execute(sql)
    series_info_id = cursor.lastrowid
    diagdb.commit()
    cursor.close()
    diagdb.close()

    return {"status": 1, "message": "Series info added!", "timestamp": str(datetime.datetime.now()), "series_info_id": series_info_id}


def UpdateSeriesInfo(userid, series_uuid, label, value, series_info_id, stage,comments):
    diagdb = db_connection()
    cursor = diagdb.cursor()
    sql = "UPDATE "+ _TABLENAME+" SET label = '%s' ,value = '%s',comments = '%s' WHERE series_info_id = '%s' AND user_id = '%s' AND series_uuid = '%s' and stage='%s'" % (
        label, value,comments, int(series_info_id), userid, series_uuid, int(stage))
    resp = cursor.execute(sql)
    diagdb.commit()
    cursor.close()
    diagdb.close()
    if resp == 1:
        return {"status": 1, "message": "Series info updated!", "timestamp": str(datetime.datetime.now()), "series_info_id": series_info_id}
    else:
        return {"status": 0, "message": "Series info not updated!", "timestamp": str(datetime.datetime.now()), "series_info_id": series_info_id}


def DeleteSeriesInfo(series_info_id, user_id, stage):
    diagdb = db_connection()
    cursor = diagdb.cursor()
    resp = cursor.execute(
        "DELETE from "+ _TABLENAME +" WHERE series_info_id = %s AND user_id = %s and stage=%s", (series_info_id, user_id, int(stage)))
    diagdb.commit()
    cursor.close()
    diagdb.close()
    if resp == 1:
        return {"status": 1, "message": "Series Info Deleted!", "timestamp": str(datetime.datetime.now()), "series_info_id": series_info_id}
    else:
        return {"status": 0, "message": "Series Info not deleted!", "timestamp": str(datetime.datetime.now()), "series_info_id": series_info_id}
